/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

/* Basic configuration */

var configurator = [];
var changeList = [];

/*
 * Basic logging functionality
 */
  
function log(message, level){
    try{
        consoleDebugLevel = parseInt(consoleDebugLevel);
    }catch(exception){
        consoleDebugLevel = 0;
    }

    try{
        if(!level){
            level = 3;
        }
        if(level <= consoleDebugLevel){
            console.log(message);
        }
    }catch(exception){
    }
}

var Configuration;
Configuration = {
    config: {
        // all config values are set in custom.phtml
        baseurl: '',
        skin: '',
        consoleDebugLevel: '',
        urlrefresh: '',
        combinationInstance: '',
        currentPriceId: '',
        urlwebservice: '',
        urlpriceloadimage: '',
        getparams: '',
        idparam: '',
        configurationvalues: '',
        combinedimagerefreshinterval : 0,
        ssl: '',
        protocol: '',
        refreshCounter: 0,
        preventImageRefresh: false
    },

    /* Do everything which is necessary at option startup: blacklisting, price calculation, etc. */
    onConstruct: function (js_template_id, id, option_id, type, doChangeList, price, blacklisting) {
        if (blacklisting) {
            Configuration.getInstanceByJsId(js_template_id).getBlacklist().blacklistCallback(id, option_id, type);
        }

        if (price != null) {
            Configuration.setOptionPriceAndReload(js_template_id, option_id, price);
        } else {
            Configuration.reloadPrice(js_template_id);
        }

        if (doChangeList) {
            changeList[js_template_id].push(id);
        }
    },

    /* generic functions */
    getInstanceByOptionId: function (id) {
        for (var i = 0; i < configurator.length; i++) {
            log("check " + id + " " + configurator[i].getOptionId(), 3);
            if (configurator[i].getOptionId() == id) {
                log("getInstanceByOptionId id=" + id + " jsid=" + configurator[i].getJSId(), 3);
                return configurator[i];
            }
        }
        return 0;
    },

    getInstanceByJsId: function (id) {
        for (var i = 0; i < configurator.length; i++) {
            log("check " + id + " " + configurator[i].getJSId(), 3);
            if (configurator[i].getJSId() == id) {
                log("getInstanceByJSId jsid=" + id + " id=" + configurator[i].getOptionId(), 3);
                return configurator[i];
            }
        }
        return 0;
    },

    refreshConfigurator: function (configurator_id) {
        log("refreshConfigurator " + configurator_id, 1);
        var instance = Configuration.getInstanceByJsId(configurator_id);

        AjaxLoader.show();
        PriceLoader.show();

        var data = jQuery('#product_addtocart_form').serializeArray();
        var refresh_data = Array();
        var i = 0;
        for (var n = 0; n < data.length; n++) {
            if (data[n]['name'] == "productoptionid") {
                if (i == Configuration.config.refreshCounter) {
                    refresh_data[n] = data[n];
                }
                i++;
            } else {
                refresh_data[n] = data[n];
            }
        }
        data = refresh_data;
        Configuration.config.refreshCounter++;

        jQuery('select:disabled,input:disabled').each(function (i, item) {
            data.push({ name: jQuery(item).attr('name'), value: jQuery(item).val() });
        });
        data.push({ name: "jstemplateoption", value: configurator_id });
        data.push({ name: "skin", value: Configuration.config.skin});

        /* check http parameters for deep links */
        if (Configuration.config.getparams) {
            var params = JSON.parse(Configuration.config.getparams);
            if (params['options']) {
                var options = params['options']
                jQuery.each(options, function (index, value) {
                    var key = "o_" + instance.getOptionId() + "_" + index;
                    data.push({name: key, value: value});
                });
            }
            jQuery.each(params, function (index, value) {
                var pattern = /o_[0-9]+_[0-9]+/;
                if (index.match(pattern)) {
                    data.push({name: index, value: value});
                }
            });
        }

        // Handle deep links
        if (Configuration.config.idparam) {
            var result = Configuration.config.configurationvalues[configurator_id];
            if (result && result != "null") {
                var params = JSON.parse(result);
                jQuery.each(params, function (index, value) {
                    var key = "o_" + instance.getOptionId() + "_" + index;
                    data.push({name: key, value: value});
                });
            }
        }

        changeList[configurator_id] = [];

        jQuery.post(
            Configuration.config.urlrefresh,
            jQuery.param(data),
            function (data, textStatus, XMLHttpRequest) {
                log("Refresh for id: " + configurator_id, 1);
                data = data.replace(/js\w{13}js/g, configurator_id);

                configurator_tag = jQuery('#configurator_options_' + configurator_id);
                configurator_tag.html(data);
                configurator_tag.show();

                for (var i = 0; i < changeList[configurator_id].length; i++) {
                    jQuery("#" + changeList[configurator_id][i]).change();
                }

                PriceLoader.hide();
                AjaxLoader.hide();
            },
            'html'
        );
        return true;


    },

    /*
     * price updates
     */

    getPriceOfOption: function (id, type) {
        switch (type) {
            case "select":
            case "selectcombi":
            case "listimage":
            case "listimagecombi":
                return Configuration.getPriceFromSelect(id);
                break;
            case "radiobuttons":
                return Configuration.getPriceFromRadio(id);
                break;
            case "checkbox":
                return Configuration.getPriceFromCheckbox(id);
            default:
                return Configuration.getPriceFromOptionDefault(id);
                break;
        }

        return false;
    },

    getPriceFromCheckbox: function (id) {
        var price = 0.0;
        if (jQuery("#" + id).is(':checked')) {
            var element = jQuery("#" + id);
            price = element.attr("attr-price");
            if (!price || isNaN(price)) {
                price = 0.0;
            }
        }
        return price;
    },

    getPriceFromOptionDefault: function (id) {
        var element = jQuery("#" + id);
        var price = element.attr("attr-price");
        if (!price || isNaN(price)) {
            price = 0.0;
        }
        return price;
    },

    getPriceFromSelect: function (id) {
        var element = jQuery("#" + id + " option:selected");
        var price = element.attr("attr-price");
        if (!price || isNaN(price)) {
            price = 0.0;
        }
        return price;
    },

    getPriceFromRadio: function (id) {
        var element = jQuery("#" + id).parent().find("input:checked");
        var price = element.attr("attr-price");
        if (!price || isNaN(price)) {
            price = 0.0;
        }
        return price;
    },

    setOptionPriceAndReload: function (jsTemplateId, optionId, price) {
        log('setOptionPriceAndReload start template-id=' + jsTemplateId + ' option-id=' + optionId + " price=" + price, 2);
        jQuery(".price-box").hide();
        var instance = Configuration.getInstanceByJsId(jsTemplateId);
        instance.setOptionPrice(price, optionId);
        instance.reloadPrice();
        jQuery(".price-box").show();
        log('setOptionPriceAndReload end', 2);
    },

    reloadPrice: function (jsTemplateId) {
        log('reloadPrice start jsTemplateId-id=' + jsTemplateId, 2);
        jQuery(".price-box").hide();
        var instance = Configuration.getInstanceByJsId(jsTemplateId);
        instance.reloadPrice();
        jQuery(".price-box").show();
        log('reloadPrice end', 2);
    },

    /* Functions for combination */
    combinationValueCallbackString: function (self) {
        var value = '';
        for (var i = 0; i < self.items.length; i++) {
            value = value + '' + self.items[i].value;
        }
        return value;
    },

    combinationValueCallback: function (self, operator, decimal_place) {
        var value = 0;
        if (operator == "*")
            value = 1;
        for (var i = 0; i < self.items.length; i++) {
            switch (operator) {
                case "+":
                    value = value + self.items[i].value;
                    ;
                    break;
                case "*":
                    value = value * self.items[i].value;
                    ;
                    break;
            }
            log('adding ' + self.items[i].value, 3);
        }
        value = new Number(value);
        value = value.toFixed(decimal_place);
        log('value result=' + value, 2);
        return value;
    },

    /* get an element from the price-list for a given value */
    getPriceFromPricelist: function (value, pricelist, decimal_place, operator_value_price) {
        var price = 0;
        for (var i = 0; i < pricelist.length; i++) {
            var to_eval = "if (" + value + " " + pricelist[i]["operator"] + " " + pricelist[i]["value"] + ") { true;} else { false;} ";

            try {
                if (eval(to_eval)) {
                    price = pricelist[i]["price"];
                    price = new Number(price);
                    price = price.toFixed(decimal_place);

                    if (operator_value_price != null) {
                        var to_eval = price + " " + operator_value_price + " " + value;
                        price = eval(to_eval);
                    }

                    if (pricelist[i]["operator"] == "<" || pricelist[i]["operator"] == "<=" || pricelist[i]["operator"] == "==") {
                        return price;
                    }
                }
            } catch (e) {
            }
        }
        return price;
    },

    /* get current the price from a price-list for a combination */
    combinationPriceFromPricelistCallback: function (self, pricelist_json, decimal_place) {
        log('combinationPriceFromPricelistCallback start', 2);
        var pricelist = JSON.parse(pricelist_json);
        if (self.value_id && pricelist[self.value_id]) {
            pricelist = pricelist[self.value_id];
        }
        var price = Configuration.getPriceFromPricelist(self.value, pricelist, decimal_place, null);
        if (price == 0) {
            price = 0;
        }
        var item = jQuery(self.input);
        if (jQuery(item).hasClass("selectcombi") || jQuery(item).hasClass("listimagecombi")) {
            Configuration.selectcombiFrontendUpdate(jQuery(item).attr("id"), self.value, pricelist_json, decimal_place, self, null);
        }

        price = new Number(price);
        price = price.toFixed(decimal_place);
        log('combinationPriceFromPricelistCallback end price=' + price, 2);
        return price;
    },

    /* calculate the price for a combination */
    combinationPriceCallback: function (self, pricelist_json, operator, decimal_place) {
        var pricelist = JSON.parse(pricelist_json);
        var price = 0;
        if (self.value_id && pricelist[self.value_id]) {
            pricelist = pricelist[self.value_id][0];
            price = pricelist["price"];

        }
        switch (operator) {
            case "*":
                price = self.value * price;
                break;
            case "+":
                price = self.value + price;
                break;
        }

        var item = jQuery(self.input);
        if (jQuery(item).hasClass("selectcombi") || jQuery(item).hasClass("listimagecombi")) {
            Configuration.selectcombiFrontendUpdate(jQuery(item).attr("id"), self.value, pricelist_json, decimal_place, self, "*");
        }

        price = new Number(price);
        price = price.toFixed(decimal_place);
        return price;
    },

    getPrice: function (self, pricelist_json, operator_value_price, decimal_place) {
        var pricelist = JSON.parse(pricelist_json);
        var price = 0;

        price = Configuration.getPriceFromPricelist(self.value, pricelist, decimal_place, operator_value_price);

        return price;
    },

    /* Update the price section in the selectcombi dropdown */
    selectcombiFrontendUpdate: function (id, value, pricelist_json, decimal_place, combination_instance, operator_value_price) {
        var pricelist = JSON.parse(pricelist_json);
        Configuration.config.combinationInstance = combination_instance;
        Configuration.config.currentPriceId = id;

        if (jQuery("#" + id).attr("price-display") == 2) {
            log('selectcombiFrontendUpdate no price update', 3);
            return null;
        }

        log('selectcombiFrontendUpdate start each', 3);

        /* Update the price in select-options first */
        jQuery("#" + id).find("option").each(function () {
            var value_id = jQuery(this).attr("attr-value-id");
            var my_value = jQuery(this).attr("attr-value");
            var id = jQuery(this).parent().attr("id");

            var clone = jQuery.extend(true, {}, Configuration.config.combinationInstance);
            clone.setItemValueOnly(id, my_value, value_id);
            var value = clone.calculateCallback(clone);

            var value_pricelist = pricelist[value_id];
            if (value_pricelist) {
                var price = Configuration.getPriceFromPricelist(value, value_pricelist, decimal_place, operator_value_price);
                var option_value_tag = jQuery("#" + id + " option[attr-value-id='" + value_id + "']");
                jQuery(option_value_tag).attr("attr-price", price);
            }
        });

        /* Update html */
        jQuery("#" + id).find("option").each(function () {
            var value_id = jQuery(this).attr("attr-value-id");
            if (value_id) {
                var option_value_tag = jQuery("#" + id + " option[attr-value-id='" + value_id + "']");
                var current_value_id = Configuration.config.combinationInstance.value_id;
                var current_price = 0;
                if (current_value_id) {
                    current_price = jQuery("#" + Configuration.config.currentPriceId + " option[attr-value-id=" + current_value_id + "]").attr("attr-price");
                }
                OptionSelect.updatePriceItemInSelect(option_value_tag, current_price);
            }
        });
        log('selectcombiFrontendUpdate html update done', 3);

        if (jQuery("#" + id).hasClass("listimagecombi")) {
            log('selectcombiFrontendUpdate update listimagecombi', 3);
            var hide_select = jQuery('#' + id).attr("hide-select");
            if (hide_select == "false") {
                hide_select = false;
            }
            var show_labels = jQuery('#' + id).attr("show-label");
            if (show_labels == "false") {
                show_labels = false;
            }
            jQuery('#' + id).next().remove();
            jQuery('#' + id).imagepicker({
                hide_select: hide_select,
                show_label: show_labels
            });
        }
        log('selectcombiFrontendUpdate done', 3);
    },

    addCombinationHandler: function (combination_instance, id, target_id, target_type, pricelist_json) {
        log('addCombinationHandler id=' + id, 2);

        var value = 0;
        var value_id = null;
        if (jQuery('#' + id).is('SELECT')) {
            value = jQuery('#' + id).find('option:selected').attr('attr-value');
            // get price-list for the selected value-id
            if ((jQuery('#' + id).hasClass("selectcombi")) || (jQuery('#' + id).hasClass("listimagecombi"))) {
                value_id = jQuery('#' + id).find('option:selected').attr('attr-value-id');
            }
            log(id + ' is SELECT', 2);
        } else if (jQuery('#' + id).hasClass('radiobuttons')) {
            log('listimage or radio ' + jQuery(".opt" + target_id + ":checked").attr('attr-value'), 3);
            value = jQuery(".opt" + target_id + ":checked").attr('attr-value');
        } else if (jQuery('#' + id).hasClass('static')) {
            value = jQuery('#' + id).html().trim();
        } else if (jQuery('#' + id).hasClass('checkbox')) {
            value = OptionCheckbox.getValue(id);
        } else {
            value = jQuery('#' + id).val();
        }
        combination_instance.addItem(id, value, value_id);

        if (target_type == "radiobuttons") {
            log("adding click handler for " + id, 2);
            jQuery(document).on("change", "#" + id, function (element) {
                var element = jQuery('#' + id).parent().find('input:checked');
                log('onclick handler id=' + id + ' called', 3);
                log('setItemValue=' + jQuery(element).attr('attr-value'), 3);
                combination_instance.setItemValue(id, jQuery(element).attr('attr-value'), jQuery(element).attr('attr-value-id'));
            });
        } else {
            log('adding onchange handler for id ' + target_id, 3);
            jQuery(document).on('change', '#' + id, function () {
                log('onchange event id ' + jQuery(this).attr("id"), 3);
                if (!Validation.validate(id)) return false;
                log('onchange handler id=' + id + ' called', 3);
                var value;
                var value_id = null;
                if (jQuery(this)[0].nodeName == 'SELECT') {
                    value = jQuery(this).find('option:selected').attr('attr-value');
                    // get price-list for the selected value-id
                    if (jQuery(this).hasClass("selectcombi") || jQuery(this).hasClass("listimagecombi")) {
                        value_id = jQuery(this).find('option:selected').attr('attr-value-id');
                    }
                    log('It´s select: ' + value + " " + value_id, 3);
                } else if (jQuery(this).hasClass("checkbox")) {
                    value = OptionCheckbox.getValue(jQuery(this).attr("id"));
                } else {
                    value = jQuery(this).val();
                    log('any other element: ' + value, 3);
                }
                combination_instance.setItemValue(jQuery(this).attr('id'), value, value_id);
                log('setItemValue=' + jQuery(this).attr('id') + " " + value + " value_id " + value_id, 2);
            });
        }
    }
};

var OptionArea = {
    checkPlaceholder : function(id) {
        if (jQuery("#"+id).hasClass("placeholder")) {
            jQuery("#"+id).removeClass("placeholder");
            jQuery("#"+id).val("");
        }
    },
    resetPlaceholder : function(id) {
        var value = jQuery("#"+id).val();
        if ( value == "") {
            jQuery("#"+id).addClass("placeholder");
            jQuery("#"+id).val(jQuery("#"+id).attr("placeholder-text"));
        }
    }
 }

var OptionWebservice = {
    getValue : function (self) {
        var value = null;
        var params = new Array();
        for(var i=0; i<self.items.length; i++) {
            var parts = self.items[i].id.split('-');
            var id = parts[parts.length-1];
            params.push({ name : id , value : self.items[i].value });
        }
        params.push({ name : 'option_id' , value : self.templateOptionId });

        AjaxLoader.show()
        var url = Configuration.config.urlwebservice;
        jQuery.ajax({
            url: url,
            type: 'get',
            cache: false,
            data:jQuery.param(params),
            async: false,
            dataType: 'html',
            success: function(data) {
               value = data;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                log('error :'+XMLHttpRequest.responseText, 1);
            }
        });

        if (typeof value == 'number'){
            value = value.toFixed(4);
        }

        AjaxLoader.hide();
        return value;
    }
}

var OptionSelect = {
    updatePriceItemInSelect : function(element, current_price) {
        if (jQuery(element).attr("option-show-price") == "false")
            return false;
        var parent = jQuery(element).parent();
        var price_display = jQuery(parent).attr("price-display");
        var price_display_zero = jQuery(parent).attr("price-display-zero");
        var price = jQuery(element).attr("attr-price");
        if (price_display == 1) { // price change relative
            price = price - current_price;
        }
        var raw_price = price;

        var locale = jQuery(parent).attr("locale");
        Globalize.culture(locale);
        price = Globalize.format(parseFloat(price), "n");
        var sign = "+";
        if (raw_price < 0) {
            sign = "";
        }
        var pricechange = "";
        if ((raw_price != 0) || (raw_price == 0 && price_display_zero == 1)) {
            pricechange= " ["+sign+" "+price+" "+jQuery(parent).attr("currency")+"]";
        }

        var text = jQuery(element).html();
        text = text.replace(/\[(.*?)\]/g,"");
        var html = jQuery.trim(text)/* +" "+pricechange*/;
        jQuery(element).html(html);

        return true;
    },

    updatePrice : function(element) {
        var current_price = jQuery(element).find('option:selected').attr('attr-price');
        jQuery(element).find("option").each(function() {
            OptionSelect.updatePriceItemInSelect(this, current_price);
        })
    }
}

var OptionCheckbox = {
    getValue : function(id) {
        var value = 0.0;
        if (jQuery('#'+id).is(':checked')) {
            value = jQuery('#'+id).val();
        }
        return value;
    }
}

var OptionSelectImage = {
    setThumbnailImage : function(select_tag_id) {
        var image_div_id = jQuery("#" + select_tag_id).next().find("img");
        var element = jQuery("#" + select_tag_id + " option:selected");
        var image = jQuery(element).attr("data-img-src");
        if (image) {
            jQuery(image_div_id).attr("src", image);
        }
    }
}

var OptionListImage = {
}

var OptionTextImage = {
    changeTextAlignment : function(group_id, current_id) {
        /* delete current selection */
        jQuery(".font-configuration [attr-dynamics-id='"+group_id+"']").each(function() {
            var thumb = jQuery(this).attr("attr-thumb");
            jQuery(this).attr("src", thumb);
        });
        /* select new selection */
        var obj = jQuery("#"+current_id);
        var checked = jQuery(obj).find("img").attr("attr-thumb-checked");
        var src = jQuery("#"+current_id).find("img").attr("src");
        jQuery("#"+current_id).find("img").attr("src", checked);
    }
}

var OptionRadioButtons = {
    onClick : function(e, obj) {
        /* Fix for old IE browsers */
        if (jQuery.browser.msie  && parseInt(jQuery.browser.version) <= 7) {
            e.preventDefault();
            jQuery("#"+jQuery(obj).attr("for")).click().change();
        }

        /* Get Parent */
        var attrId = jQuery(obj).attr("attr-id");
        var parent = jQuery("#"+attrId);

        /* Set checked value */
        parent.parent().find("input[type='radio']").each(function() {
            jQuery(this).removeAttr("checked");
        })
        jQuery(obj).attr("checked","checked");

        /* Copy values to hidden input container */
        var value = jQuery(obj).attr("attr-value");
        var valueId = jQuery(obj).attr("attr-value-id");

        parent.attr("attr-value-id", valueId).val(valueId).change();
    },
    updatePriceItemInInput : function(element, current_price, parent) {
        var price_display = jQuery(parent).attr("price-display");
        var price_display_zero = jQuery(parent).attr("price-display-zero");
        var price = jQuery(element).attr("attr-price");
        if (price_display == 1) { // price change relative
            price = price - current_price;
        }
        var raw_price = price;

        var locale = jQuery(parent).attr("locale");
        Globalize.culture(locale);
        price = Globalize.format(parseFloat(price), "n");
        var sign = "+";
        if (raw_price < 0) {
            sign = "";
        }
        var pricechange = "";
        if ((raw_price != 0) || (raw_price == 0 && price_display_zero == 1)) {
            pricechange= " ["+sign+" "+price+" "+jQuery(parent).attr("currency")+"]";
        }

        var text = jQuery(element).next().html();
        text = text.replace(/\[(.*?)\]/g,"");
        var html = jQuery.trim(text)+" "+pricechange;
        jQuery(element).next().html(html);

        return true;
    },
    updatePrice : function(element) {
        var parent = jQuery(element).parent();
        var current_price = jQuery(parent).find('input:checked').attr('attr-price');
        jQuery(parent).find("input[type=radio]").each(function() {
            OptionRadioButtons.updatePriceItemInInput(this, current_price, element);
        });
    }
}

var OptionExpression = {
     getValue: function e(self, expressionBECode){
        var expression = '';
        var value = '';
        for(var i=0; i<self.items.length; i++) {
            var parts = self.items[i].id.split('-');
            var id = parts[parts.length-1];
            if (!OptionExpression.isNumber(self.items[i].value)) {
                log('adding string', 3);
                expression = expression + ' opt'+id+'=\"'+self.items[i].value+'\"; ';
            } else {
                log('adding number', 3);
                expression = expression + ' opt'+id+'='+ self.items[i].value+'; ';
            }
        }
        expression = expression + expressionBECode;
        log("expression: "+expression, 3);
        log("expression result is "+eval(expression), 3);
        value = eval(expression);
        if (typeof value == 'number'){
            value = value.toFixed(4);
        }
        log('value ' +value, 2);
        return value;
    },
    isNumber : function(value) {
        var isNumber = true;

        // Count "." in value
        var str = new String(value);
        var count = (str.match(/\./g)||[]).length;
        if (count > 1) {
            return false;
        }

        // Parse as float
        if (isFinite(parseFloat(value)) == false) {
            return false;
        }

        return true;
    }
}

var OptionMatrixvalue = {
    getValue: function (self, matrixDimensionX, scaleX, matrixOperatorX, matrixDimensionY, scaleY, matrixOperatorY, matrix){
        var matrixdimx = matrixDimensionX;
        log('matrixdimx='+matrixdimx, 3);
        var scalex = jQuery.parseJSON(scaleX);
        log('scalex='+scalex, 3);
        var matrixopx = matrixOperatorX;
        log('matrixopx='+matrixopx, 3);
        var matrixdimy = matrixDimensionY;
        log('matrixdimy='+matrixdimy, 3);
        var scaley = jQuery.parseJSON(scaleY);
        log('scaley='+scaley, 3);
        var matrixopy = matrixOperatorY;
        log('matrixopy='+matrixopy, 3);
        matrix = jQuery.parseJSON(matrix);
        log('matrix='+matrix, 2);
        for(var i=0; i<self.items.length; i++) {
            parts = self.items[i].id.split('-');
            id = parts[parts.length-1];
            if ('opt'+id == matrixdimx) matrixdimx = self.items[i].value;
            if ('opt'+id == matrixdimy) matrixdimy = self.items[i].value;
        }
        log('matrixdimx='+matrixdimx, 3);
        log('matrixdimy='+matrixdimy, 3);
        var value = 0;

        if (matrixopx == 'number higher') {
            if (parseFloat(matrixdimx) > parseFloat(scalex[scalex.length-1])) {
                matrixdimx = scalex[scalex.length-1];
            } else if (parseFloat(matrixdimx) <= parseFloat(scalex[0])) {
                matrixdimx = scalex[0];
            } else {
                for(i=1; i<scalex.length; i++) {
                    if (parseFloat(scalex[i-1]) < parseFloat(matrixdimx) && parseFloat(scalex[i]) >= parseFloat(matrixdimx)) {
                        matrixdimx = scalex[i];
                    }
                }
            }
        }
        log('matrixdimx='+matrixdimx, 3);

        if (matrixopx == 'number lower') {
            if (parseFloat(matrixdimx) > parseFloat(scalex[scalex.length-1])) {
                matrixdimx = scalex[scalex.length-1];
            } else if (parseFloat(matrixdimx) <= parseFloat(scalex[0])) {
                matrixdimx = scalex[0];
            } else {
                for(i=0; i<scalex.length-1; i++) {
                    if (parseFloat(scalex[i]) <= parseFloat(matrixdimx) && parseFloat(scalex[i+1]) > parseFloat(matrixdimx)) {
                        matrixdimx = scalex[i];
                    }
                }
            }
        }
        log('matrixdimx='+matrixdimx, 3);

        if (matrixopy == 'number higher') {
            if (parseFloat(matrixdimy) > parseFloat(scaley[scaley.length-1])) {
                matrixdimy = scaley[scaley.length-1];
            } else if (parseFloat(matrixdimy) <= parseFloat(scaley[0])) {
                matrixdimy = scaley[0];
            } else {
                for(i=1; i<scaley.length; i++) {
                    if (parseFloat(scaley[i-1]) < parseFloat(matrixdimy) && parseFloat(scaley[i]) >= parseFloat(matrixdimy)) {
                        matrixdimy = scaley[i];
                    }
                }
            }
        }
        log('matrixdimy='+matrixdimy, 3);

        if (matrixopy == 'number lower') {
            if (parseFloat(matrixdimy) > parseFloat(scaley[scaley.length-1])) {
                matrixdimy = scaley[scaley.length-1];
            } else if (parseFloat(matrixdimy) <= parseFloat(scaley[0])) {
                matrixdimy = scaley[0];
            } else {
                for(i=0; i<scaley.length-1; i++) {
                    if (parseFloat(scaley[i]) <= parseFloat(matrixdimy) && parseFloat(scaley[i+1]) > matrixdimy) {
                        matrixdimy = scaley[i];
                    }
                }
            }
        }
        log('matrixdimy='+matrixdimy, 3);
        log(matrix, 3);
        log(matrix[matrixdimx], 3);

        if (matrix[matrixdimx] != undefined && matrix[matrixdimx][matrixdimy] != undefined)
            value = matrix[matrixdimx][matrixdimy];

        if (typeof value == 'number'){
            value = value.toFixed(4);
        }

        return value;
    }
}

/* class to handle the reload of the combined product image */
CombinedImage = Class.create();
CombinedImage.prototype = {
    initialize : function(refreshUrl) {
        this.refreshUrl = refreshUrl;
    },
    refresh : function(configuratorid) {
        if(Configuration.config.preventImageRefresh) {
            Configuration.config.combinedimagerefreshinterval = 0;
            return false;
        }
        AjaxLoader.show();

        var data = jQuery('#product_addtocart_form').serializeArray();
        jQuery('select:disabled,input:disabled,input:hidden').each(function(i,item) {
            data.push({ name: jQuery(item).attr('name'), value: jQuery(item).val() });
        });
        data.push({
            name : "jstemplateoption",
            value : configuratorid
        });
        data.push({
            name : "product",
            value : Configuration.getInstanceByJsId(configuratorid).getProductId()
        });
        data.push({
            name : "productoptionid",
            value : Configuration.getInstanceByJsId(configuratorid).getOptionId()
        });
        //alert(print_r(decodeURI(jQuery.param(data))));
        //alert(this.refreshUrl);
        jQuery.ajax({
            type : "POST",
            url : this.refreshUrl,
            data : jQuery.param(data)
        }).done(function(data) {
                if (data) {
                    if (data.length > 0) {
                        var time = new Date().getTime();
                        try {
                            data = JSON.parse(data);
                            var image_file = data['image'] + '?t=' + time;
                            var delivery_time = data['delivery_time'];
                        } catch(e) {
                            var image_file = data;
                            var delivery_time = 'DEFAULT';
                        }
                        
                        jQuery('.product-view .product-image img').fadeOut(300, function() {
                            jQuery('.product-view .product-image img').attr("src", image_file);
                        }).fadeIn(300, function() {
                            var zoomContainer = jQuery('.zoomContainer');
                            if(!(undefined === zoomContainer))
                                zoomContainer.remove();
                            jQuery('.product-view img#image').elevateZoom({
                                scrollZoom: true,
                                zoomLevel: 1,
                                cursor: "none",
                                lensBorderColor: '#f00',                                
                                lensBorderSize: 2,
                                zoomWindowWidth: 545,
                                zoomWindowHeight: 545,
                                /*zoomWindowOffsetx: 1,
                                zoomWindowOffsety: 0,*/
                                zoomWindowPosition: "product-container>.col_right",
                                borderColor: "#989898",
                                borderSize: 1
                            });
                        });
                        
                        if(delivery_time != 'DEFAULT') {
                            if(!delivery_time || !delivery_time.length || (delivery_time.toLowerCase() == 'null') || (delivery_time == '0')) {
                                jQuery('span.delivery-time-label').hide();
                                delivery_time = 'sofort lieferbar';
                            } else
                                jQuery('span.delivery-time-label').show();
                            jQuery('span.delivery-time-info').text(delivery_time);
                        }
                        
                        AjaxLoader.hide();
                    }
                }
                Configuration.config.combinedimagerefreshinterval = 0;
                log("combined refresh done",2);
            });
        return false;
    }
}

/* static object to handle the ajax loading animation */
var AjaxLoader = {
    config : {
        counter : '0'
    },
    show : function(loader) {
        activity_overlay = jQuery('#activity-overlay');
        if (activity_overlay.length == 0) {
            jQuery("body")
                .append(
                    '<div id="activity-overlay" style="background: black; position: fixed; opacity: 0.4; filter: alpha(opacity=50); top: 0; left: 0; z-index: 20045; width: 100%; height: 100%;"></div>');
        } else {
            activity_overlay.show();
        }
        if (loader) {
            activity_overlay.activity({
                color : '#fff'
            });
        }
        PriceLoader.show();

        AjaxLoader.config.counter = parseInt(AjaxLoader.config.counter) + 1;
        log('show counter ' + AjaxLoader.config.counter, 3);
    },
    hide : function() {
        AjaxLoader.config.counter = parseInt(AjaxLoader.config.counter) - 1;
        log('hide counter ' + AjaxLoader.config.counter, 3);
        if (AjaxLoader.config.counter == 0) {
            log('off', 3);
            PriceLoader.hide();
            jQuery('#activity-overlay').activity(false).hide();
        }
    }
};

var PriceLoader = {
    config : {
        counter : '0'
    },
    show : function() {
        if (PriceLoader.config.counter > 0) return 0;
        price_box = jQuery(".product-view .price-box");
        price_box.find("span").hide();
        price_box.append("<span class='loading-price'><img src='"+Configuration.config.urlpriceloadimage+"'/></span>");

        loading_box = jQuery("span.delivery-time-details>span.loading-price");
        if(loading_box)
            loading_box.remove();
        jQuery('span.delivery-time-label').show();
        delivery_box = jQuery("span.delivery-time-info");
        delivery_box.hide();
        delivery_box.before("<span class='loading-price'><img src='"+Configuration.config.urlpriceloadimage+"'/></span>");
    },
    hide : function() {
        loading_box = jQuery(".loading-price");
        loading_box.hide();

        jQuery(".product-view .price-box").find("span").show();
        jQuery("span.delivery-time-info").show();
        loading_box.remove();
    }
}

/* Change jQuery UI content method to handle HTML content */
jQuery.widget("ui.tooltip", jQuery.ui.tooltip, {
    options: {
        content: function () {
            return jQuery(this).prop('title');
        }
    }
});

/* Add event at first place of event handlers list */
jQuery.fn.bindFirst = function(name, fn) {
    this.bind(name, fn);
   if (this.data('events')) {
        var handlers = this.data('events')[name.split('.')[0]];
        var handler = handlers.pop();
        handlers.splice(0, 0, handler);
   }
};