/**
/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/


jQuery.fn.sort_select_box = function(){
    var my_options = jQuery("#" + this.attr('id') + ' option');
    var selected = jQuery("#" + this.attr('id')).val();
    my_options.sort(function(a,b) {
        var a_index = jQuery(a).attr("attr-index");
        if (!a_index) a_index = -1;
        var b_index = jQuery(b).attr("attr-index");
        if (!b_index) b_index = -1;

        if (a_index > b_index) return 1;
        else if (a_index < b_index) return -1;
        else return 0
    })
    jQuery(this).empty().append( my_options );
    jQuery("#"+this.attr('id')).val(selected);
}


Blacklist = Class.create();
Blacklist.prototype = {
    initialize : function(js_template_id, template_id, product_option_id, url) {
        this.js_template_id = js_template_id;
        this.template_id = template_id;
        this.product_option_id = product_option_id;

        this.active_blacklist_options = new Array();
        this.option_data = new Array();
        this.active_blacklist_values = new Array();
        this.active_tags = new Array();

        this.values = null;
        this.tags = null;
        this.expressions = null;
        this.options_and_values = null;
        this.values_and_option = null;
        this.option_and_children = null;
        this.option_and_alt_title = null;
        this.options_defaults = null;
        this.options_combined_image = null;
        this.options_type = null;
        this.blacklist_mode = 1;
        this.url = null;

        var config = this.loadJsonConfig(url);

        if (config.blacklist_values)
            this.values = config.blacklist_values;
        if (config.blacklist_tags)
            this.tags = config.blacklist_tags;
        if (config.blacklist_expressions)
            this.expressions = config.blacklist_expressions;

        if (config.options_and_values) // An array with all options and there values (array)
            this.options_and_values = config.options_and_values;
        if (config.values_and_option) // An array with all values and there option-id
            this.values_and_option = config.values_and_option;
        if (config.option_and_children) // An array with all relevant children of an option for blacklisting
            this.option_and_children = config.option_and_children;
        if (config.option_and_alt_title) // An array with the alt_title for any option
            this.option_and_alt_title = config.option_and_alt_title;
        if (config.options_defaults) // Option defaults
            this.options_defaults = config.options_defaults;
        if (config.options_type) // Option Type
            this.options_type = config.options_type;
        if (config.blacklist_tag_values) // Value Tags
            this.blacklist_tag_values = config.blacklist_tag_values;
        if (config.blacklist_mode) // 1 = Hide Option Values, 2 = Disable Option Values
            this.blacklist_mode = parseFloat(config.blacklist_mode);
        if (config.options_combined_image)
            this.options_combined_image = config.options_combined_image;
    },
    loadJsonConfig: function(url) {
        var config;
        log("loading blacklist json from "+url, 1);
        var success = false;
        var retry = 3;

        while (!success && retry > 0) {
            log("retry "+retry,1);
            jQuery.ajax({
                url: url,
                dataType : 'json',
                async : false,
                error: function(request, text, errstr) {
                    log("error loading json config for blacklisting "+text+" "+errstr, -1);
                    success = false;
                    retry--;
                }
            }).done(function(data) {
                log("blacklist json config was loaded", 1);
                config = data;
                success= true;
            });
        }
        return config;
    },
    resetBlacklisting: function() {
        this.active_blacklist_options = new Array();
        this.active_blacklist_values = new Array();
        this.active_tags = new Array();
        this.option_data = new Array();
    },
    getJsTemplateId: function() {
        return this.js_template_id;
    },
    getTemplateId: function() {
        return this.template_id;
    },
    getproductOptionId: function() {
        return this.product_option_id;
    },
    isEmpty: function (value){
        return (value == null || value.length === 0);
    },
    /*
     * This is the main callback for the blacklisting
     * Will check if there is some active blacklisting for the changed value
     */
    blacklistCallback: function(tag_id, option_id, type) {
        block_ondocument_events = true;
        log('blacklist callback started', 2);

        /* register the data of the option in array to handle later updates */
        if (!(this.option_data[option_id] instanceof Array)) {
            this.option_data[option_id] = new Array();
            this.option_data[option_id]["tag_id"] = tag_id;
            this.option_data[option_id]["type"] = type;
        }

        var combined_update = false;
        if (this.options_combined_image[option_id]) {
            combined_update = true;
        }

        var value_id = null;
        var value_type = "id";
        switch (type) {
            case "radiobuttons":
                value_id= parseFloat(jQuery("#"+tag_id).attr("attr-value-id"));
                break;
            case "checkbox":
                value = OptionCheckbox.getValue(tag_id);
            case "text":
            case "area":
            case "date":
            case "webservice":
            case "expression":
            case "matrixvalue":
            case "combi":
                value_type = "text";
                value_id = parseFloat(jQuery("#"+tag_id).val());
                break;
            case "listimage":
            case "listimagecombi":
            case "select":
            case "overlayimage":
            case "overlayimagecombi":
            case "selectimage":
            case "selectcombi":
            default:
                value_id = parseFloat(jQuery("#"+tag_id).val());
                if (isNaN(value_id)) {
                    value_id = 0;
                }
                break;
        }

        if (!this.isEmpty(value_id)) {
            if (value_type == "id" && this.values && this.values[value_id]) { // Ok, the value has some blacklisting

                if (this.values[value_id]["blacklist_options"]) { // Ok, there are options to set to the blacklist
                    for( var i=0; i<this.values[value_id]["blacklist_options"] .length; i++ ) {
                        this.addOptionToBlackist(
                            this.values[value_id]["blacklist_options"][i]["id"],
                            this.values[value_id]["blacklist_options"][i]["alt_title"],
                            this.getOptionTagId(this.values[value_id]["blacklist_options"][i]["id"]),
                            option_id,
                            value_id
                        );
                    }
                }

                if (this.values[value_id]["blacklist_values"]) { // Ok, there are values to set to the blacklist
                    for( var i=0; i<this.values[value_id]["blacklist_values"] .length; i++ ) {
                        this.addValueToBlacklist(
                            this.values[value_id]["blacklist_values"][i],
                            this.values_and_option[this.values[value_id]["blacklist_values"][i]],
                            this.options_type[this.values_and_option[this.values[value_id]["blacklist_values"][i]]],
                            this.getOptionTagId(this.values_and_option[this.values[value_id]["blacklist_values"][i]]),
                            option_id,
                            value_id
                        );
                        if (this.options_combined_image[this.values_and_option[this.values[value_id]["blacklist_values"][i]]]) {
                            combined_update = true;
                        }
                    }
                }
            }

            if (value_type == "text" && this.expressions && this.expressions[option_id]) { // Ok, the value has some expression blacklisting
                for( var i=0; i<this.expressions[option_id].length; i++ ) {
                    var value = this.expressions[option_id][i]["value"];
                    var operator = this.expressions[option_id][i]["operator"];
                    var blacklist_value_id = this.expressions[option_id][i]["value_id"];

                    var status = false;
                    switch (operator) {
                        case "<":
                            if (value_id < value)
                                status = true;
                            break;
                        case "<=":
                            if (value_id <= value)
                                status = true;
                            break;
                        case ">":
                            if (value_id > value)
                                status = true;
                            break;
                        case ">=":
                            if (value_id >= value)
                                status = true;
                            break;
                        case "=":
                            if (value_id == value)
                                status = true;
                            break;
                        case "!=":
                            if (value_id != value)
                                status = true;
                            break;
                    }

                    if (status == true) {
                        this.addValueToBlacklist(
                            blacklist_value_id,
                            this.values_and_option[blacklist_value_id],
                            this.options_type[this.values_and_option[blacklist_value_id]],
                            this.getOptionTagId(this.values_and_option[blacklist_value_id]),
                            option_id,
                            0
                        );
                    } else {
                        /* Check if the value is currently blacklisted by the option */
                        var id = parseFloat(blacklist_value_id);
                        if (this.active_blacklist_values[id]) {
                            var element = this.active_blacklist_values[id]["from_id"];
                            if (jQuery.inArray(this.getBlacklistFromId(option_id, 0), element) != -1) {
                                this.removeValueFromBlacklist(id, option_id,  0, this.options_type[this.values_and_option[id]]);
                            }
                        }
                    }
                }
            }

            if (this.cleanBlacklistForOption(option_id, value_id, type)) {
                combined_update = true;
            }

            /* Add active tags and and values to blacklist */
            if (this.tags && this.tags[value_id]) {
                for( var i=0; i<this.tags[value_id].length; i++ ) {
                    var value = this.tags[value_id][i];
                    if (jQuery.inArray(value, this.active_tags) == -1) {
                        var element = new Array();
                        element["tag"] = value;
                        element["from_id"] = new Array();
                        element["from_id"].push(this.getBlacklistFromId(this.values_and_option[value_id], value_id));
                        this.active_tags[value] = element;

                        if (this.blacklist_tag_values[value]) {
                            for( var n=0; n<this.blacklist_tag_values[value].length; n++ ) {
                                var id = parseFloat(this.blacklist_tag_values[value][n]);
                                var opt_id = this.values_and_option[id];
                                this.addValueToBlacklist(
                                    id,
                                    opt_id,
                                    this.options_type[opt_id],
                                    this.getOptionTagId(opt_id),
                                    this.values_and_option[value_id],
                                    value_id
                                );
                                if (this.options_combined_image[opt_id]) {
                                    combined_update = true;
                                }
                            }
                        }

                    } else {
                        this.active_tags[value]["from_id"].push(value_id);
                    }
                }
            }
        }

        log("combined update: opt-id="+option_id+" is "+combined_update,2);
        if (combined_update) {
            var instance = Configuration.getInstanceByJsId(this.js_template_id);
            if (instance.getCombinedImage()) {
                if (Configuration.config.combinedimagerefreshinterval == 0) {
                    log("refresh start timer",2);
                    Configuration.config.combinedimagerefreshinterval = 1;
                    var code = instance.getCombinedImage().refresh(this.getJsTemplateId());
                    if (code) { window.setTimeout(code, 100); }
                }
            }
        }

        block_ondocument_events = false;
    },

    /*
     * Cleanup the active blacklist for the given option
     */
    cleanBlacklistForOption: function(option_id, value_id, type) {
        var combined_update = false;
        var values = this.options_and_values[parseFloat(option_id)];
        if (values instanceof Array) {
            for (var i=0; i<values.length; i++) {
                var current_value_id = values[i];
                if (current_value_id != value_id) {

                    /* Options */
                    if (this.values && this.values[current_value_id] && this.values[current_value_id]["blacklist_options"]) {
                        var current_value = this.values[current_value_id];
                        for( var n=0; n<current_value["blacklist_options"] .length; n++ ) {
                            var current_option_id = parseFloat(current_value["blacklist_options"][n]["id"]);
                            if (this.active_blacklist_options[current_option_id])  {
                                var element = this.active_blacklist_options[current_option_id]["from_id"];
                                if (jQuery.inArray(this.getBlacklistFromId(option_id, current_value_id), element) != -1) {
                                    /* Show the option, add to the DOM */
                                    this.removeOptionFromBlacklist(current_option_id, option_id, current_value_id);
                                    if (this.options_combined_image[option_id]) {
                                        combined_update = true;
                                    }
                                    Configuration.setOptionPriceAndReload(this.js_template_id, current_option_id, Configuration.getPriceOfOption(this.getOptionTagId(current_option_id), this.options_type[current_option_id]));
                                }
                            }
                        }
                    }

                    /* Values */
                    if (this.values && this.values[values[i]] && this.values[values[i]]["blacklist_values"]) {
                        var current_value = this.values[values[i]];
                        for( var n=0; n<current_value["blacklist_values"] .length; n++ ) {
                            var blacklist_value_id = current_value["blacklist_values"][n];
                            if (this.active_blacklist_values[blacklist_value_id])  {
                                var element = this.active_blacklist_values[blacklist_value_id]["from_id"];
                                if (jQuery.inArray(this.getBlacklistFromId(option_id, current_value_id ), element) != -1) {
                                    /* Show the value, add to the DOM */
                                    this.removeValueFromBlacklist(blacklist_value_id, option_id, current_value_id, this.options_type[this.values_and_option[blacklist_value_id]]);
                                    if (this.options_combined_image[option_id]) {
                                        combined_update = true;
                                    }
                                }
                            }
                        }
                    }

                    /* Tags */
                    var my_value = values[i];
                    if (this.tags && this.tags[my_value]) {

                        for( var n=0; n<this.tags[my_value].length; n++ ) {
                            var value = this.tags[my_value][n];
                            if (this.active_tags[value]) {
                                var pos = jQuery.inArray(this.getBlacklistFromId(this.values_and_option[my_value], my_value), this.active_tags[value]["from_id"]);
                                if (pos != -1) {
                                    this.active_tags[value]["from_id"].splice(pos,1);
                                    if (this.active_tags[value]["from_id"].length == 0) {
                                        delete this.active_tags[value];
                                        /* TODO show values for this tag */
                                        for (k=0; k<this.blacklist_tag_values[value].length; k++) {
                                            var blacklist_value_id = this.blacklist_tag_values[value][k];
                                            var from_id = this.getBlacklistFromId(this.values_and_option[my_value], my_value);
                                            var from_array  = this.active_blacklist_values[blacklist_value_id]["from_id"];
                                            if (jQuery.inArray(from_id, from_array) != -1) {
                                                this.removeValueFromBlacklist(parseFloat(blacklist_value_id),  this.values_and_option[my_value], my_value, this.options_type[this.values_and_option[parseFloat(blacklist_value_id)]]);
                                                if (this.options_combined_image[this.values_and_option[parseFloat(blacklist_value_id)]]) {
                                                    combined_update = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /* Check, if there are any values that have to be hidden and are not, do it now */
        /* Could happen in the case that the option that is blacklisting is shown before the value to be hidden is rendered */
        for (var value_key in this.active_blacklist_values) {
            var element = this.active_blacklist_values[value_key];
            if (element["hide"] == 0 && element["from_id"].length > 0) {
                html = this.hideValueFromDOM(this.getOptionTagId(this.values_and_option[value_key]), value_key, type);
                /* Todo Store pos of the element */
                element["html"] = html;
                element["hide"] = 1;

                /* Reload Price */
                var value_option_id = this.values_and_option[value_key];
                var type = this.options_type[value_option_id];
                var tag = this.getOptionTagId(value_option_id);
                Configuration.setOptionPriceAndReload(this.js_template_id, value_option_id, Configuration.getPriceOfOption(tag, type));
            }
        }

        return combined_update;
    },


    /*
     *
     *  Handling of blacklist values
     *
     */


    /*
     * Add an value to the blacklist
     */
    addValueToBlacklist: function(value_id, option_id, option_type, option_tag_id, from_id, from_value_id) {
        /* Init the array */
        if (!(this.active_blacklist_values[value_id] instanceof Array)) {
            this.active_blacklist_values[value_id] = new Array();
            this.active_blacklist_values[value_id]["from_id"] = new Array();
            this.active_blacklist_values[value_id]["hide"] = 0;
        }

        /* get pos of value */
        var pos = this.getValuePos(option_tag_id, value_id, option_type);

        /* If this is the first blacklisting hide the value from the DOM */
        if (this.active_blacklist_values[value_id]["hide"] == 0 && pos != -1) {
            this.active_blacklist_values[value_id]["pos"] = pos;
            this.active_blacklist_values[value_id]["hide"] = 1;
            switch (this.blacklist_mode) {
                case 2:
                    this.active_blacklist_values[value_id]["html"] = this.disableOptionValue(option_tag_id, value_id, option_type);
                    break;
                default:
                    this.active_blacklist_values[value_id]["html"] = this.hideValueFromDOM(option_tag_id, value_id, option_type);
                    break;
            }

        }

        /* Add value-id to the active blacklisting array */
        if (jQuery.inArray(this.getBlacklistFromId(from_id, from_value_id), this.active_blacklist_values[value_id]["from_id"]) == -1) {
            this.active_blacklist_values[value_id]["from_id"].push(this.getBlacklistFromId(from_id, from_value_id));
        }

        /* Reload Price */
        var value_option_id = this.values_and_option[value_id];
        var type = this.options_type[value_option_id];
        var tag = this.getOptionTagId(value_option_id);
        Configuration.setOptionPriceAndReload(this.js_template_id, value_option_id, Configuration.getPriceOfOption(tag, type));
    },

    getValuePos: function(option_tag_id, value_id, type) {
        switch (type) {
            case "select":
            case "selectimage":
            case "selectcombi":
            case "listimage":
            case "listimagecombi":
            case "overlayimage":
            case "overlayimagecombi":
                var pos = jQuery('#'+option_tag_id+' option[attr-value-id="'+value_id+'"]').index();
                return pos;
                break;
            case "radiobuttons":
                var element = jQuery('#'+option_tag_id).parent().find('input[type="radio"][attr-value-id="'+value_id+'"]');
                var pos = jQuery(element).attr("attr-index");
                return pos;
                break;
        }

        return -1;

    },

    /*
     * Remove an value from the blacklist
     */
    removeValueFromBlacklist: function(value_id, from_id, from_value_id, type) {
        this.active_blacklist_values[value_id]["from_id"].splice(jQuery.inArray(this.getBlacklistFromId(from_id, from_value_id), this.active_blacklist_values[value_id]["value_id"]),1);

        // if the blacklist is empty, then add the value to the DOM again
        if (this.active_blacklist_values[value_id]["from_id"].length == 0) {
            switch (this.blacklist_mode) {
                case 2:
                    this.enableOptionValue(
                        this.getOptionTagId(this.values_and_option[value_id]),
                        this.active_blacklist_values[value_id]["pos"],
                        this.active_blacklist_values[value_id]["html"],
                        type
                    );
                    break;
                default:
                    this.showValueInDOM(
                        this.getOptionTagId(this.values_and_option[value_id]),
                        this.active_blacklist_values[value_id]["pos"],
                        this.active_blacklist_values[value_id]["html"],
                        type
                    );
                    break;
            }

            this.active_blacklist_values[value_id]["hide"] = 0;
        }

        /* Reload Price */
        var value_option_id = this.values_and_option[value_id];
        var type = this.options_type[value_option_id];
        var tag = this.getOptionTagId(value_option_id);
        Configuration.setOptionPriceAndReload(this.js_template_id, value_option_id, Configuration.getPriceOfOption(tag, type));    },

    /*
     * Hide an value from the DOM if option is type select
     */
    hideValueFromDOM: function(option_tag_id, value_id, type) {
        var html;
        switch (type) {
            case "select":
            case "selectcombi":
                html = jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').detach();
                break;
            case "overlayimage":
            case "overlayimagecombi":
                html = jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').detach();
                OptionOverlayImage.buildOverlay(option_tag_id);
                break;
           case "selectimage":
                html = jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').detach();
                OptionSelectImage.setThumbnailImage(option_tag_id);
                break;
            case "radiobuttons":
                html = jQuery('#input-wrapper-'+option_tag_id+'-'+value_id).detach();
                break;
            case "listimage":
            case "listimagecombi":
                html = jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').detach();
                var hide_select = jQuery('#'+option_tag_id).attr("hide-select");
                if (hide_select == "false") {
                    hide_select = false;
                }
                var show_labels = jQuery('#'+option_tag_id).attr("show-label");
                if (show_labels == "false") {
                    show_labels = false;
                }
                jQuery('#'+option_tag_id).next().remove();
                jQuery('#'+option_tag_id).imagepicker({
                    hide_select : hide_select,
                    show_label  : show_labels
                });
                break;
        }

        /* update summary */
        Configuration.updateSummary(option_tag_id);

        return html;
    },

    /*
     * Disable an option in select and radio, hide from dom, if it is a listimage-type
     */
    disableOptionValue: function(option_tag_id, value_id, type) {
        var html;
        switch (type) {
            case "select":
            case "selectcombi":
                html = "disabled";
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').attr("disabled","disabled");
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').removeAttr("selected");
                break;
            case "overlayimage":
            case "overlayimagecombi":
                html = "disabled";
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').attr("disabled","disabled");
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').removeAttr("selected");
                OptionOverlayImage.buildOverlay(option_tag_id);
                break;
            case "selectimage":
                html = "disabled";
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').attr("disabled","disabled");
                jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').removeAttr("selected");
                OptionSelectImage.setThumbnailImage(option_tag_id);
                break;
            case "radiobuttons":
                html = "disabled";
                jQuery('#input-wrapper-'+option_tag_id+'-'+value_id+' input').attr("disabled","disabled");
                jQuery('#input-wrapper-'+option_tag_id+'-'+value_id+' input').next().addClass("disabled");
                jQuery('#input-wrapper-'+option_tag_id+'-'+value_id+' input').removeAttr("checked");
                break;
            case "listimage":
            case "listimagecombi":
                html = jQuery('#'+option_tag_id+' option[value="'+value_id+'"]').detach();
                var hide_select = jQuery('#'+option_tag_id).attr("hide-select");
                if (hide_select == "false") {
                    hide_select = false;
                }
                var show_labels = jQuery('#'+option_tag_id).attr("show-label");
                if (show_labels == "false") {
                    show_labels = false;
                }
                jQuery('#'+option_tag_id).next().remove();
                jQuery('#'+option_tag_id).imagepicker({
                    hide_select : hide_select,
                    show_label  : show_labels
                });
                break;
        }

        /* update summary */
        Configuration.updateSummary(option_tag_id);

        return html;
    },

    /*
     * Add a previously hidden value to the DOM again
     */
    adjustPos: function(pos, count) {
        if (pos > count) {
            pos = count;
        }
        if (pos == 0 || pos == undefined) {
            pos = 1;
        }
        return pos;
    },
    showValueInDOM: function(option_tag_id, pos, document, type) {
        switch (type) {
            case "select":
            case "selectcombi":
                var html = jQuery('<div>').append(jQuery(document).clone()).html();
                jQuery('#'+option_tag_id).prepend(html);
                jQuery('#'+option_tag_id).sort_select_box();
                break;
            case "overlayimage":
            case "overlayimagecombi":
                var html = jQuery('<div>').append(jQuery(document).clone()).html();
                jQuery('#'+option_tag_id).prepend(html);
                jQuery('#'+option_tag_id).sort_select_box();
                OptionOverlayImage.buildOverlay(option_tag_id);
                break;
            case "selectimage":
                var html = jQuery('<div>').append(jQuery(document).clone()).html();
                jQuery('#'+option_tag_id).prepend(html);
                jQuery('#'+option_tag_id).sort_select_box();
                OptionSelectImage.setThumbnailImage(option_tag_id);
                break;
            case "radiobuttons":
                /* TODO Insert at previous index pos */
                var parent = jQuery('#'+option_tag_id).parent();
                document.appendTo(parent);
                break;
            case "listimage":
            case "listimagecombi":
                var html = jQuery('<div>').append(jQuery(document).clone()).html();
                jQuery('#'+option_tag_id).prepend(html);
                jQuery('#'+option_tag_id).sort_select_box();

                /* List Image */
                var hide_select = jQuery('#'+option_tag_id).attr("hide-select");
                if (hide_select == "false") {
                    hide_select = false;
                }
                var show_labels = jQuery('#'+option_tag_id).attr("show-label");
                if (show_labels == "false") {
                    show_labels = false;
                }
                jQuery('#'+option_tag_id).next().remove();
                jQuery('#'+option_tag_id).imagepicker({
                    hide_select : hide_select,
                    show_label  : show_labels
                });
                break;
        }

        /* update summary */
        Configuration.updateSummary(option_tag_id);
    },

    enableOptionValue: function(option_tag_id, pos, document, type) {
        switch (type) {
            case "select":
            case "selectcombi":
                jQuery('#'+option_tag_id).find("option:eq("+(pos)+")").removeAttr("disabled");
                break;
            case "overlayimage":
            case "overlayimagecombi":
                jQuery('#'+option_tag_id).find("option:eq("+(pos)+")").removeAttr("disabled");
                OptionOverlayImage.buildOverlay(option_tag_id);
                break;
            case "selectimage":
                jQuery('#'+option_tag_id).find("option:eq("+(pos)+")").removeAttr("disabled");
                OptionSelectImage.setThumbnailImage(option_tag_id);
                break;
            case "radiobuttons":
                jQuery('#'+option_tag_id).parent().find('input[attr-index="'+pos+'"]').removeAttr("disabled");
                jQuery('#'+option_tag_id).parent().find('input[attr-index="'+pos+'"]').next().removeClass("disabled");
                break;
            case "listimage":
            case "listimagecombi":
                var html = jQuery('<div>').append(jQuery(document).clone()).html();
                var count = jQuery('#'+option_tag_id).children().length;
                pos = this.adjustPos(pos, count);
                if (count > 0) {
                    jQuery('#'+option_tag_id).find("option:eq("+(pos-1)+")").after(html);
                } else {
                    jQuery('#'+option_tag_id).prepend(html);
                }

                /* List Image */
                var hide_select = jQuery('#'+option_tag_id).attr("hide-select");
                if (hide_select == "false") {
                    hide_select = false;
                }
                var show_labels = jQuery('#'+option_tag_id).attr("show-label");
                if (show_labels == "false") {
                    show_labels = false;
                }
                jQuery('#'+option_tag_id).next().remove();
                jQuery('#'+option_tag_id).imagepicker({
                    hide_select : hide_select,
                    show_label  : show_labels
                });
                break;
        }
    },

    /*
     *
     *  Handling of blacklist options
     *
     */


    /*
     * Add an option to the blacklist
     */
    addOptionToBlackist: function(option_id, option_alt_title, tag_id, from_id, from_value_id) {
        /* Delete file uploads */
        var deleteElements = jQuery('#file-wrapper-' + tag_id).find(".uploadifytag");
            if (deleteElements.length >0 ) {
            jQuery.each(deleteElements, function() {
                JustsellingUplodify.helper.deleteUpload(this);
                var id = jQuery(this).attr('id');
                var id = id.replace('uploadifytag', '');
                jQuery("#uplodifyimg" + id).remove();
                jQuery("#uploadifytag" + id).remove();
            });
            jQuery('#file-wrapper-' + tag_id).find("div.hideuplodifybutton").removeClass("hideuplodifybutton");
        }

        /* Init the array */
        if (!(this.active_blacklist_options[option_id] instanceof Array)) {
            this.active_blacklist_options[option_id] = new Array();
            this.active_blacklist_options[option_id]["from_id"] = new Array();
        }

        /* Check if there are any children of the option to blacklist too */
        if (this.option_and_children[option_id]) {
            for (var i=0; i<this.option_and_children[option_id].length; i++) {
                this.addOptionToBlackist(
                    this.option_and_children[option_id][i],
                    this.option_and_alt_title[this.option_and_children[option_id][i]],
                    this.getOptionTagId(this.option_and_children[option_id][i]),
                    from_id,
                    from_value_id
                );
            }
        }

        /* If this is the first blacklisting hide the option from the DOM */
        if (this.active_blacklist_options[option_id]["from_id"].length == 0) {
            this.active_blacklist_options[option_id]["html"] = this.hideOption(option_alt_title);
            this.active_blacklist_options[option_id]["alt_title"] = option_alt_title;
        }

        /* Add option-id to the active blacklisting array */
        if (jQuery.inArray(this.getBlacklistFromId(from_id, from_value_id), this.active_blacklist_options[option_id]["from_id"]) == -1) {
            this.active_blacklist_options[option_id]["from_id"].push(this.getBlacklistFromId(from_id, from_value_id));
        }

        /* Reload Price */
        var type = this.options_type[option_id];
        Configuration.setOptionPriceAndReload(this.js_template_id, option_id, Configuration.getPriceOfOption(tag_id, type));

        /* update summary */
        Configuration.updateSummary(tag_id);
    },

    /*
     * Remove an option from the blacklist
     */
    removeOptionFromBlacklist: function(option_id, from_id, from_value_id) {
        var from_list_count = this.active_blacklist_options[option_id]["from_id"].length;
        this.active_blacklist_options[option_id]["from_id"].splice(jQuery.inArray(this.getBlacklistFromId(from_id, from_value_id), this.active_blacklist_options[option_id]["from_id"]),1);

        /* Check if there are any children of the option to remove from the blacklist too */
        if (this.option_and_children[option_id]) {
            for (var i=0; i<this.option_and_children[option_id].length; i++) {
                var child_option_id = this.option_and_children[option_id][i];
                this.removeOptionFromBlacklist(
                    child_option_id,
                    from_id,
                    from_value_id
                );
            }
        }

        // if the blacklist is empty, then add the option to the DOM again
        if (from_list_count > 0 && this.active_blacklist_options[option_id]["from_id"].length == 0) {
            this.showOption(this.active_blacklist_options[option_id]["alt_title"], this.active_blacklist_options[option_id]["html"]);
            if (this.options_defaults[option_id]) {
                this.setOptionDefaultValue(option_id, this.options_defaults[option_id]);
            }
        }

        /* Call blacklist callback to handle blacklisting when showing the option after blacklisting */
        if (this.option_data[option_id]) {
            this.blacklistCallback(this.option_data[option_id]["tag_id"], option_id, this.option_data[option_id]["type"]);
        }
        /* update summary */
        Configuration.updateSummary(this.getOptionTagId(option_id));
    },

    /*
     * Set default value for an option
     */
    setOptionDefaultValue: function(option_id, value) {
        if (this.option_data[option_id]) {
            switch (this.option_data[option_id]["type"]) {
                /* TODO: other relevant option type to get value */
                case "select":
                    jQuery("#"+this.option_data[option_id]["tag_id"]).val(value);
                    break;
            }
        }
    },

    /*
     * Hide an option from the DOM
     */
    hideOption: function(option_alt_title) {
        var div_id = "wrapper-"+option_alt_title+"-"+this.js_template_id;
        var main_element = jQuery("#"+div_id);
        var html = jQuery(main_element).find('.option-details').detach();
        jQuery(main_element).empty();
        return html;
    },

    /*
     * Add a previously hidden option to the DOM again
     */
    showOption: function(option_alt_title, html) {
        var div_id = "wrapper-"+option_alt_title+"-"+this.js_template_id;
        var main_element = jQuery("#"+div_id);
        jQuery(main_element).append(html);

        /* File Upload */
        var file_element = jQuery(main_element).find(".file-wrapper");
        if (jQuery(file_element).length > 0) {
            if (jQuery(file_element).find(".uploadifive-button").length < 1) {
                var uploadArrayIndex = option_alt_title + "-" +this.js_template_id;
                var uploadObject = uploadArray[uploadArrayIndex];
                var optionId = uploadObject.optionId;
                var jsTemplateId = uploadObject.jsTemplateId;
                var buttonImage = uploadObject.buttonImage;
                var optionFiletype = uploadObject.optionFiletype;
                var optionMaxsize = uploadObject.optionMaxsize;
                var id = uploadObject.id;
                var validationClasses = uploadObject.validationClasses;
                var optionMulti = uploadObject.optionMulti;
                var sessionId =  uploadObject.sessionId;
                var mimetypes = uploadObject.mimetypes;
                JustsellingUploadJsPhtml.initFile(optionId, jsTemplateId, optionFiletype, optionMaxsize, id, validationClasses, optionMulti,sessionId, mimetypes);
            }
        }

        /* List Image */
       jQuery(main_element).find("select.listimage").each(function() {
            var option_tag_id  = jQuery(this).attr("id");
            var hide_select = jQuery('#'+option_tag_id).attr("hide-select");
            if (hide_select == "false") {
                hide_select = false;
            }
            var show_labels = jQuery('#'+option_tag_id).attr("show-label");
            if (show_labels == "false") {
                show_labels = false;
            }
            jQuery('#'+option_tag_id).next().remove();
            jQuery('#'+option_tag_id).imagepicker({
                hide_select : hide_select,
                show_label  : show_labels
            });
            jQuery(option_tag_id).css("position","absolute");
            jQuery(option_tag_id).css("left","-99999px");
        });
    },

    /*
     * Build the option tag id
     */
    getOptionTagId: function(id) {
        return "options-"+this.product_option_id+"-"+this.js_template_id+"-template-"+id;
    },

    getBlacklistFromId: function(option_id, value_id) {
        return parseFloat(option_id) * 100000 + value_id;
    }

}