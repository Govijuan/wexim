/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

if(typeof Product=='undefined') {
    var Product = {};
}

Product.TemplateOptions = Class.create();
Product.TemplateOptions.prototype = {
    initialize : function(config){
        this.templateOptions = config.options;
        this.optionId = config.optionId;
        this.productId = null;
        this.jsid = config.jsid;
        this.blacklist = null;
        this.combinedImage = null;

        this.template_price_cache = null;
        this.template_price_cache_valid = false;
        log(config, 1);
    },
    setProductId: function(data) {
        this.productId = data;
    },
    getProductId: function() {
        return this.productId;
    },
    setBlacklist: function(data) {
        this.blacklist = data;
    },
    getBlacklist: function() {
        return this.blacklist;
    },
    setCombinedImage: function(data) {
        this.combinedImage = data;
    },
    getCombinedImage: function() {
        return this.combinedImage;
    },
    getOptionId: function() {
        return this.optionId
    },
    getJSId: function() {
        return this.jsid
    },
    setOptionPrice : function(price,templateOptionId){
        log('setOptionPrice start template-id='+templateOptionId+' :' + price, 2);
        var pos = this.getTemplateOptionPosition(templateOptionId);
        if( pos !== null) {
            this.templateOptions[pos].price = price;
            log("set templateOption price to " + price, 2)
        }
        this.template_price_cache_valid = false;
        log('setOptionPrice end', 3);
    },
    getTemplatePrice : function() {
        log("get template price start", 2);
        if (this.template_price_cache_valid && this.template_price_cache) {
            return (this.template_price_cache*1);
        }

        var price = 0;
        for( i=0; i<this.templateOptions.length; i++) {
            log("getTemplatePrice i="+i+" price="+this.templateOptions[i].price*1, 3);
            price += (this.templateOptions[i].price*1);
        }

        this.template_price_cache = price*1;
        this.template_price_cache_valid = true;

        log("get template price end price="+price, 2);
        return (price*1);
    },

    reloadPrice : function() {
        log("reloadPrice start id="+this.optionId,2);
        var optionId = this.optionId;

        opConfig.reloadPrice();

        // Get all other prices for the other configurator-templates
        var configPrices=0.0;
        for (var i in opConfig.config) {
            if (i != optionId) {
                log("price="+opConfig.config[i],3);
                if (typeof(opConfig.config[i]) == 'number') {
                    configPrices+= opConfig.config[i];
                }
            }
        }
        log("configPrices="+configPrices, 3);

        var postprice = this.calculatePostpricerules();
        var template_price = this.getTemplatePrice();
        opConfig.config[optionId] = template_price ;
        log("id="+optionId+" price="+template_price, 3);
        optionsPrice.changePrice('options', opConfig.config[optionId]+configPrices+postprice);

        optionsPrice.reload();
        log('reloadPrice end', 2);
    },
    getTemplateOptionPosition : function(templateOptionId) {
        for( var i=0; i<this.templateOptions.length; i++) {
            if( this.templateOptions[i].id == templateOptionId ) {
                return i;
            }
        }
        return null;
    }
};

Product.TemplateOptions.Combination = Class.create();
Product.TemplateOptions.Combination.prototype = {
    initialize : function(calculateCallback, priceCallback, optionId, inputId, templateOptionInstance){
        this.id = inputId;
        this.value = 0;
        this.value_id = null;
        this.items = new Array();
        this.calculateCallback = calculateCallback;
        this.priceCallback = priceCallback;
        this.templateOptionId = optionId;
        this.input = jQuery('#'+inputId);
        this.toInstance = templateOptionInstance;
    },
    addItem : function(id,value, value_id) {
        log("addItem start id=" + id + " value=" + value + " value_id="+value_id, 2);
        if( !value ) {
            value = 0;
        }

        var temp2Value = value *1;
        if (!isNaN(temp2Value)){
            value = value *1;
        }
        log("typeof value " + (typeof value), 3);
        var tmpValue = new String(value);
        if( tmpValue.match(',') != null ) {
            var temp3Value = parseFloat( tmpValue.replace(',','.') );
            if (!isNaN(temp3Value)){
                value = temp3Value;
            }
        }
        if (value_id) {
            this.value_id = value_id;
        }
        this.items.push({ id : id , value : value, value_id : value_id });
        log("endAdditem value=" +value, 2);
    },
    setItemValueOnly : function(id,value, value_id) {
        log("setItemValueOnly start " + id + " " +value+ " value_id="+value_id, 2);
        for(var i=0; i< this.items.length; i++) {
            if( this.items[i].id == id ) {
                var tmpValue = new String(value);
                if( tmpValue.match(',') != null ) {
                    value = parseFloat( tmpValue.replace(',','.') );
                }
                var temp2Value = value *1;
                if (!isNaN(temp2Value)){
                    value = value *1;
                }
                this.items[i].value = value;
                break;
            }
        }
        if (value_id) {
            this.value_id = value_id;
        }
        log("setItemValueOnly end", 2);
    },
    setItemValue : function(id,value, value_id) {
        log("setItemValue start " + id + " " +value+ " value_id="+value_id, 2);
        for(var i=0; i< this.items.length; i++) {
            if( this.items[i].id == id ) {

                if(value != undefined){
                    var tmpValue = new String(value);
                    if( tmpValue.match(',') != null ) {
                        value = parseFloat( tmpValue.replace(',','.') );
                    }
                    var temp2Value = value *1;
                    if (!isNaN(temp2Value)){
                        value = value *1;
                    }
                    this.items[i].value = value;
                }
                break;
            }
        }
        if (value_id) {
            this.value_id = value_id;
        }else{
            var $item = jQuery('#'+id);
            if ($item.hasClass("selectcombi") ||$item.hasClass("listimagecombi") || $item.hasClass("overlayimagecombi")) {
                this.value_id = null;
            }
        }
        this.calculate();
        log("setItemValue end", 2);
    },
    getItemValue : function(id) {
        for(var i=0; i< this.items.length; i++) {
            if( this.items[i].id == id ) {
                return this.items[i].value;
            }
        }
        return null;
    },
    getElementId: function() {
        return jQuery(this.input).attr("id");
    },
    getId: function() {
        return this.id;
    },
    getJsTemplateId: function() {
        return this.toInstance.getJSId();
    },
    triggerCalculateCallback : function() {
        this.value = this.calculateCallback(this);
    },
    calculate : function() {
        this.value = this.calculateCallback(this);
        var element = jQuery(this.input);

        if (typeof(element) === "object") {
            if (!element.is("select")) {
                if( optionsPrice.priceFormat.decimalSymbol == ',' &&
                    this.value &&
                    typeof this.value == "string" &&
                    OptionExpression.isNumber(this.value)) {

                    var val = this.value.replace('.',',');
                    this.input.val( val );
                } else {
                    this.input.val( this.value );
                }
            }
        }
    },
    setPrice : function() {
        log("setPrice start", 2);
        var price = this.priceCallback(this);
        this.toInstance.setOptionPrice(price,this.templateOptionId);
        this.toInstance.reloadPrice();
        log("setPrice end", 2);
    }
};

var Combination;
Combination = {
    config : {
        calculate_price : new Array()
    },
    initialize: function() {
        this.combinations = new Array();
    },
    add: function(instance, id, source_id, source_type) {
        jQuery("#"+id).addClass("active-event");
        if (!(this.combinations[id] instanceof Array)) {
            this.combinations[id] = new Array();
        }
        var index = this.combinations[id].length;
        this.combinations[id][index] = new Array();
        this.combinations[id][index]['instance'] = instance;
        this.combinations[id][index]['source_id'] = source_id;
        this.combinations[id][index]['source_type'] = source_type;
    },

    addCalculatePrice: function(instance) {
        this.config.calculate_price[instance.getId()] = instance;
    },
    clearCalcualtePrice: function() {
        this.config.calculate_price = new Array();
    },
    doCalculatePrice: function() {
        var instances = this.config.calculate_price;
        for(var key in instances) {
            if(instances.hasOwnProperty(key)){
             instances[key].setPrice();
            }
        }
    },

    startToProcess: function(id) {
        AjaxLoader.show();
        jQuery(".active-event").each(function() {
            jQuery(this).removeClass("active-event");
            jQuery(this).addClass("deactive-event");
        });
        block_ondocument_events = true;

        Combination.clearCalcualtePrice();
        Combination.process(id, null, 0);
        Combination.doCalculatePrice();

        jQuery(".deactive-event").each(function() {
            jQuery(this).removeClass("deactive-event");
            jQuery(this).addClass("active-event");
        });
        block_ondocument_events = false;
        AjaxLoader.hide();
    },

    process: function(id, parent_id, iteration) {
        if (iteration > 250) { /* max Amount of recursions */
            return false;
        }
        if (this.combinations[id]) {
            for (var i=0; i<this.combinations[id].length; i++) {
                var value;
                var value_id = null;
                var source_type = this.combinations[id][i]['source_type'];

                if ( source_type == 'radiobuttons') {
                    var element = jQuery('#' + id).parent().find('input:checked');
                } else {
                    var element = jQuery("#"+id);
                }
                var combination_instance = this.combinations[id][i]['instance'];

                var current_element = jQuery(element);
                if (typeof(current_element) === "object") {
                    if (current_element.is("select")) {
                        value = current_element.find('option:selected').attr('attr-value');
                        // get price-list for the selected value-id
                        if (current_element.hasClass("selectcombi") ||
                            current_element.hasClass("listimagecombi") ||
                            current_element.hasClass("overlayimagecombi")) {
                            value_id = current_element.find('option:selected').attr('attr-value-id');
                        }
                        log('It´s select: ' + value + " " + value_id, 3);
                    } else if (current_element.hasClass("checkbox")) {
                        value = OptionCheckbox.getValue(current_element.attr("id"));
                    } else if (element.hasClass('custom')) {
                        value = element.attr('attr-value');
                    } else {
                        value = current_element.val();
                        log('any other element: ' + value, 3);
                    }

                    combination_instance.setItemValue(current_element.attr('id'), value, value_id);
                    log('setItemValue=' + current_element.attr('id') + " " + value + " value_id " + value_id, 2);
                }

                // Blacklist handling
                var js_template_id = combination_instance.getJsTemplateId();
                var instance = Configuration.getInstanceByJsId(js_template_id);
                var callback = instance.getBlacklist().blacklistCallback(id, this.getOptionId(id), source_type);
                Configuration.reloadPrice(combination_instance.getJsTemplateId());

                // Process the changed element
                this.process(combination_instance.getElementId(), id, iteration+1);
                this.addCalculatePrice(combination_instance);
            }
        }
    },
    getOptionId: function(id) {
        if (id) {
            var parts = id.split("-");
            return parts[parts.length-1];
        }
        return id;
    }
};
Combination.initialize();