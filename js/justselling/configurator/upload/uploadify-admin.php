<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

require_once '../../../../app/Mage.php';
umask(0);
$app = Mage::app('');


// Define a destination
$targetFolder = Mage::getBaseDir('media') .'/tmp/upload/admin';


if (!file_exists(str_replace('//', '/', $targetFolder))) {
    mkdir(str_replace('//', '/', $targetFolder), 0755, true);
    mkdir(str_replace('//', '/', $targetFolder . DS . "thumbs"), 0755, true); // Make subfoder for thumbnails
}

if (!empty($_FILES)) {
    $fileParts = pathinfo($_FILES['Filedata']['name']);
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $targetPath = $targetFolder;

	$filename = $_FILES['Filedata']['name'];
	if($_POST['filename']){
		$filename = $_POST['filename'] .'.' .$fileParts['extension'];
	}

    $targetFile = rtrim($targetPath, '/') . '/' . $filename;

    $uploaded =  move_uploaded_file($tempFile,$targetFile);
	Mage::log('$uploaded ' .$uploaded);
	if($uploaded){
		if (!file_exists(str_replace('//','/',$targetFolder))){
			mkdir(str_replace('//','/',$targetPath.DS."thumbs".DS.$session_id), 0755, true);
		}

		$file = str_replace(Mage::getBaseDir('media') .'/tmp/upload/admin'.DS, "", $targetFile);
		$image_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) .'/tmp/upload/admin' . DS . $file;

		$html = '<div class="uploadifyimgwrapper"><img class="uplodifyimg' . '" attr-file="' .$file .'" src="' . $image_url . '"/>';
		$html .= '<a class="uploadifytag" id="uploadifytag' . '" href="#" ></a></div>';
		echo $html;
	}else{
		echo "<div class='error'>File couldn't be saved to temp folder. Maybe filesize is to big or has no permission for writing.</div>";
	}


}
?>