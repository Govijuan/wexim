var JustsellingUplodify = {
	config : {
		baseJsUrl : '',
		skinJsUrl : '',
		deleteUrl : ''
	},
	initFile : function(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, validationClasses, optionMulti, sessionId, mimetypes) {
		if (!this.browserSupport.isFlash()) {
			if (this.browserSupport.isHTML5InputTypeFile() && !this.browserSupport.oldIE()) {
				this.html5.start(timestamp, token, optionId, jsTemplateId, mimetypes, optionMaxsize, id, buttonImage, validationClasses, optionMulti, sessionId, optionFiletype);
			} else {
				JustsellingUploadJsPhtml.errorOverlay.browserSupportNoUploadType();
			}
		} else {
            window.setTimeout(function() {
                JustsellingUplodify.flash.start(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, validationClasses, optionMulti, sessionId);
            }, 200);
		}
	},
	initMyuploads : function(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, sessionId, mimetypes) {
		if (!this.browserSupport.isFlash()) {
			if (this.browserSupport.isHTML5InputTypeFile()) {
				this.html5.update(timestamp, token, optionId, jsTemplateId, mimetypes, optionMaxsize, id, buttonImage, sessionId, optionFiletype);
			} else {
				JustsellingUploadJsPhtml.errorOverlay.browserSupportNoUploadType();
			}
		} else {
                this.flash.update(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, sessionId);
		}
	},
	flash : {
		start : function(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, validationClasses, optionMulti, sessionId) {
			var id = id;
			var uploadifyWrapper = jQuery('#file-wrapper-' + id);
			var input = jQuery('#file-wrapper-' + id).find('input').clone();
			var uploadifyElement = jQuery("#" + id);
			var swf = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.swf';
			var uploader = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.php';
			var cancelImage = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/cancel.png';
            sessionId = mageSessionId;

			var uploadifyElement = uploadifyElement.uploadify({
				'debug' : false,
				'formData' : {
					'timestamp' : timestamp,
					'token' : token,
					'optionId' : optionId,
					'jsTemplateId' : jsTemplateId,
					'frontend' : sessionId
				},
				'swf' : swf,
				'uploader' : uploader,
				'buttonImage' : buttonImage,
				'buttonClass' : 'upload-button',
				'progressData' : 'percentage',
				'cancelImage' : cancelImage,
				'fileTypeExts' : optionFiletype,
				'fileSizeLimit' : optionMaxsize,
				'width' : 144,
				'auto' : true,
				onInit : function() {

					jQuery('#file-wrapper-' + id).attr('attr-vc', validationClasses).attr('attr-multi', optionMulti);
					jQuery('#file-wrapper-' + id).find('.file-wrapper1').append(input).addClass('flashupload');
					jQuery('#file-wrapper-' + id).find('.file-wrapper2').addClass('clearfix');
				},
				onProgress : function() {
					JustsellingUplodify.helper.checkValidationClasses(id);
				},
				onCancel : function() {
					JustsellingUplodify.helper.checkValidationClasses(id);
				},
				onSelectError : function(file, errorCode, errorMsg) {
					JustsellingUploadJsPhtml.flash.onSelectError(this, errorCode, file);
				},
				onUploadSuccess : function(file, data, response) {
					JustsellingUplodify.helper.checkValidationClasses(id, true);
					JustsellingUplodify.helper.removeValidationAdvice(id);

					var fileWrapper1 = uploadifyWrapper.find("#file-wrapper1-" + id);
					var fileWrapper2 = uploadifyWrapper.find("#file-wrapper2-" + id);

					if (optionMulti && optionMulti != '0') {
						if (fileWrapper2.is(':visible')) {
							fileWrapper2.append(data);
						} else {
							fileWrapper2.html(data);
							fileWrapper2.show();
						}
					} else {
						fileWrapper2.html(data);
						fileWrapper1.addClass('hideuplodifybutton');
						fileWrapper2.show();
					}
				}
			});
			uploadifyWrapper.find('.file-wrapper2').on('click', 'a', function(e) {
				e.preventDefault();
				JustsellingUplodify.helper.deleteUpload(this);
			});
			jQuery("#" + id).show();
		},
		update : function(timestamp, token, optionId, jsTemplateId, optionFiletype, optionMaxsize, id, buttonImage, sessionId) {
			var id = id;
			var uploadifyWrapper = jQuery('#file-wrapper-' + id);
			var input = jQuery('#file-wrapper-' + id).find('input').clone();
			var uploadifyElement = jQuery("#" + id);
			var swf = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.swf';
			var uploader = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.php';
			var cancelImage = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/cancel.png';
            sessionId = mageSessionId;

			var uploadifyElement = uploadifyElement.uploadify({
				'debug' : false,
				'formData' : {
					'timestamp' : timestamp,
					'token' : token,
					'optionId' : optionId,
					'jsTemplateId' : jsTemplateId,
					'frontend' : sessionId,
					'uploadId' : id
				},
				'swf' : swf,
				'uploader' : uploader,
				'buttonImage' : buttonImage,
				'buttonClass' : 'upload-button',
				'progressData' : 'percentage',
				'cancelImage' : cancelImage,
				'fileTypeExts' : optionFiletype,
				'fileSizeLimit' : optionMaxsize,
				'width' : 144,
				'auto' : true,
				onInit : function() {
				},
				onSelectError : function(file, errorCode, errorMsg) {
					JustsellingUploadJsPhtml.flash.onSelectError(this, errorCode, file);
				},
				onUploadSuccess : function(file, data, response) {
					location.reload(true);
				}
			});
			jQuery("#" + id).show();
		}
	},
	html5 : {
		start : function(timestamp, token, optionId, jsTemplateId, mimetypes, optionMaxsize, id, buttonImage, validationClasses, optionMulti, sessionId, optionFiletype) {
			var id = id;
			var uploadifyWrapper = jQuery('#file-wrapper-' + id);
			var uploadifyElement = jQuery("#" + id);
			var swf = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.swf';
			var uploadScript = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.php';
			var cancelImage = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/cancel.png';
            var fileType = JSON.parse(mimetypes);
            sessionId = mageSessionId;

			var uploadifyElement = uploadifyElement.uploadifive({
				'debug' : false,
				'formData' : {
					'timestamp' : timestamp,
					'token' : token,
					'optionId' : optionId,
					'jsTemplateId' : jsTemplateId,
					'frontend' : sessionId
				},
				'swf' : swf,
				'uploadScript' : uploadScript,
				'buttonText' : 'Upload',
				'buttonClass' : 'upload-button',
				'fileType' : fileType,
				'fileSizeLimit' : optionMaxsize,
				'width' : 144,
				'auto' : true,
				onInit : function() {
					uploadifyWrapper.attr('attr-vc', validationClasses).attr('attr-multi', optionMulti);
					uploadifyWrapper.find('.file-wrapper2').addClass('clearfix');
					var input = uploadifyWrapper.find('input.template-option');
					uploadifyWrapper.find('.file-wrapper1').append(input).addClass('html5upload');;
				},
				onProgress : function() {
					JustsellingUplodify.helper.checkValidationClasses(id);
				},
				onFallback : function() {
					alert(fallbackMessage);
				},
				onCancel : function() {
					JustsellingUplodify.helper.checkValidationClasses(id);
				},
				onError : function(errorType, file, uploadAll) {
					JustsellingUploadJsPhtml.html5.onError(errorType, optionFiletype, optionMaxsize);
				},
				onUploadComplete : function(file, data) {
					uploadifyWrapper.find('.complete .close').click();
					JustsellingUplodify.helper.checkValidationClasses(id, true);
					JustsellingUplodify.helper.removeValidationAdvice(id);

					var fileWrapper1 = uploadifyWrapper.find("#file-wrapper1-" + id);
					var fileWrapper2 = uploadifyWrapper.find("#file-wrapper2-" + id);

					if (optionMulti && optionMulti != '0') {
						if (fileWrapper2.is(':visible')) {
							fileWrapper2.append(data);
						} else {
							fileWrapper2.html(data);
							fileWrapper2.show();
						}
					} else {
						fileWrapper2.html(data);
						fileWrapper1.addClass('hideuplodifybutton');
						fileWrapper2.show();
					}
				}
			});
			uploadifyWrapper.find('.file-wrapper2').on('click', 'a', function(e) {
				e.preventDefault();
				JustsellingUplodify.helper.deleteUpload(this);
			});
			jQuery("#" + id).show();
		},
		update : function(timestamp, token, optionId, jsTemplateId, mimetypes, optionMaxsize, id, buttonImage, sessionId, optionFiletype) {

			var id = id;
			var uploadifyWrapper = jQuery('#file-wrapper-' + id);
			var uploadifyElement = jQuery("#" + id);
			var swf = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.swf';
			var uploadScript = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.php';
			var cancelImage = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/cancel.png';
			var fileType = JSON.parse(mimetypes);
            sessionId = mageSessionId;

			var uploadifyElement = uploadifyElement.uploadifive({
				'debug' : false,
				'formData' : {
					'timestamp' : timestamp,
					'token' : token,
					'optionId' : optionId,
					'jsTemplateId' : jsTemplateId,
					'frontend' : sessionId,
					'uploadId' : id
				},
				'swf' : swf,
				'uploadScript' : uploadScript,
				'buttonText' : 'Upload',
				'buttonClass' : 'upload-button',
				'fileType' : fileType,
				'fileSizeLimit' : optionMaxsize,
				'width' : 144,
				'auto' : true,
				onInit : function() {
					var input = uploadifyWrapper.find('input.template-option');
					uploadifyWrapper.find('.file-wrapper1').append(input);
				},
				onError : function(errorType, file, uploadAll) {
					JustsellingUploadJsPhtml.html5.onError(errorType, optionFiletype, optionMaxsize);
				},
				onQueueComplete : function() {
				},
				onUploadComplete : function(file, data) {
					location.reload(true);
				}
			});
			jQuery("#" + id).show();
		}
	},
	browserSupport : {
		isHTML5InputTypeFile : function() {
			var elem = document.createElement("input");
			elem.type = "file";
			if (elem.disabled)
				return false;
			try {
				elem.value = "Test";
				// Throws error if type=file is implemented
				return elem.value != "Test";
			} catch (e) {
				return elem.type == "file";
			}
		},
		isFlash : function() {
            if (jQuery.browser.safari) {
                log ("using html5 support for safari",1);
                return false; // Use HTML5 support for Safari Browsers
            }
            log ("flash status is "+jQuery.browser.flash,1);
            return jQuery.browser.flash;
		},
		oldIE : function(){
			if (jQuery.browser.msie && parseInt(jQuery.browser.version) <= 9) {
				return true;
			}else{
				return false;
			}
		}
	},
	helper : {
		checkValidationClasses : function(id, onComplete) {
			var wrapperSelector = JustsellingUplodify.helper.createWrapperSelector(id);
			var uploadifyWrapper = jQuery(wrapperSelector);
			var validationClasses = uploadifyWrapper.attr('attr-vc');
			if (validationClasses) {
				var imageSize = uploadifyWrapper.find('.file-wrapper2 img').size();
				if (imageSize > 0 || onComplete) {
					uploadifyWrapper.find('.file-wrapper1 input').removeClass(validationClasses);
				} else {
					uploadifyWrapper.find('.file-wrapper1 input').addClass(validationClasses);
				}
			}
		},
		createWrapperSelector : function(id) {
			if (id.indexOf("file-wrapper") !== -1) {
				id = '#' + id;
			} else {
				id = '#file-wrapper-' + id;
			}
			return id;
		},
		changeUploadButtonVisisbility : function(id) {
			var wrapperSelector = JustsellingUplodify.helper.createWrapperSelector(id);
			var uploadifyWrapper = jQuery(wrapperSelector);
			var optionMulti = uploadifyWrapper.attr('attr-multi');
			if (!optionMulti || optionMulti == '0') {
				var imageSize = uploadifyWrapper.find('.file-wrapper2 img').size();
				var hideuplodifybutton = uploadifyWrapper.find('.hideuplodifybutton');
				if (imageSize == 0 || imageSize == '0') {
					hideuplodifybutton.removeClass('hideuplodifybutton');
				}
			}
		},
		removeValidationAdvice : function(id) {
			var wrapperSelector = JustsellingUplodify.helper.createWrapperSelector(id);
			var uploadifyWrapper = jQuery(wrapperSelector).find('.validation-advice').remove();
		},
		deleteUpload : function(elm) {
			var id = jQuery(elm).attr('id');
			var id = id.replace('uploadifytag', '');
			var wrapperId = jQuery(elm).parents('.file-wrapper').attr('id');
			jQuery.get(JustsellingUplodify.config.deleteUrl, {
				"id" : id
			}, function(data, textStatus, XMLHttpRequest) {
				jQuery("#uplodifyimg" + id).remove();
				jQuery("#uploadifytag" + id).remove();
				JustsellingUplodify.helper.checkValidationClasses(wrapperId);
				JustsellingUplodify.helper.changeUploadButtonVisisbility(wrapperId)
			}, 'html').fail(function() {
				alert("Unknown Error");
			});
		}
	}
};
