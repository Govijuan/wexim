<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

require_once '../../../../app/Mage.php';
umask(0);
$app = Mage::app('admin');

$targetFolder = Mage::getBaseDir('var') .'/imports';

if (!file_exists(str_replace('//', '/', $targetFolder))) {
    mkdir(str_replace('//', '/', $targetFolder), 0755, true);
}

if (!empty($_FILES)) {
    $fileParts = pathinfo($_FILES['Filedata']['name']);
    $tempFile = $_FILES['Filedata']['tmp_name'];
    $targetPath = $targetFolder;

	$filename = $_FILES['Filedata']['name'];
	if(isset($_POST['filename'])){
		$filename = $_POST['filename'] .'.' .$fileParts['extension'];
	}

    if (!$filename) {
        Js_Log::log("Filename to upload is not given", $this, Zend_Log::ERR);
    } else {
        $targetFile = rtrim($targetPath, '/') . '/' . $filename;
        $uploaded =  move_uploaded_file($tempFile,$targetFile);
    }
    echo urlencode($filename);
}
?>