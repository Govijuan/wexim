<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

require_once '../../../../app/Mage.php';
umask(0);
$app = Mage::app('default');

// Laad propper session
$session_name = "frontend";
Mage::Log("session ".$session_name);
if (!isset($_POST[$session_name])) {
	exit;
} else {
	session_id($_POST[$session_name]);
	session_start();
}

// Define a destination
$session_id =  Mage::getSingleton('core/session')->getSessionId();
Mage::Log("session is ".$session_id);
$targetFolder = Mage::getBaseDir('media').DS.Mage::getStoreConfig('fileuploader/general/mediapath').DS.$session_id.DS;

if (file_exists( $targetFolder . '/' . $_POST['filename'])) {
	echo 1;
} else {
	echo 0;
}
?>