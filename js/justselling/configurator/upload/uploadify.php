<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

require_once '../../../../app/Mage.php';
umask(0);
$app = Mage::app('');

// Laad propper session
$session_name = "frontend";
if (!isset($_POST[$session_name])) {
	exit;
} else {
	session_id($_POST[$session_name]);
	session_start();
}

// Define a destination
$session_id =  $_POST['frontend'];
$targetFolder = Mage::getBaseDir('media').DS.Mage::getStoreConfig('fileuploader/general/mediapath').DS.$session_id.DS;


if (!file_exists(str_replace('//','/',$targetFolder))){
	mkdir(str_replace('//','/',$targetFolder), 0755, true);
	mkdir(str_replace('//','/',$targetFolder.DS."thumbs"), 0755, true); // Make subfoder for thumbnails
}

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $targetFolder;
	$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
	
	// Validate the file type
	$fileTypes = explode(";",Mage::getStoreConfig('fileuploader/general/filetype'));
	foreach ($fileTypes as $key => $value)
		$fileTypes[$key] = trim($fileTypes[$key]);
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	if (true || in_array($fileParts['extension'],$fileTypes)) {
		move_uploaded_file($tempFile,$targetFile);
		
		$optionId = $_POST['optionId'];
		$jsTemplateId = $_POST['jsTemplateId'];
		
		if (!file_exists(str_replace('//','/',$targetFolder))){
			mkdir(str_replace('//','/',$targetPath.DS."thumbs".DS.$session_id), 0755, true);
		}
		$file = str_replace(Mage::getBaseDir('media').DS.Mage::getStoreConfig('fileuploader/general/mediapath').DS, "", $targetFile);
		if(isset($_POST['uploadId']) && $_POST['uploadId']){
			$upload = Mage::getModel('configurator/upload')->load($_POST['uploadId']);
			$upload->setFile($file);
			$upload->setStatus('1');
			$upload->save();
		}else{
			$upload = Mage::getModel('configurator/upload');
			$upload->setSessionId($session_id);
			$upload->setFile($file);
			$upload->setStatus('1');
			$upload->setOptionId($optionId);
			$upload->setJsTemplateId($jsTemplateId);
			$upload->setCreatedAt(new Zend_Date());
			$upload->save();
		}
		
		// Create Thumbnail
		if (in_array($fileParts['extension'],array('png','gif','jpg','jpeg'))) {
			$thumb = Mage::getBaseDir('media').DS.Mage::getStoreConfig('fileuploader/general/mediapath').DS."thumbs".DS.$file;
			$image = new Varien_Image($targetFile);
			$image->keepAspectRatio(true);
			$image->resize(80,80);
			$image->save($thumb);
			$image_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).Mage::getStoreConfig('fileuploader/general/mediapath').DS."thumbs".DS.$file;
		} else {
			// Mage::Log("it is not image");
			$image_url = Mage::helper('configurator')->getUploadImage($file);
		}
		
		$html = '<div class="uploadifyimgwrapper"><img id="uplodifyimg' .$upload->getId() .'" src="'.$image_url.'"/>';
		$html .= '<a class="uploadifytag" id="uploadifytag' .$upload->getId()  .'" href="#" ></a></div>';
		// Mage::log('***** html ' .$html);
		echo $html;
		
	} else {
		echo 'Invalid file type';
	}
}
?>