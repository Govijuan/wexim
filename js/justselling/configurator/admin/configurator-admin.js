jQuery(document).ready(function(){
    ConfiguratorTemplate.init();
});

var ConfiguratorTemplate = {
    init: function () {
        ConfiguratorTemplate.createAndOpenImport();
    },
    createAndOpenImport : function(){
        var $templateButton = jQuery('#import_template').removeClass('scalable');
        var baseUrl = $templateButton.attr('onclick');
        var url = $templateButton.attr('class');
        var title = $templateButton.attr('title');
        $templateButton.after('<div class="importer-wrapper"><input type="text" name="importer" id="template_importer" /></div>');
        $templateButton.hide();
        ConfiguratorTemplate.createUplodify(baseUrl, url, title);
    },
    createUplodify: function (baseUrl, url, title) {
        var uploadifyElement = jQuery("#template_importer");

        var swf = baseUrl +'justselling/configurator/upload/uploadify.swf';
        var uploader = baseUrl+ 'justselling/configurator/upload/uploadify-importer.php';
        var buttonClass =  'upload-button';
        var buttonText = title;
        var cancelImage = baseUrl+ 'justselling/configurator/upload/cancel.png';

        uploadifyElement.uploadify({
            'swf': swf,
            'uploader': uploader,
            'buttonClass': buttonClass,
            'buttonText': buttonText,
            'cancelImage': cancelImage,
            'height': 16,
            'width': 120,
            'fileTypeExts': '*.zip',
            'removeTimeout': 0,
            onUploadError: function (file, errorCode, errorCode2, errorMsg) {
                ConfigurationAdmin.helper.messages.error(Translator.translate('onUploadError') + errorMsg + '(' +errorCode2 + ')');
            },
            onSelectError: function (file, errorCode, errorCode2, errorMsg) {
                ConfigurationAdmin.helper.messages.error(Translator.translate('onSelectError') + errorMsg + '(' +errorCode2 + ')');
            },
            onUploadSuccess: function (file, data, response) {
                if(file['name']){
                    var importurl = url +"filename/" + data;
                    location.href = importurl;
                }else{
                    ConfigurationAdmin.helper.messages.error('Leider ist etwas beim importieren der Datei schiefgegangen.');
                }
            }
        });
    }
};


var ConfigurationAdmin = {
    config: {
        optionValues: new Array(),
        optionId: '',
        newOptionId: 0,
        newSavedOptionId: 0,
        optionValueId: '',
        pricelistId: '',
        valuePricelistId: '',
        urlOptionvalue: '',
        urlOptionvalues: '',
        urlValuedetails: '',
        urlOptiondetails: '',
        urlClearcache: '',
        urlMatrixcsv: '',
        urlCopyoption: '',
        saveOption: '',
        save: '',
        deleteButtonUrl: '',
        loadingText: '',
        copyOptionText: '',
        clearingCacheText: '',
        templateId: 0,
        fieldId: '',
        scrollTopMarker: ''
    },
    init: function () {
        ConfigurationAdmin.option.buildInit();
        ConfigurationAdmin.bindUIActions();
        JustsellingUplodify.init();
        extendJQUery();
    },
    bindUIActions: function () {
        jQuery(".add_new_defined_option").live('click', function () {
            ConfigurationAdmin.option.addNew();
        });
        jQuery("select.select-product-option-type").live("change", function () {
            ConfigurationAdmin.select.optionType(this);
        });
        jQuery("#add_new_defined_row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.add.optionValue(this);
        });
        jQuery("#add_new_defined_pricelist_row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.add.pricelist(this);
        });
        jQuery("button.delete-template-option").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.remove.option(this);
        });
        jQuery("button.delete-template-pricelist-row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.remove.pricelist(this);
        });
        jQuery("button.copy-template-option").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.copy.option(this);
        });
        jQuery("button.edit-template-option").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.edit.option(this);
        });
        jQuery("button.save-template-option").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.save.option(this);
        });
        jQuery("button.cancel-template-option").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.cancel.option(this);
        });
        jQuery("button.edit-template-option-value").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.edit.optionValue(this);
        });
        jQuery("button.save-template-option-value").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.save.optionValue(this);
        });
        jQuery("button.cancel-template-option-value").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.cancel.optionValue(this);
        });
        jQuery("button.delete-template-option-row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.remove.optionValue(this);
        });
        jQuery("button.textimage_configure_button").live("click", function (e) {
            e.preventDefault();
            TextImage.overlay.show(this);
        });
        jQuery("button.export_matrix_button").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.exportcsv.matrix(this);
        });
        jQuery("#configurator_cache_clear").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.clearcache();
        });
        jQuery("#add_new_defined_pricelistvalue_row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.add.pricelistvalue(this);
        });
        jQuery("button.delete-template-pricelistvalue-row").live("click", function (e) {
            e.preventDefault();
            ConfigurationAdmin.button.remove.pricelistvalue(this);
        });
    },
    option: {
        buildInit: function () {
            jQuery(ConfigurationAdmin.config.optionValues).each(function () {
                this.is_require = this.is_require * 1;
                ConfigurationAdmin.option.add(this);
            });
        },
        addNew: function () {
            // move to right tab
            if(jQuery("#configurator_tabs_form_options_content").is(':hidden')){
                jQuery('#edit_form > div').hide();
                jQuery('#configurator_tabs_form_options_content').show();
                jQuery('#configurator_tabs a').removeClass('active');
                jQuery('#configurator_tabs_form_options').addClass('active');
            }
            jQuery(window).scrollTop(0);

            if(ConfigurationAdmin.config.templateId == 0){
                var title = jQuery('#configurator_tabs_form_section_content #title').val();
                if(title && title.length > 0){
                    var ok = confirm(Translator.translate('optionCantCreateBeforeTemplateSaved'));
                    if (ok){
                        ConfigurationAdmin.helper.ajax.saveTemplate();
                    }else{
                        return;
                    }
                }else{
                    ConfigurationAdmin.helper.messages.error(Translator.translate('optionCantCreateWithoutTemplateId'));
                    return;
                }
            }else{
                ConfigurationAdmin.helper.option.createNew();
            }
        },
        add: function (option) {
            var $templateContent = jQuery("#optionTypeTemplate").tmpl(option).appendTo("#product_options_container");
            ConfigurationAdmin.helper.option.setSelectFields($templateContent, option);
        },
        copy: function (option) {
            var $templateContent = jQuery("#optionTypeTemplate").tmpl(option).prependTo("#product_options_container");
            ConfigurationAdmin.helper.option.setSelectFields($templateContent, option);
            ConfigurationAdmin.option.sort();
            jQuery('#option_' + option['id']).scrollTo();
        },
        sort: function () {
            jQuery("#product_options_container .option-box").orderBy(function () {
                return +jQuery(this).attr('attr-sortorder');
            },function () {
                return +jQuery(this).attr('attr-id');
            }).appendTo("#product_options_container");
        }
    },
    select: {
        optionType: function (elm) {
            var $optionBoxChild = jQuery(elm).parents(".option-box-child");
            var optionBoxChildSel = '#' + $optionBoxChild.attr('id');
            var $optionDetails = $optionBoxChild.find('.option_details');
            // get template
            var tmplItem = jQuery.tmplItem($optionBoxChild);
            var option = tmplItem.data;

            // get values
            var type = option.type = jQuery(elm).val();
            var parent_id = jQuery(tmplItem.nodes).find("select[name$='[parent_id]']").val();
            var font = jQuery(tmplItem.nodes).find("select[name$='[font]']").val();
            var is_require = jQuery(tmplItem.nodes).find("select[name$='[is_require]']").val();
            var value = jQuery(tmplItem.nodes).find("select[name$='[value]']").val();
            var placeholder = jQuery(tmplItem.nodes).find("select[name$='[placeholder]']").val();
            var upload_type = jQuery(tmplItem.nodes).find("select[name$='[upload_type]']").val();
            var is_visible = jQuery(tmplItem.nodes).find("select[name$='[is_visible]']").val();
            var apply_discount = jQuery(tmplItem.nodes).find("select[name$='[apply_discount]']").val();
            var alt_title = jQuery(tmplItem.nodes).find("select[name$='[alt_title]']").val();
            var option_group = jQuery(tmplItem.nodes).find("select[name$='[option_group]']").val();
            var default_value = jQuery(tmplItem.nodes).find("select[name$='[default_value]']").val();
            var matrix_operator_x = jQuery(tmplItem.nodes).find("select[name$='[matrix_operator_x]']").val();
            var matrix_operator_y = jQuery(tmplItem.nodes).find("select[name$='[matrix_operator_y]']").val();
            var listimage_hover = jQuery(tmplItem.nodes).find("select[name$='[listimage_hover]']").val();
            var listimage_style = jQuery(tmplItem.nodes).find("select[name$='[listimage_style]']").val();
            var text_validate = jQuery(tmplItem.nodes).find("select[name$='[text_validate]']").val();

            option.title = jQuery(tmplItem.nodes).find("input[name$='[title]']").val();
            option.sort_order = jQuery(tmplItem.nodes).find("input[name$='[sort_order]']").val();
            option.alt_title = jQuery(tmplItem.nodes).find("input[name$='[alt_title]']").val();
            option.decimal_place = jQuery(tmplItem.nodes).find("input[name$='[decimal_place]']").val();
            option.product_id = jQuery(tmplItem.nodes).find("input[name$='[product_id]']").val();
            option.default_value = jQuery(tmplItem.nodes).find("input[name$='[default_value]']").val();
            option.expression = jQuery(tmplItem.nodes).find("input[name$='[expression]']").val();
            option.url = jQuery(tmplItem.nodes).find("input[name$='[url]']").val();
            option.option_image_delete = jQuery(tmplItem.nodes).find("input[name$='[option_image_delete]']").val();

            // update jquery template
            tmplItem.data = option;
            tmplItem.update();

            // Reset Selectboxes
            jQuery(tmplItem.nodes).find("select.select-product-option-type").val(type);
            jQuery(tmplItem.nodes).find("select.select-product-option-parent_id").val(parent_id);
            jQuery(tmplItem.nodes).find("select.select-product-option-font").val(option.font);
            jQuery(tmplItem.nodes).find("select[name$='[is_require]']").val(is_require);
            jQuery(tmplItem.nodes).find("select[name$='[value]']").val(value);
            jQuery(tmplItem.nodes).find("select[name$='[placeholder]']").val(placeholder);
            jQuery(tmplItem.nodes).find("select[name$='[upload_type]']").val(upload_type);
            jQuery(tmplItem.nodes).find("select[name$='[is_visible]']").val(is_visible);
            jQuery(tmplItem.nodes).find("select[name$='[option_group]']").val(option_group);
            jQuery(tmplItem.nodes).find("select[name$='[apply_discount]']").val(apply_discount);
            jQuery(tmplItem.nodes).find("select[name$='[matrix_operator_x]']").val(matrix_operator_x);
            jQuery(tmplItem.nodes).find("select[name$='[matrix_operator_y]']").val(matrix_operator_y);
            jQuery(tmplItem.nodes).find("select[name$='[listimage_hover]']").val(listimage_hover);
            jQuery(tmplItem.nodes).find("select[name$='[listimage_style]']").val(listimage_style);
            jQuery(tmplItem.nodes).find("select[name$='[text_validate]']").val(text_validate);

            var $tmplOptionDetails = jQuery(tmplItem.nodes).find('.option_details');
            $tmplOptionDetails.replaceWith($optionDetails);
            var url = ConfigurationAdmin.config.urlOptiondetails + option.id + "?optionType=" + type;
            jQuery.get(
                url,
                function (html) {
                    var $dependentoptions = $optionBoxChild.find('.dependentoptions-wrapper');
                    $dependentoptions.replaceWith(html);
                }
            );

            var url = ConfigurationAdmin.config.urlOptionvalues + option.id + "?optionType=" + type;

            jQuery(tmplItem.nodes).find("select[name$='[default_value]']").html(ConfigurationAdmin.config.loadingText).load(
                url,
                function () {
                    jQuery(tmplItem.nodes).find("select.select-product-option-default_value").val(default_value);
                }
            ).show();

            $optionBoxChild = jQuery(optionBoxChildSel);
            JustsellingUplodify.initOption($optionBoxChild);
            return false;
        }
    },
    button: {
        add: {
            optionValue: function (elm) {
                AjaxLoader.show();

                var $optionBox = jQuery(elm).parents(".option-box");
                var $optionBoxChild = jQuery(elm).parents(".option-box-child");

                var validationErrors = false;
                var id = -1;
                if ($optionBoxChild.length > 0) {
                    id = $optionBoxChild.attr('attr-id');
                    var varienFormId = $optionBoxChild.attr('id');
                    var optionValueFormPart = new varienForm(varienFormId);
                    if (!optionValueFormPart.validator.validate()) {
                        validationErrors = true;
                    }
                }

                if (!validationErrors) {
                    // save new option before save new optionvalue
                    if (id == 0 || id == '0') {
                        var fields = $optionBoxChild.find("select, textarea, input").serializeArray();
                        var tmplItem = jQuery.tmplItem($optionBoxChild);
                        var tmpldata = tmplItem.data;
                        var templateIdObject = ConfigurationAdmin.helper.getTemplateObject(tmpldata);
                        fields.push(templateIdObject);
                        var url = ConfigurationAdmin.config.saveOption + 'id/' + tmpldata.id + '/so/1' + '?isAjax=true&form_key=' + window['FORM_KEY'];
                        jQuery.post(
                            url,
                            fields,
                            function (json, textStatus, XMLHttpRequest) {
                                try {
                                    var data = JSON.parse(json);
                                    var message = data['message'];
                                    var option = data['option'];

                                    if (message == 'success') {
                                        // replace option-box
                                        var $tmplContentOptionBox = jQuery("#optionTypeTemplate").tmpl(option);
                                        $optionBoxChild.parents('.option-box').replaceWith($tmplContentOptionBox);
                                        ConfigurationAdmin.config.newSavedOptionId = option['id'];
                                        // replace option-child-box
                                        option['child'] = 'child';
                                        var $tmplContent = jQuery("#optionTypeTemplateEdit").tmpl(option).appendTo("#product_options_container");
                                        ConfigurationAdmin.helper.option.overlay.open($tmplContentOptionBox, $tmplContent);
                                        ConfigurationAdmin.helper.option.setSelectFields($tmplContent, option);
                                        ConfigurationAdmin.helper.option.setDetails(option['id']);
                                        JustsellingUplodify.initOption($tmplContentOptionBox);
                                        // open new option value content
                                        var $newOptionBoxChild = $tmplContentOptionBox.find(".option-box-child");
                                        ConfigurationAdmin.helper.optionValue.addNew($newOptionBoxChild.find('#add_new_defined_row'));
                                        AjaxLoader.hide();
                                    } else {
                                        $optionBoxChild.find('.error-msg').html(message).show();
                                        AjaxLoader.hide();
                                    }
                                } catch (e) {
                                    $optionBoxChild.find('.error-msg').html(e + '<br/>' + data).show();
                                    AjaxLoader.hide();
                                }
                            }
                        );
                    } else {
                        ConfigurationAdmin.helper.optionValue.addNew(elm);
                        AjaxLoader.hide();
                    }
                } else {
                    AjaxLoader.hide();
                }
            },
            pricelist: function (elm) {
                var tmplItem = jQuery(elm).parents("tr").tmplItem();
                var nextId = ConfigurationAdmin.helper.next.pricelistId();

                jQuery('#optionTypePriceTableRowEdit').tmpl({option_id: tmplItem.data.id, id: nextId, price: "", operator: "=", value: ""}).appendTo("#pricelist_row_" + tmplItem.data.id);
            },
            pricelistvalue: function (elm) {
                var tmplItem = jQuery(elm).parents("tr").tmplItem();
                var nextId = ConfigurationAdmin.helper.next.valuePricelistId();

                jQuery('#optionTypeValuePriceTableRow').tmpl({option_id: tmplItem.data.option_id, value_id: tmplItem.data.value_id, id: nextId, price: "", operator: "=", value: ""}).appendTo("#option_" + tmplItem.data.option_id + "_values_" + tmplItem.data.value_id + "_pricelist");
            }
        },
        remove: {
            option: function (elm) {
                AjaxLoader.show();

                var $optionBox = jQuery(elm).parents(".option-box");
                var tmplItem = jQuery.tmplItem($optionBox);
                var tmplData = tmplItem.data;

                tmplItem.data['child'] = 'child';
                var $optionBoxChild = jQuery("#optionTypeTemplateEdit").tmpl(tmplItem.data);

                var deleteSel = '#' + ConfigurationAdmin.config.fieldId + tmplData.id + 'child_is_delete';
                var $deleteInput = $optionBoxChild.find(deleteSel);
                $deleteInput.val(1)

                var fields = $optionBoxChild.find("select, textarea, input").serializeArray();

                var templateIdObject = ConfigurationAdmin.helper.getTemplateObject(tmplData);
                fields.push(templateIdObject);

                var url = ConfigurationAdmin.config.saveOption + 'id/' + tmplData.id + '/so/1' + '?isAjax=true&form_key=' + window['FORM_KEY'];
                jQuery.post(
                    url,
                    fields,
                    function (json, textStatus, XMLHttpRequest) {
                        try {
                            var data = JSON.parse(json);
                            var message = data['message']
                            if (message == 'success') {
                                $optionBox.remove();
                                var baseoptionClass = 'parent-baseoption_' + tmplData.id;
                                jQuery('.' +baseoptionClass).html('keine Auswahl').removeClass(baseoptionClass).attr('attr-parentId', '');
                                ConfigurationAdmin.helper.messages.clear(tmplData.id);
                                AjaxLoader.hide();
                            } else {
                                ConfigurationAdmin.helper.messages.error(message);
                                AjaxLoader.hide();
                            }
                        } catch (e) {
                            ConfigurationAdmin.helper.messages.error(json);
                            AjaxLoader.hide();
                        }
                    }
                );


            },
            optionValue: function (elm) {
                AjaxLoader.show();
                var $optionBox = jQuery(elm).parents(".option-box");
                var $optionBoxChild = jQuery(elm).parents(".option-box-child");
                var tmplOptionItem = jQuery.tmplItem($optionBox);

                var $rowContainer = jQuery(elm).parents("tr");
                var rowId = $rowContainer.attr('id');
                var tmplItem = jQuery.tmplItem($rowContainer);
                var value = tmplItem.data;

                tmplOptionItem.data.values = jQuery.grep(tmplOptionItem.data.values, function (e, i) {
                    return e.id != value.id;
                });

                var $tmplContent = jQuery("#optionTypeSelectRowRemove").tmpl(tmplItem.data);
                $rowContainer.replaceWith($tmplContent);
                var $deleteRowContainer = jQuery('#' + rowId);

                var fields = $tmplContent.find("select, textarea, input").serializeArray();

                var templateIdObject = ConfigurationAdmin.helper.getTemplateObject(tmplOptionItem.data);
                fields.push(templateIdObject);

                var url = ConfigurationAdmin.config.saveOption + 'id/' + tmplOptionItem.data.id + '/sov/1?isAjax=true&form_key=' + window['FORM_KEY'];
                jQuery.post(
                    url,
                    fields,
                    function (json, textStatus, XMLHttpRequest) {
                        var data = JSON.parse(json);
                        var message = data['message'];
                        var defaultTitle = data['optionDefaultTitle'];
                        if (message == 'success') {
                            $deleteRowContainer.remove();
                            if (defaultTitle) {
                                $optionBox.find('.default-value .a-left').html(defaultTitle);
                            }
                            ConfigurationAdmin.helper.defaultValue.clear($optionBox, value.id);

                            // reset all params
                            if (ConfigurationAdmin.helper.option.overlay.isVisible()) {
                                var tmplOptionItem = jQuery.tmplItem($optionBoxChild);
                                tmplOptionItem.data.values = jQuery.grep(tmplOptionItem.data.values, function (e, i) {
                                    return e.id != value.id;
                                });

                                var $optionTypeOption = $optionBoxChild.find('.select-product-option-type option:selected');
                                ConfigurationAdmin.select.optionType($optionTypeOption);
                            }
                            ConfigurationAdmin.helper.messages.clear(tmplOptionItem.data.id);
                            AjaxLoader.hide();
                        } else {
                            ConfigurationAdmin.helper.messages.error(message);
                            AjaxLoader.hide();
                        }
                    },
                    'html'
                );
            },
            pricelist: function (elm) {
                var $optionBox = jQuery(elm).parents("div.option-box");
                var $tmplItemContainer = jQuery.tmplItem($optionBox);
                var $rowContainer = jQuery(elm).parents("tr");

                var tmplItem = jQuery.tmplItem($rowContainer);
                var pricelist = tmplItem.data;

                $tmplItemContainer.data.pricelist = jQuery.grep($tmplItemContainer.data.pricelist, function (e, i) {
                    return e.id != pricelist.id;
                });

                $rowContainer.find('input[id$="is_delete"]').val(1);
                $rowContainer.addClass('no-display').addClass('ignore-validate').hide();
            },
            pricelistvalue: function (elm) {
                var optionContainer = jQuery(elm).parents("div.pricelistvalues table");
                var tmplItemContainer = jQuery.tmplItem(optionContainer);

                var rowContainer = jQuery(elm).parents("tr:first");
                rowContainer = jQuery(rowContainer);

                var tmplItem = jQuery.tmplItem(rowContainer);
                var pricelist = tmplItem.data;

                tmplItemContainer.data.pricelist = jQuery.grep(tmplItemContainer.data.pricelist, function (e, i) {
                    return e.id != pricelist.id;
                });

                rowContainer.find('input[id$="is_delete"]').val(1);
                rowContainer.addClass('no-display').addClass('ignore-validate').hide();
            }
        },
        copy: {
            option: function (elm) {
                var $optionBox = jQuery(elm).parents(".option-box");
                var tmplItem = jQuery.tmplItem($optionBox);
                var option = tmplItem.data;
                var x = window.confirm(ConfigurationAdmin.config.copyOptionText);
                if (x) {
                    var url = ConfigurationAdmin.config.urlCopyoption + option.id + '?isAjax=true&form_key=' + window['FORM_KEY'];
                    jQuery.post(
                        url,
                        function (json, textStatus, XMLHttpRequest) {
                            try {
                                var data = JSON.parse(json);
                                var message = data['message'];
                                if (message == 'success') {
                                    var option = data['option'];
                                    ConfigurationAdmin.option.copy(option);
                                    ConfigurationAdmin.helper.messages.clear(option.id);
                                    AjaxLoader.hide();
                                } else {
                                    ConfigurationAdmin.helper.messages.error(message);
                                    AjaxLoader.hide();
                                }
                            } catch (e) {
                                ConfigurationAdmin.helper.messages.error(json);
                            }
                        },
                        'html'
                    );
                }
            }
        },
        edit: {
            option: function (elm) {
                AjaxLoader.show();
                var $optionBox = jQuery(elm).parents("div.option-box");
                var tmplItem = jQuery.tmplItem($optionBox);

                ConfigurationAdmin.helper.messages.clear(tmplItem.data.id);

                var url = ConfigurationAdmin.config.urlOptionvalue + tmplItem.data.id;
                jQuery.get(
                    url,
                    function (data) {
                        var option = data['option'];
                        var parentSelect = data['parent_select'];
                        var parentSelectId = data['parent_selectId'];

                        option['child'] = 'child';
                        ConfigurationAdmin.helper.option.clearOptionBoxContent($optionBox);
                        var $tmplContent = jQuery("#optionTypeTemplateEdit").tmpl(option).appendTo("#product_options_container");
                        jQuery('#' +parentSelectId).replaceWith(parentSelect);
                        ConfigurationAdmin.helper.option.overlay.open($optionBox, $tmplContent);
                        ConfigurationAdmin.helper.option.setSelectFields($tmplContent, option);
                        ConfigurationAdmin.helper.option.setDetails(option['id']);
                        JustsellingUplodify.initOption($optionBox);
                        AjaxLoader.hide();
                    },
                    'json'
                );
            },
            optionValue: function (elm) {
                AjaxLoader.show();
                var $rowContainer = jQuery(elm).parents("tr");
                var tmplItem = jQuery.tmplItem($rowContainer);
                var $tmplContent = jQuery("#optionTypeSelectRowEdit").tmpl(tmplItem.data);
                var detailsSelector = "#template_option_" + tmplItem.data.option_id + "_details_" + tmplItem.data.id;

                ConfigurationAdmin.helper.messages.clear(tmplItem.data.option_id);

                ConfigurationAdmin.helper.optionValue.overlay.open($rowContainer, $tmplContent);
                ConfigurationAdmin.helper.ajax.getOptionValueDetail(detailsSelector, tmplItem.data.option_id, tmplItem.data.id);
                var $optionBox = jQuery(elm).parents("div.option-box");
                JustsellingUplodify.initOption($optionBox);
                ConfigurationAdmin.helper.optionValue.scrollTopBeforeEdit('#option_' + tmplItem.data.option_id);
            }
        },
        cancel: {
            option: function (elm) {
                var $optionBoxChild = jQuery(elm).parents(".option-box-child");
                var $optionOvlContent = jQuery(elm).parents(".option-ovl-content");
                var tmplItem = jQuery.tmplItem($optionBoxChild);
                if (tmplItem.data.id == 0 || tmplItem.data.id == '0' || tmplItem.data.id == ConfigurationAdmin.config.newOptionId) {
                    jQuery(elm).parents(".option-box").remove();
                } else if (tmplItem.data.id == ConfigurationAdmin.config.newSavedOptionId) {
                    ConfigurationAdmin.button.remove.option(elm);
                } else {
                    ConfigurationAdmin.helper.option.copy.optionValues($optionOvlContent, $optionBoxChild);
                    JustsellingUplodify.initOption($optionBoxChild);
                }
                ConfigurationAdmin.config.newOptionId = -1;
                ConfigurationAdmin.config.newSavedOptionId = -1;
                jQuery('#configurator_tabs_form_options').removeClass('error');
                ConfigurationAdmin.helper.messages.clear(tmplItem.data.id);
                ConfigurationAdmin.helper.option.overlay.close();
            },
            optionValue: function (elm) {
                var $rowContainer = jQuery(elm).parents("tr");
                var tmplItem = jQuery.tmplItem($rowContainer);
                if (tmplItem.data.id == 0 || tmplItem.data.id == '0') {
                    var parentId = $rowContainer.attr('attr-parentid');
                    jQuery('#' + parentId).remove();
                }
                ConfigurationAdmin.helper.optionValue.overlay.close();
            }
        },
        save: {
            option: function (elm) {
                AjaxLoader.show();
                var $optionBoxChild = jQuery(elm).parents(".option-box-child");
                var varienFormId = $optionBoxChild.attr('id');

                var validationErrors = false;
                var optionValueFormPart = new varienForm(varienFormId);
                if (!optionValueFormPart.validator.validate()) {
                    validationErrors = true;
                }

                if (!validationErrors) {
                    var fields = $optionBoxChild.find("select, textarea, input").serializeArray();

                    var tmplItem = jQuery.tmplItem($optionBoxChild);
                    var tmpldata = tmplItem.data;

                    var templateIdObject = ConfigurationAdmin.helper.getTemplateObject(tmpldata);
                    fields.push(templateIdObject);

                    var url = ConfigurationAdmin.config.saveOption + 'id/' + tmpldata.id + '/so/1' + '?isAjax=true&form_key=' + window['FORM_KEY'];
                    jQuery.post(
                        url,
                        fields,
                        function (json, textStatus, XMLHttpRequest) {
                            try {
                                var data = JSON.parse(json);
                                var message = data['message'];
                                var option = data['option'];

                                if (message == 'success') {
                                    $optionBoxChild.find('.error-msg').hide();
                                    var $tmplContent = jQuery("#optionTypeTemplate").tmpl(option);
                                    var optionBoxSel = '#' + $optionBoxChild.parents('.option-box').attr('id');
                                    var $optionBox = jQuery(optionBoxSel);
                                    $optionBox.html('').replaceWith($tmplContent);
                                    JustsellingUplodify.initOption($tmplContent);
                                    ConfigurationAdmin.helper.option.overlay.close();
                                    ConfigurationAdmin.option.sort();
                                    jQuery('#option_' + option['id']).scrollTo();
                                    ConfigurationAdmin.helper.messages.clear(tmpldata.id);
                                    AjaxLoader.hide();
                                } else {
                                    $optionBoxChild.find('.error-msg').html(message).show();
                                    AjaxLoader.hide();
                                }
                            } catch (e) {
                                $optionBoxChild.find('.error-msg').html(e + '<br/>' + data).show();
                            }
                        }
                    );
                } else {
                    AjaxLoader.hide();
                }
            },
            optionValue: function (elm) {
                AjaxLoader.show();
                var $rowContainer = jQuery(elm).parents("tr");
                var rowId = $rowContainer.attr('id');
                var row2Id = rowId + '_2';

                var parentSelector = '#' + $rowContainer.attr('attr-parentid');
                var $parentContainer = jQuery(parentSelector);

                var validationErrors = false;
                var optionValueFormPart = new varienForm(rowId);
                if (!optionValueFormPart.validator.validate()) {
                    validationErrors = true;
                }
                var optionValueFormPart2 = new varienForm(row2Id);
                if (!optionValueFormPart2.validator.validate()) {
                    validationErrors = true;
                }

                if (!validationErrors) {
                    var fields1 = $rowContainer.find("select, textarea, input").serializeArray();
                    var fields2 = $rowContainer.next().find("select, textarea, input").serializeArray();
                    var fields = jQuery.merge(jQuery.merge([], fields1), fields2);

                    var $optionBox = jQuery(elm).parents(".option-box");
                    var tmplOptionItem = jQuery.tmplItem($optionBox);
                    var tmplOptiondata = tmplOptionItem.data;

                    var tmplOptionValueItem = jQuery.tmplItem($rowContainer);
                    var tmplOptionValuedata = tmplOptionValueItem.data;
                    // update values
                    tmplOptionValuedata.title = jQuery(tmplOptionValueItem.nodes).find("input[name$='[title]']").val();
                    tmplOptionValuedata.value = jQuery(tmplOptionValueItem.nodes).find("input[name$='[value]']").val();
                    tmplOptionValuedata.price = jQuery(tmplOptionValueItem.nodes).find("input[name$='[price]']").val();
                    tmplOptionValuedata.sku = jQuery(tmplOptionValueItem.nodes).find("input[name$='[sku]']").val();
                    tmplOptionValuedata.sort_order = jQuery(tmplOptionValueItem.nodes).find("input[name$='[sort_order]']").val();
                    tmplOptionValueItem.data = tmplOptionValuedata;

                    var templateIdObject = ConfigurationAdmin.helper.getTemplateObject(tmplOptiondata);
                    fields.push(templateIdObject);

                    var url = ConfigurationAdmin.config.saveOption + 'id/' + tmplOptiondata.id + '/sov/1?isAjax=true&form_key=' + window['FORM_KEY'];
                    jQuery.post(
                        url,
                        fields,
                        function (json, textStatus, XMLHttpRequest) {
                            var data = JSON.parse(json);
                            var message = data['message'];
                            var optionValueId = data['optionValueId'];

                            if (message == 'success') {
                                // add save id from backend and load template without input field
                                var tmplOptionValuedata = tmplOptionValueItem.data;
                                tmplOptionValuedata.id = optionValueId;
                                if (!tmplOptionValuedata['price']) {
                                    tmplOptionValuedata['price'] = 0;
                                }
                                if (!tmplOptionValuedata['sort_order']) {
                                    tmplOptionValuedata['sort_order'] = 0;
                                }
                                tmplOptionValueItem.data = tmplOptionValuedata;
                                var $tmplContent = jQuery("#optionTypeSelectRow").tmpl(tmplOptionValueItem.data);
                                $parentContainer.replaceWith($tmplContent);

                                if (ConfigurationAdmin.helper.option.overlay.isVisible()) {
                                    // add new optionValue to optioValues from option-box if new element
                                    var $optionBoxChild = jQuery(elm).parents(".option-box").find(".option-box-child");
                                    var optionBoxtmplItem = jQuery.tmplItem($optionBoxChild);
                                    if (!ConfigurationAdmin.helper.optionValue.array.inOptionArray(optionBoxtmplItem.data.values, optionValueId)) {
                                        optionBoxtmplItem.data.values.push(tmplOptionValueItem.data);
                                    }
                                    // reset all params
                                    var $optionTypeOption = $optionBoxChild.find('.select-product-option-type option:selected');
                                    ConfigurationAdmin.select.optionType($optionTypeOption);
                                }

                                ConfigurationAdmin.helper.optionValue.sort(tmplOptiondata.id);
                                ConfigurationAdmin.helper.defaultValue.set($optionBox, optionValueId, tmplOptionValuedata);
                                ConfigurationAdmin.helper.optionValue.overlay.close();
                                ConfigurationAdmin.helper.messages.clear(tmplOptiondata.id);
                                AjaxLoader.hide();
                            } else {
                                ConfigurationAdmin.helper.messages.errorOptionValue(message, tmplOptiondata.id);
                                ConfigurationAdmin.helper.optionValue.overlay.close();
                                AjaxLoader.hide();
                            }
                        }
                    );
                } else {
                    AjaxLoader.hide();
                }
            }
        },
        exportcsv: {
            matrix: function (elm) {
                var optionContainer = jQuery(elm).parents("div.option-box");
                var tmplItem = jQuery.tmplItem(optionContainer);
                var option = tmplItem.data;
                var url = ConfigurationAdmin.config.urlMatrixcsv + option.id;
                window.location = url;
            }
        },
        clearcache: function () {
            var url = ConfigurationAdmin.config.urlClearcache;
            var buttonhtml = jQuery("#clear_cache_button").html()
            jQuery("#clear_cache_button").html(ConfigurationAdmin.config.clearingCacheText).load(url, function () {
                jQuery("#clear_cache_button").html(buttonhtml)
            });
        }
    },
    upload: {
        csv: {
            optionValue: function (filename, optionId) {
                var optionType = ConfigurationAdmin.helper.option.getOptionType(optionId);
                var url = ConfigurationAdmin.config.saveOption + 'id/' + optionId + '/optiontype/' + optionType + '?isAjax=true&form_key=' + window['FORM_KEY'];
                jQuery.post(
                    url,
                    { optionvaluecsv: filename },
                    function (json, textStatus, XMLHttpRequest) {
                        var data = JSON.parse(json);
                        var message = data['message'];
                        var matrixstatus = data['matrixstatus'];
                        var matrixmessage = data['matrixmessage'];
                        var optionValues = data['optionValues'];
                        if (message == 'success') {
                            if (matrixstatus == 'success') {
                                for (var i = 0; i < optionValues.length; i++) {
                                    jQuery('#optionTypeSelectRow').tmpl({option_id: optionId, id: optionValues[i]['id'], title: optionValues[i]['title'], value: optionValues[i]['value'], price: optionValues[i]['price'], sku: optionValues[i]['sku'], sort_order: optionValues[i]['sort_order']}).appendTo("#select_option_type_row_" + optionId);
                                }
                                ConfigurationAdmin.helper.messages.clear(optionId);
                                ConfigurationAdmin.helper.messages.successOptionValue(matrixmessage, optionId);
                            }else{
                                ConfigurationAdmin.helper.messages.clear(optionId);
                                ConfigurationAdmin.helper.messages.errorOptionValue(matrixmessage, optionId);
                            }
                            AjaxLoader.hide();
                        } else {
                            ConfigurationAdmin.helper.messages.error(message);
                            AjaxLoader.hide();
                        }
                    }
                );
            },
            matrix: function (filename, optionId) {
                var optionType = ConfigurationAdmin.helper.option.getOptionType(optionId);

                var url = ConfigurationAdmin.config.saveOption + 'id/' + optionId + '/optiontype/' + optionType + '?isAjax=true&form_key=' + window['FORM_KEY'];

                var delimiter = jQuery('#matrix-delimiter-' + optionId).val();
                jQuery.post(
                    url,
                    {
                        matrixcsv: filename,
                        delimiter: delimiter
                    },
                    function (json, textStatus, XMLHttpRequest) {
                        var data = JSON.parse(json);
                        var message = data['message'];
                        var matrixstatus = data['matrixstatus'];
                        var matrixmessage = data['matrixmessage'];
                        var optionValues = data['optionValues'];
                        if (message == 'success') {
                            if (matrixstatus == 'success') {
                                jQuery('#matrixfilename-' + optionId).html(filename);
                                jQuery('#matrixfilenameParent-' + optionId).html(filename);
                                var $optionBox = jQuery('#option_' + optionId);
                                var tmplItem = jQuery.tmplItem($optionBox);
                                var tmpldata = tmplItem.data;
                                tmpldata['matrix_filename'] = filename;
                                var $optionBoxChild = $optionBox.find('.option-box-child')
                                var tmplItemChild = jQuery.tmplItem($optionBoxChild);
                                var tmpldataChild = tmplItemChild.data;
                                tmpldataChild['matrix_filename'] = filename;
                                ConfigurationAdmin.helper.messages.successOptionValue(matrixmessage, optionId);
                            }else{
                                ConfigurationAdmin.helper.messages.errorOptionValue(matrixmessage, optionId);
                            }
                            AjaxLoader.hide();
                        } else {
                            ConfigurationAdmin.helper.messages.errorOptionValue(message, optionId);
                            AjaxLoader.hide();
                        }
                    }
                );
            }
        }
    },
    helper: {
        ajax: {
            getOptionValueDetail: function (detailsSelector, optionId, optionValueId) {
                jQuery(detailsSelector).html(ConfigurationAdmin.config.loadingText).parents('tr').show();

                var optionType = ConfigurationAdmin.helper.option.getOptionType(optionId);

                var url = ConfigurationAdmin.config.urlValuedetails + optionId + '/value/' + optionValueId + '/optiontype/' + optionType;
                jQuery.get(
                    url,
                    function (data) {
                        jQuery(detailsSelector).html(data).show();
                        var images = jQuery(detailsSelector).find('.input-image');
                        for (var i = 0; i < images.length; i++) {
                            var id = jQuery(images[i]).attr('id');
                            JustsellingUplodify.flash.image(id);
                        }
                        AjaxLoader.hide();
                    }
                );
            },
            saveTemplate : function(){
                AjaxLoader.show();
                var fields = jQuery('#edit_form').find("select, textarea, input").serializeArray();

                var url = ConfigurationAdmin.config.save  +'?isAjax=true&form_key=' + window['FORM_KEY'];
                jQuery.ajax({
                    type: 'POST',
                    url: url,
                    data: fields,
                    success: function (json) {
                        try {
                            var message = json['message'];
                            var templateId = json['templateId'];

                            if (message == 'success') {
                                ConfigurationAdmin.config.templateId = templateId;
                                ConfigurationAdmin.config.templateCreate = true;
                                ConfigurationAdmin.helper.messages.clear(null);
                                var onclickDelete = "deleteConfirm('" + Translator.translate('deleteConfirm') + "' , '" + ConfigurationAdmin.config.deleteButtonUrl + "id/" + templateId +"')";
                                var button = '<button title="Löschen" type="button" class="scalable delete" onclick="' + onclickDelete  + '" ><span><span><span>Löschen</span></span></span></button>';
                                jQuery(".content-header button:nth-child(2)").after(button);
                                ConfigurationAdmin.helper.option.createNew();
                                ConfigurationAdmin.helper.messages.successOptionValue(Translator.translate('saveTemplateSuccess'),ConfigurationAdmin.config.newOptionId);
                                AjaxLoader.hide();
                            } else {
                                ConfigurationAdmin.helper.messages.error(message);
                                AjaxLoader.hide();
                            }
                        } catch (e) {
                          ConfigurationAdmin.helper.messages.error(e);
                          AjaxLoader.hide();
                        }
                    },
                    dataType: 'json',
                    async:false
                });
            }
        },
        option: {
            overlay: {
                open: function ($optionContainer, $tmplContent) {
                    var $overlay = jQuery('#option-ovl');
                    if ($overlay.length == 0) {
                        jQuery("body").append('<div id="option-ovl"></div>').show();
                        jQuery('#option-ovl').show();
                    } else {
                        $overlay.show();
                    }
                    var containerWidth = $optionContainer.width();
                    $optionContainer.find('.option-ovl-content').html($tmplContent.css('width', containerWidth)).show();
                },
                close: function () {
                    var $overlaycontent = jQuery('.option-ovl-content');
                    var $overlay = jQuery('#option-ovl');
                    $overlaycontent.hide();
                    $overlay.hide();
                    $overlaycontent.html('');
                },
                isVisible: function () {
                    var isVisible = false;
                    var $overlay = jQuery('#option-ovl');
                    if ($overlay.length != 0) {
                        isVisible = $overlay.is(':visible');
                    }
                    return isVisible;
                }
            },
            setSelectFields: function ($templateContent, option) {
                var $optionContainer = jQuery($templateContent);
                $optionContainer.find("select.select-product-option-type").val(option['type']);
                $optionContainer.find("select.select-product-option-is_require").val(option['is_require']);
                $optionContainer.find("select.select-product-option-value").val(option['value']);
                $optionContainer.find("select.select-product-option-placeholder").val(option['placeholder']);
                $optionContainer.find("select.select-product-option-upload_type").val(option['upload_type']);
                $optionContainer.find("select.select-product-option-is_visible").val(option['is_visible']);
                $optionContainer.find("select.select-product-option-apply_discount").val(option['apply_discount']);
                $optionContainer.find("select.select-product-option-option_group").val(option['option_group_id']);
                $optionContainer.find("select.select-product-option-font").val(option['font']);
                $optionContainer.find("select.select-product-option-matrix_operator_x").val(option['matrix_operator_x']);
                $optionContainer.find("select.select-product-option-matrix_operator_y").val(option['matrix_operator_y']);
                $optionContainer.find("select.select-product-listimage-hover").val(option['listimage_hover']);
                $optionContainer.find("select.select-product-listimage-style").val(option['listimage_style']);
                $optionContainer.find("select.select-product-option-text_validate").val(option['text_validate']);
                $optionContainer.find("select.select-product-option-product_id").val(option['product_id']);

                var parentId = $optionContainer.find("select.select-product-option-parent_id").val(option['parent_id']);
                parentId.find("option[value='" + option.id + "']").remove();

                if (option.type == 'combi' || option.type == 'selectcombi' || option.type == 'overlayimagecombi' || option.type == 'matrixvalue' || option.type == 'expression' || option.type == 'http' || option.type == 'matrixvalue') {
                    $optionContainer.find("select.select-temlate-option-operator").val(option['operator']);
                    $optionContainer.find("select.select-temlate-option-operator-value-price").val(option['operator_value_price']);
                }
                if (option.pricelist.length > 0) {
                    jQuery(option.pricelist).each(function (i, item) {
                        jQuery('#template_option_' + option.id + '_pricelist_' + item.id + '_operator').val(item['operator']);
                    });
                }

                var url = ConfigurationAdmin.config.urlOptionvalues + option.id + "?optionType=" + option['type'];
                jQuery($templateContent).find("select[name$='[default_value]']").html(ConfigurationAdmin.config.loadingText).load(
                    url,
                    function () {
                        $optionContainer.find("select.select-product-option-default_value").val(option['default_value']);
                    }
                ).show();
            },
            setDetails: function (optionId) {
                var urlOptionDetail = ConfigurationAdmin.config.urlOptiondetails + optionId  +'?form_key=' + window['FORM_KEY'];
                jQuery.ajax({
                    type: 'POST',
                    url: urlOptionDetail,
                    success: function (html) {
                      var $detail = jQuery("#template_option_" + optionId + "_details");
                      $detail.html(html).show();
                    },
                    dataType: 'html'
                });
            },
            createNew : function(){
                var option = emptyOption();
                var $optionBox = jQuery("#optionTypeTemplate").tmpl(option);
                $optionBox.prependTo("#product_options_container");
                ConfigurationAdmin.helper.option.setSelectFields($optionBox, option);

                option['child'] = 'child';
                ConfigurationAdmin.helper.option.clearOptionBoxContent($optionBox);
                var $tmplContent = jQuery("#optionTypeTemplateEdit").tmpl(option).appendTo("#product_options_container");
                ConfigurationAdmin.helper.option.overlay.open($optionBox, $tmplContent);
                ConfigurationAdmin.helper.option.setSelectFields($tmplContent, option);
                ConfigurationAdmin.helper.option.setDetails(option['id']);
                JustsellingUplodify.initOption($optionBox);
            },
            copy: {
                optionValues: function ($optionOvlContent, $optionBoxChild) {
                    var tmplItem = jQuery.tmplItem($optionBoxChild);
                    var tmpldata = tmplItem.data;
                    delete tmpldata["child"];
                    var $tmplContent = jQuery("#optionValueTemplate").tmpl(tmpldata);
                    var $childOptionValueContent = $optionOvlContent.find('.option-values-content');
                    var childId = $childOptionValueContent.attr('id');
                    var parentId = childId + '_parent';
                    var $parentOptionValuesContent = jQuery('#' + parentId);
                    $parentOptionValuesContent.replaceWith($tmplContent);
                    $parentOptionValuesContent.attr('id', childId);
                }
            },
            clearOptionBoxContent: function ($optionBox) {
                var $parentOptionValueContent = $optionBox.find('.option-values-content');
                var parentId = $parentOptionValueContent.attr('id');
                $parentOptionValueContent.attr('id', parentId + '_parent');
                $parentOptionValueContent.html('');
            },
            getOptionType: function (optionId) {
                var optionTypeSelector = "#template_option_" + optionId + "_type option:selected";
                var optionType = jQuery(optionTypeSelector).val();
                if (!optionType) {
                    var optionTypeSelector = "#optiontype_" + optionId;
                    optionType = jQuery(optionTypeSelector).html();
                }
                return optionType;

            }
        },
        optionValue: {
            overlay: {
                open: function (elm, $tmplContent) {
                    var $overlay = jQuery('#option-value-ovl');
                    if ($overlay.length == 0) {
                        jQuery("body").append('<div id="option-value-ovl"></div>').show();
                        jQuery('#option-value-ovl').show();
                    } else {
                        $overlay.show();
                    }
                    var position = elm.parents('.option-values-content').position();
                    var topPos = position.top - 10;
                    elm.parents('.option-box').find('.option-value-ovl-content').css('margin-top', topPos).html($tmplContent).show();


                },
                close: function () {
                    var $overlaycontent = jQuery('.option-value-ovl-content');
                    var $overlay = jQuery('#option-value-ovl');
                    $overlaycontent.hide();
                    $overlay.hide();
                    $overlaycontent.html('');
                    ConfigurationAdmin.helper.optionValue.scrollBackAferEdit();
                }
            },
            array: {
                inOptionArray: function (optionValues, optionValueId) {
                    var found = false;
                    for (var i = 0; i < optionValues.length; i++) {
                        if (optionValues[i].id == optionValueId) {
                            found = true;
                            break;
                        }
                    }
                    return found;
                }
            },
            addNew: function (elm) {
                var tmplItem = jQuery(elm).parents("tr").tmplItem();

                var $rowContainer = jQuery('#optionTypeSelectRow').tmpl({option_id: tmplItem.data.id, id: 0, title: "", sku: "", sort_order: ""}).appendTo("#select_option_type_row_" + tmplItem.data.id);
                var detailsSelector = "#template_option_" + tmplItem.data.id + "_details_" + 0;
                var tmplRowItem = jQuery.tmplItem($rowContainer);
                var $tmplContent = jQuery("#optionTypeSelectRowEdit").tmpl(tmplRowItem.data);

                ConfigurationAdmin.helper.optionValue.overlay.open($rowContainer, $tmplContent);
                ConfigurationAdmin.helper.ajax.getOptionValueDetail(detailsSelector, tmplRowItem.data.option_id, tmplRowItem.data.id);
                ConfigurationAdmin.helper.optionValue.scrollTopBeforeEdit('#option_' + tmplItem.data.id);
            },
            sort : function(optionId){
                var $optionTrSel = "#select_option_type_row_" + optionId + " tr";
                var $optionTrValues = jQuery($optionTrSel);
                $optionTrValues.orderBy(function () {
                    return +jQuery(this).find('.type-order').html();
                },function () {
                    return jQuery(this).find('.type-value').html();
                }).appendTo("#select_option_type_row_" + optionId);
            },
            scrollTopBeforeEdit : function(optionId){
                jQuery(optionId).scrollTo();
                ConfigurationAdmin.config.scrollTopMarker = jQuery(document).scrollTop();
            },
            scrollBackAferEdit : function(){
                if(ConfigurationAdmin.config.scrollTopMarker){
                    jQuery(document).scrollTop(ConfigurationAdmin.config.scrollTopMarker);
                    ConfigurationAdmin.config.scrollTopMarker = '';
                }
            }
        },
        defaultValue : {
          set : function($optionBox, optionValueId, tmplOptionValuedata){
              var $defaultValueElm = $optionBox.find('.option-default-value');
              var defaultValueId = $defaultValueElm.attr('attr-default');
              if(defaultValueId == optionValueId){
                  $defaultValueElm.html(tmplOptionValuedata['title']);
              }
          },
          clear : function($optionBox, optionValueId){
              var $defaultValueElm = $optionBox.find('.option-default-value');
              var defaultValueId = $defaultValueElm.attr('attr-default');
              if(defaultValueId == optionValueId){
                  $defaultValueElm.html('kein Standardwert');
              }
          }
        },
        next: {
            optionId: function () {
                ConfigurationAdmin.config.optionId++;
                return ConfigurationAdmin.config.optionId;
            },
            optionValueId: function () {
                ConfigurationAdmin.config.optionValueId++;
                return ConfigurationAdmin.config.optionValueId;
            },
            pricelistId: function () {
                ConfigurationAdmin.config.pricelistId++;
                return ConfigurationAdmin.config.pricelistId;
            },
            valuePricelistId: function () {
                ConfigurationAdmin.config.valuePricelistId++;
                return ConfigurationAdmin.config.valuePricelistId;
            }
        },
        getTemplateObject: function (tmplData) {
            var templateId = tmplData['template_id'];
            var templateIdObject = new Object;
            templateIdObject.name = 'templateId';
            templateIdObject.value = templateId;
            return templateIdObject;

        },
        messages : {
            error : function(errorText){
                var errorMessage = "<ul class='messages'><li class='error-msg'><ul><li><span>"+ errorText +"</span></li></ul></li></ul>";
                jQuery('#messages').html(errorMessage);
                jQuery(window).scrollTop(0);
            },
            errorOptionValue: function(errorText, optionId){
                var $optionBox = jQuery('#option_' + optionId);
                $optionBox.find('.success-msg').html('').hide();
                $optionBox.find('.error-msg').html(errorText).show();
            },
            successOptionValue: function(successText, optionId){
                var $optionBox = jQuery('#option_' + optionId);
                $optionBox.find('.error-msg').html('').hide();
                $optionBox.find('.success-msg').html(successText).show();
            },
            clear :  function(optionId){
                if(optionId){
                    var $optionBox = jQuery('#option_' + optionId);
                    $optionBox.find('.error-msg').html('').hide();
                    $optionBox.find('.success-msg').html('').hide();
                }
               jQuery('#messages').html('');
            }
        }
    }
};


var JustsellingUplodify = {
    config: {
        buttonWidth: 80,
        buttonHeight: 16,
        buttonClass: 'upload-button',
        buttonText: 'Upload...',
        baseJsUrl: '',
        buttonImage: '',
        swf: '',
        uploader: '',
        cancelImage: ''
    },
    init: function () {
        if (window.location.protocol == 'https:') {
            var baseUrl = JustsellingUplodify.config.baseJsUrl;
            if (baseUrl.indexOf("https") == -1) {
                JustsellingUplodify.config.baseJsUrl = baseUrl.replace('http', 'https');
            }
        }
        JustsellingUplodify.config.swf = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify.swf';
        JustsellingUplodify.config.uploader = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/uploadify-admin.php';
        JustsellingUplodify.config.cancelImage = JustsellingUplodify.config.baseJsUrl + 'justselling/configurator/upload/cancel.png';
        jQuery('.optionvaluecsv').each(function () {
            var id = jQuery(this).attr('id');
            var optionId = jQuery(this).attr('attr-optionId');
            JustsellingUplodify.flash.optionValueCsv(id, optionId);
        });
        jQuery('.templateimage').each(function () {
            var id = jQuery(this).attr('id');
            JustsellingUplodify.flash.tempalteimage(id);
        });
        jQuery('.file-wrapper-templateimage').on('click', 'a', function (e) {
            e.preventDefault();
            JustsellingUplodify.helper.deleteUpload(this);
        });
    },
    initOption: function (elm) {
        jQuery(elm).find('.optionvaluecsv').each(function () {
            var id = jQuery(this).attr('id');
            var optionId = jQuery(this).attr('attr-optionId');
            JustsellingUplodify.flash.optionValueCsv(id, optionId);
        });
        jQuery(elm).find('.matrixcsv').each(function () {
            var id = jQuery(this).attr('id');
            var optionId = jQuery(this).attr('attr-optionId');
            JustsellingUplodify.flash.matrixCsv(id, optionId);
        });
        var images = jQuery(elm).find('.input-image');
        for (var i = 0; i < images.length; i++) {
            var id = jQuery(images[i]).attr('id');
            var optionId = jQuery(images[i]).parents('.option-box').attr('attr-id');
            JustsellingUplodify.flash.image(id, optionId);
        }
        jQuery('.image-preview a').live('click', function (e) {
            e.preventDefault();
            JustsellingUplodify.helper.deleteUpload(this);
        });
    },
    flash: {
        image: function (id, optionId) {
            var uploadifyWrapper = jQuery('#file-wrapper-' + id);
            var uploadifyElement = jQuery("#" + id);

            uploadifyElement.uploadify({
                'formData': {
                    'filename': id
                },
                'swf': JustsellingUplodify.config.swf,
                'uploader': JustsellingUplodify.config.uploader,
                'buttonClass': JustsellingUplodify.config.buttonClass,
                'buttonText': JustsellingUplodify.config.buttonText,
                'cancelImage': JustsellingUplodify.config.cancelImage,
                'height': JustsellingUplodify.config.buttonHeight,
                'width': JustsellingUplodify.config.buttonWidth,
                onUploadError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onUploadError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onSelectError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onSelectError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onUploadSuccess: function (file, data, response) {
                    ConfigurationAdmin.helper.messages.clear(optionId);

                    var $uploadify = uploadifyWrapper.find(".uploadify");
                    var $previewImage = uploadifyWrapper.find(".image-preview");
                    var $queue = uploadifyWrapper.find(".uploadify-queue");

                    $previewImage.html(data);
                    $uploadify.addClass('hideuplodifybutton');
                    $previewImage.show();
                    var filename = $previewImage.find('.uplodifyimg').attr('attr-file');
                    jQuery('#hidden-' + id).val(filename);
                    $queue.html('');

                    var isOptionImage = uploadifyWrapper.attr('attr-optionimage');
                    if (isOptionImage && isOptionImage == '1') {
                        var $optionBoxChild = uploadifyWrapper.parents('.option-box-child');
                        var tmplItem = jQuery.tmplItem($optionBoxChild);
                        var tmpldata = tmplItem.data;
                        tmpldata['option_image'] = filename;
                        tmpldata['option_image_temp'] = 1;

                    }
                }
            });
            jQuery("#" + id).show();
            JustsellingUplodify.helper.changeUploadButtonVisisbility(id);
        },
        tempalteimage: function (id) {
            var uploadifyWrapper = jQuery('#file-wrapper-' + id);
            var uploadifyElement = jQuery("#" + id);
            uploadifyElement.uploadify({
                'swf': JustsellingUplodify.config.swf,
                'uploader': JustsellingUplodify.config.uploader,
                'buttonClass': JustsellingUplodify.config.buttonClass,
                'buttonText': JustsellingUplodify.config.buttonText,
                'cancelImage': JustsellingUplodify.config.cancelImage,
                'height': 20,
                'width': JustsellingUplodify.config.buttonWidth,
                'removeTimeout': 0,
                onUploadError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.error(Translator.translate('onUploadError') + errorMsg + '(' +errorCode2 + ')');
                },
                onSelectError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.error(Translator.translate('onSelectError') + errorMsg + '(' +errorCode2 + ')');
                },
                onUploadSuccess: function (file, data, response) {
                    ConfigurationAdmin.helper.messages.clear(null);
                    var filename = file['name'];
                    var $uploadify = uploadifyWrapper.find(".uploadify");
                    var $previewImage = uploadifyWrapper.find(".image-preview");
                    var $queue = uploadifyWrapper.find(".uploadify-queue");

                    $previewImage.html(data);
                    $uploadify.addClass('hideuplodifybutton');
                    $previewImage.show();
                    jQuery('#hidden-' + id).val(filename);
                    $queue.html('');
                }
            });
            jQuery("#" + id).show();
        },
        optionValueCsv: function (id, optionId) {
            var uploadifyElement = jQuery("#" + id);
            uploadifyElement.uploadify({
                'formData': {
                    'filename': id
                },
                'swf': JustsellingUplodify.config.swf,
                'uploader': JustsellingUplodify.config.uploader,
                'buttonClass': JustsellingUplodify.config.buttonClass,
                'buttonText': JustsellingUplodify.config.buttonText,
                'cancelImage': JustsellingUplodify.config.cancelImage,
                'height': JustsellingUplodify.config.buttonHeight,
                'width': JustsellingUplodify.config.buttonWidth,
                'fileTypeExts': '*.csv',
                'removeTimeout': 0,
                onUploadError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onUploadError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onSelectError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onSelectError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onUploadSuccess: function (file, data, response) {
                    ConfigurationAdmin.helper.messages.clear(optionId);
                    AjaxLoader.show();
                    var fileExtension = file['type'];
                    if (fileExtension.length <= 0) {
                        fileExtension = "." + file['name'].split(".")[1];
                    }
                    var filename = id + fileExtension;
                    ConfigurationAdmin.upload.csv.optionValue(filename, optionId);
                }
            });
            jQuery("#" + id).show();
        },
        matrixCsv: function (id, optionId) {
            var uploadifyElement = jQuery("#" + id);
            uploadifyElement.uploadify({
                'swf': JustsellingUplodify.config.swf,
                'uploader': JustsellingUplodify.config.uploader,
                'buttonClass': JustsellingUplodify.config.buttonClass,
                'buttonText': JustsellingUplodify.config.buttonText,
                'cancelImage': JustsellingUplodify.config.cancelImage,
                'height': JustsellingUplodify.config.buttonHeight,
                'width': JustsellingUplodify.config.buttonWidth,
                'fileTypeExts': '*.csv',
                'removeTimeout': 0,
                onUploadError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onUploadError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onSelectError: function (file, errorCode, errorCode2, errorMsg) {
                    ConfigurationAdmin.helper.messages.errorOptionValue(Translator.translate('onSelectError') + errorMsg + '(' +errorCode2 + ')', optionId);
                },
                onUploadSuccess: function (file, data, response) {
                    ConfigurationAdmin.helper.messages.clear(optionId);
                    AjaxLoader.show();
                    var filename = file['name'];
                    ConfigurationAdmin.upload.csv.matrix(filename, optionId);
                }
            });
            jQuery("#" + id).show();
        }
    },
    helper: {
        createWrapperSelector: function (id) {
            if (id.indexOf("file-wrapper") !== -1) {
                id = '#' + id;
            } else {
                id = '#file-wrapper-' + id;
            }
            return id;
        },
        changeUploadButtonVisisbility: function (id) {
            var wrapperSelector = JustsellingUplodify.helper.createWrapperSelector(id);
            var uploadifyWrapper = jQuery(wrapperSelector);
            uploadifyWrapper.find('.option-image-wrapper').show();
            var optionMulti = uploadifyWrapper.attr('attr-multi');
            if (!optionMulti || optionMulti == '0') {
                var imageSize = uploadifyWrapper.find('.image-preview img').size();
                var hideuplodifybutton = uploadifyWrapper.find('.hideuplodifybutton');
                if (imageSize == 0 || imageSize == '0') {
                    hideuplodifybutton.removeClass('hideuplodifybutton');
                }
                if (imageSize > 0 || imageSize == '1') {
                    uploadifyWrapper.find('.uploadify').addClass('hideuplodifybutton');
                }
            }
        },
        deleteUpload: function (elm) {
            var fileWrapper = jQuery(elm).parents('.file-wrapper');
            fileWrapper.find('.image-preview').html('');
            var id = fileWrapper.find('.uploadify').attr('id');
            jQuery('#hidden-' + id).val('');
            JustsellingUplodify.helper.changeUploadButtonVisisbility(id);

            var isOptionImage = fileWrapper.attr('attr-optionimage');
            if (isOptionImage && isOptionImage == '1') {
                var $optionBoxChild = fileWrapper.parents('.option-box-child');
                var tmplItem = jQuery.tmplItem($optionBoxChild);
                var tmpldata = tmplItem.data;
                tmpldata['option_image'] = '';
                tmpldata['option_image_temp'] = 0;
            }

        }
    }

};


var AjaxLoader = {
    config: {
        counter: '0'
    },
    show: function () {
        var $activity_overlay = jQuery('#activity-overlay');
        if ($activity_overlay.length == 0) {
            jQuery("body").append('<div id="activity-overlay" style="background: black; position: fixed; opacity: 0.4; filter: alpha(opacity=50); top: 0; left: 0; z-index: 20045; width: 100%; height: 100%;"></div>');
        } else {
            $activity_overlay.show();
        }
        jQuery('#activity-overlay').activity({color: '#fff'});
        AjaxLoader.config.counter = parseInt(AjaxLoader.config.counter) + 1;
    },
    hide: function () {
        AjaxLoader.config.counter = parseInt(AjaxLoader.config.counter) - 1;
        if (AjaxLoader.config.counter <= 0) {
            jQuery('#activity-overlay').activity(false).hide();
        }
    }
};


var TextImage = {
    config: {
        getfontconfigurationUrl: '',
        savefontUrl: ''
    },
    init: function () {
        var allFonts = jQuery("#all-fonts option");
        for (var i = 0; i < allFonts.length; i++) {
            var option = jQuery(allFonts[i]);
            if (!option.val()) {
                option.addClass('deleteAllFont');
            }
        }
        jQuery('.deleteAllFont').remove();

        jQuery("#textimage_overlay").overlay({
            top: 100,
            mask: {
                color: '#000',
                loadSpeed: 200,
                opacity: 0.5
            },
            closeOnClick: true,
            load: false
        });
        this.bindUIActions();
    },
    bindUIActions: function () {
        jQuery("#move-right").click(function () {
            var allFonts = jQuery("#all-fonts").val();
            if (allFonts) {
                for (var i = 0; i < allFonts.length; i++) {
                    var value = allFonts[i];
                    jQuery("#selected-fonts").append('<option value="' + value + '">' + jQuery("#all-fonts option[value=" + value + "]").text() + '</option>');
                    jQuery("#all-fonts  option[value=" + value + "]").remove();
                }
            }
        });
        jQuery("#move-left").click(function () {
            var selectedFonts = jQuery("#selected-fonts").val();
            if (selectedFonts) {
                for (var i = 0; i < selectedFonts.length; i++) {
                    var value = selectedFonts[i];
                    jQuery("#all-fonts").append('<option value="' + value + '">' + jQuery("#selected-fonts option[value=" + value + "]").text() + '</option>');
                    jQuery("#selected-fonts  option[value=" + value + "]").remove();
                }
            }
        });
        jQuery("#add_color").click(function () {
            jQuery("#selected-colors").append('<option value="' + jQuery('#color_code').val() + '">' + jQuery("#color_code").val() + " " + jQuery("#color_title").val() + '</option>');
        });
        jQuery("#delete_color").click(function () {
            jQuery("#selected-colors  option[value=" + jQuery('#selected-colors').val() + "]").remove();
        });
        jQuery("#add_pos").click(function () {
            jQuery("#selected-pos").append('<option value="' + jQuery('#pos_x').val() + "-" + jQuery('#pos_y').val() + '">' + jQuery("#pos_x").val() + "-" + jQuery("#pos_y").val() + " " + jQuery("#pos_title").val() + '</option>');
        });
        jQuery("#delete_pos").click(function () {
            jQuery("#selected-pos  option[value=" + jQuery('#selected-pos').val() + "]").remove();
        });
        jQuery("#moveup_color").click(function () {
            jQuery('#selected-colors option:selected').each(function () {
                jQuery(this).insertBefore(jQuery(this).prev());
            });
        });
        jQuery("#movedown_color").click(function () {
            jQuery('#selected-colors option:selected').each(function () {
                jQuery(this).insertAfter(jQuery(this).next());
            });
        });
        jQuery("#moveup_font").click(function () {
            jQuery('#selected-fonts option:selected').each(function () {
                jQuery(this).insertBefore(jQuery(this).prev());
            });
        });
        jQuery("#movedown_font").click(function () {
            jQuery('#selected-fonts option:selected').each(function () {
                jQuery(this).insertAfter(jQuery(this).next());
            });
        });
        jQuery("#moveup_pos").click(function () {
            jQuery('#selected-pos option:selected').each(function () {
                jQuery(this).insertBefore(jQuery(this).prev());
            });
        });
        jQuery("#movedown_pos").click(function () {
            jQuery('#selected-pos option:selected').each(function () {
                jQuery(this).insertAfter(jQuery(this).next());
            });
        });
        jQuery("#color_code").change(function () {
            jQuery("#color_preview").css("background-color", jQuery('#color_code').val());
        });
        jQuery(".textimage_configure_save_button").click(function () {
            var url = TextImage.config.savefontUrl;
            var fontIds = jQuery('#selected-fonts option').map(function (i, n) {
                return jQuery(n).attr('value');
            }).get().join(',');
            var fontColors = jQuery('#selected-colors option').map(function (i, n) {
                return jQuery(n).text();
            }).get().join(',');
            var fontPositions = jQuery('#selected-pos option').map(function (i, n) {
                return jQuery(n).text();
            }).get().join(',');
            new Ajax.Request(url, {
                asynchronous: false,
                parameters: {
                    optionid: jQuery("#option_id").val(),
                    fonts: fontIds,
                    min_font_size: jQuery("#min_font_size").val(),
                    max_font_size: jQuery("#max_font_size").val(),
                    min_font_angle: jQuery("#min_font_angle").val(),
                    max_font_angle: jQuery("#max_font_angle").val(),
                    choose_font: (jQuery("#choose-font").is(':checked') ? 1 : 0),
                    choose_font_size: (jQuery('#choose-font-size').is(':checked') ? 1 : 0),
                    choose_font_angle: (jQuery("#choose-font-angle").is(':checked') ? 1 : 0),
                    choose_font_color: (jQuery("#choose-font-color").is(':checked') ? 1 : 0),
                    choose_font_pos: (jQuery("#choose-font-pos").is(':checked') ? 1 : 0),
                    choose_text_alignment: (jQuery("#choose-text-alignment").is(':checked') ? 1 : 0),
                    colors: fontColors,
                    pos: fontPositions
                },
                onSuccess: function (response) {
                    console.log(response.responseText);
                    jQuery("#textimage_overlay").overlay().close();
                },
                onFailure: function () {
                    console.log("error");
                }
            });
        });
    },
    overlay: {
        show: function (elm) {
            var optionBox = jQuery(elm).parents('.option-box');
            optionBox.find('.admin_overlay .loadingtext').html(ConfigurationAdmin.config.loadingText).show();
            optionBox.find('.admin_overlay .textimage-content').hide();

            optionBox.find('.admin_overlay').overlay().load();

            var optionId = optionBox.attr('id').replace('option_', '');

            var url = TextImage.config.getfontconfigurationUrl;
            new Ajax.Request(url, {
                asynchronous: false,
                parameters: {
                    optionid: optionId
                },
                onSuccess: function (response) {
                    var values = jQuery.parseJSON(response.responseText);
                    console.log(values);
                    jQuery("#min_font_size").val(values.min_font_size);
                    jQuery("#max_font_size").val(values.max_font_size);
                    jQuery("#min_font_angle").val(values.min_font_angle);
                    jQuery("#max_font_angle").val(values.max_font_angle);
                    if (values.choose_font == "1") jQuery("#choose-font").prop("checked", true);
                    if (values.choose_font_size == "1") jQuery("#choose-font-size").prop("checked", true);
                    if (values.choose_font_angle == "1") jQuery("#choose-font-angle").prop("checked", true);
                    if (values.choose_font_color == "1") jQuery("#choose-font-color").prop("checked", true);
                    if (values.choose_font_pos == "1") jQuery("#choose-font-pos").prop("checked", true);
                    if (values.choose_text_alignment == "1") jQuery("#choose-text-alignment").prop("checked", true);

                    jQuery.each(values.fonts, function (index, value) {
                        console.log("font=", index, " ", value);
                        jQuery("#selected-fonts").append('<option value="' + index + '">' + value + '</option>');
                        jQuery("#all-fonts  option[value=" + index + "]").remove();
                    });

                    jQuery.each(values.colors, function (index, value) {
                        console.log("color=", index, " ", value);
                        jQuery("#selected-colors").append('<option value="' + index + '">' + index + " " + value + '</option>');
                    });

                    jQuery.each(values.pos, function (index, value) {
                        console.log("pos=", index, " ", value);
                        jQuery("#selected-pos").append('<option value="' + index + '">' + index + " " + value + '</option>');
                    });


                    optionBox.find('.admin_overlay .loadingtext').html('').hide();
                    optionBox.find('.admin_overlay .textimage-content').show();

                    console.log("ready");
                },
                onFailure: function () {
                    console.log("error");
                }
            });
        }
    }
};

function extendJQUery() {
    jQuery.fn.orderBy = function (keySelector, keySelector2) {
        return this.sort(function (a, b) {
            var sort1A = keySelector.apply(a);
            var sort1B = keySelector.apply(b);
            if (sort1A > sort1B)
                return 1;
            if (sort1A < sort1B)
                return -1;

            if (keySelector2) {
                a = keySelector2.apply(a);
                b = keySelector2.apply(b);

                if (typeof a === 'string') {
                    a = a.toLowerCase();
                }
                if (typeof b === 'string') {
                    b = b.toLowerCase();
                }

                if (a > b)
                    return 1;
                if (a < b)
                    return -1;

                return 0;
            } else {
                return 0;
            }

        });
    };
    jQuery.fn.scrollTo = function () {
        jQuery('html, body').animate({
            scrollTop: (jQuery(this).offset().top - 30) + 'px'
        }, 'fast');
        return this;
    };
}

function emptyOption() {
    var optionId = 0;
    ConfigurationAdmin.config.newOptionId = optionId;

    var option = {
        alt_title: "",
        apply_discount: null,
        decimal_place: 2,
        default_title: "",
        default_value: null,
        expression: null,
        font: null,
        font_angle: null,
        font_color: null,
        font_pos_x: null,
        font_pos_y: null,
        font_size: null,
        font_width_x: null,
        font_width_y: null,
        id: optionId,
        is_require: 1,
        is_visible: 1,
        listimage_hover: null,
        listimage_items_per_line: null,
        listimage_style: null,
        matrix_csv_delimiter: ",",
        matrix_dimension_x: null,
        matrix_dimension_y: null,
        matrix_filename: null,
        matrix_operator_x: null,
        matrix_operator_y: null,
        max_characters: "0",
        max_value: null,
        min_value: null,
        operator: null,
        operator_value_price: "none",
        option_group_id: null,
        option_image: null,
        parent_id: null,
        parent_title: null,
        placeholder: null,
        price: "0",
        pricelist: new Array(),
        product_attribute: null,
        product_id: null,
        sku: null,
        sort_order: "0",
        template_id: ConfigurationAdmin.config.templateId,
        text_validate: null,
        title: "",
        type: null,
        upload_filetypes: null,
        upload_maxsize: null,
        upload_type: null,
        url: null,
        value: null,
        values: new Array()
    };
    return option;
}