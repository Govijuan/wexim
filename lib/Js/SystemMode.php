<?php
/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_jscoreutils
 * @copyright   Copyright (c) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 * @author      Bodo Schulte
 */
class Js_SystemMode extends Js_Abstract {

	/**
	 * @return boolean True in case the service mode is 'production', false otherwise. In case the core module is not installed, true is returned.
	 */
	public function isProduction() {
		if ($mode = self::_getSystemMode()) {
			return $mode->isProduction();
		}
		return true;
	}

	/**
	 * @return JsSystemMode|bool false if not set in system
	 */
	protected static function _getSystemMode() {
		if ($utils = self::getCoreUtils()) {
			/* @var $mode JsSystemMode */
			$mode = $utils->getMode();
			return $mode;
		}
		return false;
	}

	/**
	 * @return boolean True in case the service mode is 'production', false otherwise. In case the core module is not installed, true is returned.
	 */
	public static function isScopeProduction() {
		$systemMode = self::_getSystemMode();
		return $systemMode ? $systemMode->isProduction() : true;
	}

	/**
	 * @return string the system's operation scope (development|production|staging)
	 */
	public static function getSystemScope() {
		if ($utils = self::getCoreUtils()) {
			/* @var $mode JsSystemMode */
			$mode = $utils->getMode();
			return $mode->getOperationScope();
		}
		return 'production';
	}

	/**
	 * @return string the system's operation mode (master|slave)
	 */
	public static function getSystemMode() {
		if ($mode = self::_getSystemMode()) {
			return $mode->getOperationMode();
		}
		return 'slave';
	}

	/** @return boolean True in case the service mode is 'staging', false otherwise. In case the core module is not installed, false is returned. */
	public function isStaging() {
		if ($mode = self::_getSystemMode()) {
			return $mode->isStaging();
		}
		return false;
	}

	/**
	 * @return boolean True in case the service scope is 'staging', false otherwise. In case the core module is not installed, true is returned.
	 */
	public static function isScopeStaging() {
		$systemMode = self::_getSystemMode();
		return $systemMode ? $systemMode->isStaging() : false;
	}


	/**
	 * @return boolean True in case the service mode is 'development', false otherwise. In case the core module is not installed, false is returned.
	 */
	public function isDevelopment() {
		if ($mode = self::_getSystemMode()) {
			return $mode->isDevelopment();
		}
		return false;
	}

	/**
	 * @return boolean True in case the service scope is 'development', false otherwise. In case the core module is not installed, true is returned.
	 */
	public static function isScopeDevelopment() {
		$systemMode = self::_getSystemMode();
		return $systemMode ? $systemMode->isDevelopment() : false;
	}

	/**
	 * @return boolean True in case the operation mode is 'master', false otherwise.
	 */
	public static function isModeMaster() {
		$systemMode = self::_getSystemMode();
		return $systemMode ? $systemMode->isMaster() : false;
	}

	/**
	 * @return boolean True in case the operation mode is 'slave', false otherwise.
	 */
	public static function isModeSlave() {
		$systemMode = self::_getSystemMode();
		return $systemMode ? $systemMode->isSlave() : true;
	}

}
?>