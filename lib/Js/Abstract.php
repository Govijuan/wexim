<?php
/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_
 * @copyright   Copyright (c) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 * @author      Bodo Schulte
 */
abstract class Js_Abstract {
    
    
    /** @return Justselling_CoreUtils_Helper_Data|bool false in case the justselling core utils logger is not available */
    protected static function getCoreUtils() {
		if (!method_exists(Mage::getConfig(), 'getHelperClassName')) {
			return false;
		}
		$helper = Mage::getConfig()->getHelperClassName('jscoreutils');
		return @class_exists($helper) ? Mage::helper('jscoreutils') : false;
    }
    
    /**
     * Returns the module name if it can be detected. In case the given parameter is a class the module name will be 
     * extracted, in case it is a string, it simple will be returned.
     * 
     * @param mixed $module object instance or string
     * @return string|bool false in case a module name could not be extracted
     */
    protected static function getModuleName($module) {
        if (is_object($module)) {
            $className = get_class($module);
            $parts = explode('_', $className);
            if (count($parts) >= 2) {
                return strtolower($parts[1]);
            }
            return false;
        } else if (is_string($module)) {
            return strtolower($module);
        }
        return false;
    }
}
?>