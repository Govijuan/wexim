<?php
/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_jscoreutils
 * @copyright   Copyright (c) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 * @author      Bodo Schulte
 */
class Js_Log extends Js_Abstract {
    
    /**
     * Logs the given message to the appropriate log file.
     * 
     * @param string $message The message to log
     * @param string|mixed $module OPTIONAL Any module class or a string
     * @param Zend_Log $level OPTIONAL The log level
     * @param bool $forceLog OPTIONAL true forces the log to be written even if deactivated
     */
    public static function log($message, $module=null, $level=Zend_Log::DEBUG, $forceLog=false) {
        if ($utils = self::getCoreUtils()) {
            /* @var $logger JsLogger */
            $logger = $utils->getLogger();
            $logger->log($message, $level, self::getModuleName($module), $forceLog);
            return;
        }
		$logFile = self::getModuleName($module) ? self::getModuleName($module).'.log' : '';
        Mage::log($message, $level, $logFile, $forceLog);
    }
    
    /**
     * Logs an exception
     * 
     * @param Exception $e
     * @param string|mixed $module OPTIONAL Any module class or a string
     * @param string $msg OPTIONAL message text
     */
    public static function logException(Exception $e, $module, $msg='') {
        if ($utils = self::getCoreUtils()) {
            /* @var $logger JsLogger */
            $logger = $utils->getLogger();
            $logger->logError((empty($msg)) ? 'An unexpected problem (exception) occurs!' : $msg, $e, Zend_Log::ERR, self::getModuleName($module), true);
            return;
        }
        Mage::logException($e);
    }
    

}
?>