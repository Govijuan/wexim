<?php
/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_jscoreutils
 * @copyright   Copyright (c) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 * @author      Bodo Schulte
 */
class Js_Announcement extends Js_Abstract {

    
    /**
     * Sends an announcement to the given list of (email-)recipients. In case the core module is not available, a log INFO will be written. 
     * 
     * @param array $recipients List of e-mail recipients
     * @param string $message The message to announce
     * @param string $subject The message subject (optional)
     */
    public static function announce($recipients=array(), $message, $subject='') {
        if ($utils = self::getCoreUtils()) {
            $a = new JsAnnouncement($recipients, $message);
            if (!empty($subject)) $a->setSubject($subject);
            $a->send();
            return;
        }
        Mage::log($subject.$message, Zend_Log::INFO, self::getModuleName($module), true);
    }
}
?>