
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function hexToString(hex) {
    var bytes = [];
    for(var i = 0; i < hex.length - 1; i += 2) {
        bytes.push(parseInt(hex.substr(i, 2), 16));            
    }
    return String.fromCharCode.apply(String, bytes);
}

var TEMPLATE_OPTIONS = new Array();

$(document).on('keyup', 'input:text', function(event) {
    if(event.keyCode == '13') {
        event.preventDefault();
        $(this).trigger('blur');
    }
});

$(document).on('click', 'button', function(event) {
    event.preventDefault();
    var send = new Object;
    var id = this.id;
    var templateNameHex = id.substring(id.indexOf('[') + 1, id.indexOf(']'));
    var row = $(this).closest('.row');
    var empty = true;
    row.find('input:text').each(function() {
        if(this.value.length)
            empty = false;
    });
    if(!empty) {
        if(!confirm("Achtung: Diese Zeile enthält Daten!\n\nMöchten Sie sie wirklich unwiderruflich löschen?"))
            return;
    }
    
    send.template_name = hexToString(templateNameHex);
    send.id = id.substring(id.lastIndexOf('[') + 1, id.lastIndexOf(']'));
    
    $(row).remove();
    if(!send.id.length) {
        return;
    }

    $.ajax({
        type: "POST",
        url: '/matrixcfg/index.php/update-matrix/delete',
        data: send
    });    
});

$(document).on('focus', 'input:text', function() {
    $(this).css({'color': 'black', 'border-color': '#888'});
});

$(document).on('blur', 'input:text', function() {
    var input = this;
    var name = input.name;
    var send = new Object;

    if(name.indexOf('new-rows-amount') != -1)
        return;

    send.field = name.substring(0, name.indexOf('['));
    send.template_name = hexToString(name.substring(name.indexOf('[') + 1, name.indexOf(']')));
    send.id = name.substring(name.lastIndexOf('[') + 1, name.lastIndexOf(']'));
    send.value = this.value;
    
    if(!send.id.length)
        send.id = '-1';
    
    $.ajax({
        type: "POST",
        url: '/matrixcfg/index.php/update-matrix/',
        data: send
    }).done(function(data) {
        if(data.length) {
            var row = $(input).closest('.row');
            row.find('input:text').each(function() {
                var name = this.name;
                $(this).attr('name', name.replace('[]', '[' + data + ']'));
            });
            row.find('button').each(function() {
                var id = this.id;
                $(this).attr('id', id.replace('[]', '[' + data + ']'));
            });
        }
        
        $(input).stop()
            .animate({"border-color": '#fff'}, 100)
            .animate({"border-color": '#8b8'}, 100)
            .animate({"color": '#8b8'}, 0);
        //alert(data);
    }); 
});

$(document).ready(function($) {    
        
    $('.new-rows-button').click(function() {
        var button = this;
        var name = button.name;
        var templateNameHex = name.substring(name.indexOf('[') + 1, name.indexOf(']'));
        var templateName = hexToString(templateNameHex);

        var templateFound = false;
        var templateIndex = -1;
        for(i = 0; i < TEMPLATE_OPTIONS.length; i++) {
            if(TEMPLATE_OPTIONS[i].templateName == templateName) {
                templateFound = true;
                templateIndex = i;
                //break;
            }
        }
        
        var columns = TEMPLATE_OPTIONS[templateIndex].columns;
        var tableColumns = "<div>{0}</div>";
        for(i = 0; i < columns.length; i++) {
            if(columns[i] == 'image_front') {
                tableColumns += '<div class="separator">&raquo;&raquo;&raquo;</div>';
            }
            tableColumns += '<div><input type="text" name="' + columns[i] + '[' + templateNameHex + '][]" value=""></div>';
            if(columns[i] == 'delivery_time') {
                tableColumns += '<div class="button"><button id="remove_row[' + templateNameHex + '][]">Zeile löschen</button></div>';
            }
        }
        
        var container = $(this).closest('.table-data');        
        var rowCount = -1;
        $(container).find('.row').each(function() {
            rowCount++;
        });
        
        var amount = $(this).parent().find('input').val();
        for(i = 0; i < amount; i++) {
            $(this).closest('.row.footer').before('<form id="template[' + templateNameHex + ']"><div class="row">' + tableColumns.format((rowCount++).toString()) + '</div>');
        }
    });
});
