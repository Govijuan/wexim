
$(document).ready(function($) {
    var $items = $('.vertical-tabs>ul>li');
    $items.click(function() {
        $items.removeClass('selected');
        $(this).addClass('selected');

        var index = $items.index($(this));
        $('.vertical-tabs>div').hide().eq(index).show();
    }).eq(0).click();
});
