<?php

class UpdateMatrixController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function preDispatch() {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function indexAction() {
        if (!$this->getRequest()->isPost()) {
            return $this->_forward('index');
        }
        $post = $this->getRequest()->getPost();
        $matrix = new Application_Model_MatrixConfigMapper();
        if($post['id'] == '-1') {
            $id = $matrix->insertRow($post['template_name']);
            echo $id;
        }
        else
            $id = $post['id'];
        $matrix->updateCell($id, $post['field'], $post['value']);
    }

    public function deleteAction() {
        if (!$this->getRequest()->isPost()) {
            return $this->_forward('index');
        }
        $post = $this->getRequest()->getPost();
        $matrix = new Application_Model_MatrixConfigMapper();
        $id = $post['id'];
        $templateName = $post['template_name'];
        $matrix->deleteRow($id, $templateName);
    }

}

