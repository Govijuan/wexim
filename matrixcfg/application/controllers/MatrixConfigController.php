<?php

class MatrixConfigController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $templatesData = new Application_Model_ConfiguratorTemplateMapper();
        $entries = $templatesData->fetchAll();
        
        $tableIntegrity = new Application_Model_ConfiguratorTableIntegrity();
        $tableIntegrity->checkIntegrity($entries);
        
        $this->view->entries = $entries;
    }

}

