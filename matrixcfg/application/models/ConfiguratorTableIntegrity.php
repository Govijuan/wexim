<?php

class Application_Model_ConfiguratorTableIntegrity {
    
    protected $_columns;
    protected $_dbTable;

    public function setDbTable($dbTable) {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Ungültiges Table Data Gateway angegeben');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable() {
        if(null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ConfiguratorTableIntegriy');
        }
        return $this->_dbTable;
    }
    
    public function fetchColumns() {
        $this->_columns = $this->getDbTable()->info(Zend_Db_Table_Abstract::COLS);
        return $this;
    }
    
    public function checkIntegrity($data) {
        $this->fetchColumns();
        $requiredColumns = array();
        foreach($data as $entry) {
            foreach($entry->getTemplateOptions() as $option) {
                if(!in_array($option->getAltTitle(), $requiredColumns) && !in_array($option->getAltTitle(), $this->_columns))
                    $requiredColumns[] = $option->getAltTitle();
            }
        }
        if(count($requiredColumns))
            $this->createIntegrity($requiredColumns);
        return $this;
    }
    
    public function createIntegrity($newColumns) {
        $sql = "ALTER TABLE `" . $this->getDbTable()->getName() . "` ";
        foreach($newColumns as $index => $column) {
            $sql .= "ADD COLUMN `{$column}` VARCHAR(32) NOT NULL AFTER `template_name`";
            if($index < count($newColumns) - 1)
                $sql .= ", ";
        }
        $this->getDbTable()->getAdapter()->query($sql);
        
        return $this;
    }

}

