<?php

class Application_Model_ConfiguratorOptionMapper {

    protected $_dbTable;
 
    public function setDbTable($dbTable) {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Ungültiges Table Data Gateway angegeben');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable() {
        if(null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ConfiguratorOption');
        }
        return $this->_dbTable;
    }

    public function fetchAllByTemplateId($template_id) {
        $db = $this->getDbTable();
        $select = $db->select()->where('template_id = ?', $template_id);
        $resultSet = $db->fetchAll($select);
        $entries = $this->_assignEntries($resultSet);
        return $entries;
    }
    
    // ****
    
    protected function _assignEntries($resultSet) {
        $entries = array();
        foreach($resultSet as $row) {
            $entry = new Application_Model_ConfiguratorOption();
            $entry->setId($row->id)->setTemplateId($row->template_id)->setTitle($row->title)->setAltTitle($row->alt_title);
            $entries[] = $entry;
        }
        return $entries;
    }
}

