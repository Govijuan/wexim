<?php

class Application_Model_ConfiguratorTemplateMapper {
    
    protected $_dbTable;
 
    public function setDbTable($dbTable) {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Ungültiges Table Data Gateway angegeben');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable() {
        if(null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_ConfiguratorTemplate');
        }
        return $this->_dbTable;
    }

    public function fetchAll() {
        $db = $this->getDbTable();
        $select = $db->select()->order('title ASC');
        $resultSet = $db->fetchAll($select);
        $entries = $this->_assignEntries($resultSet);
        return $entries;
    }
    
    // ****
    
    protected function _assignEntries($resultSet) {
        $entries = array();
        foreach($resultSet as $row) {
            $entry = new Application_Model_ConfiguratorTemplate();
            $entry->setId($row->id)->setTitle($row->title)->setTemplateOptions()->setTemplateData();
            $entries[] = $entry;
        }
        return $entries;
    }

}