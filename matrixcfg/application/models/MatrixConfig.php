<?php

class Application_Model_MatrixConfig {
    
    protected $_id;
    protected $_templateName;
    protected $_image;
    protected $_deliveryTime;
    protected $_data;
    protected $_templateOptions;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        $this->$method($value);
    }
 
    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        return $this->$method();
    }
 
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId() {
        return $this->_id;
    }
 
    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setTemplateOptions($options) {
        $this->_templateOptions = $options;
        return $this;
    }
    
    public function getTemplateOptions() {
        return $this->_templateOptions;
    }
    
    public function setTemplateName($name) {
        $this->_templateName = $name;
        return $this;
    }
    
    public function getTemplateName() {
        return $this->_templateName;
    }

    public function setImage($image) {
        $this->_image = $image;
        return $this;
    }
    
    public function getImage() {
        return $this->_image;
    }
    
    public function setDeliveryTime($delivery_time) {
        $this->_deliveryTime = $delivery_time;
        return $this;
    }
    
    public function getDeliveryTime() {
        return $this->_deliveryTime;
    }
    
    public function setData(array $data) {
        $temp_data = array();
        foreach($data as $key => $value) {
            switch($key) {
                case "id":
                case "template_name":
                case "delivery_time":
                case "image_front":
                case "image_back":
                    break;
                default:
                    if($this->_isKeyOfTemplate($key))
                        $temp_data[$key] = $value;
            }
        }
        
        $order = array();
        foreach($this->_templateOptions as $option) {
            $altTitle = $option->getAltTitle();
            if(!in_array($altTitle, $order))
                $order[] = $altTitle;
        }
        $this->_data = array_merge(array_flip($order), $temp_data);
        return $this;
    }
    
    public function getData() {
        return $this->_data;
    }
    
    // ****
    
    protected function _isKeyOfTemplate($key) {
        $result = FALSE;
        foreach($this->getTemplateOptions() as $option) {
            if($option->getAltTitle() == $key) {
                $result = TRUE;
                break;
            }
        }
        return $result;
    }
    
}