<?php

class Application_Model_MatrixConfigMapper {
    
    protected $_dbTable;
 
    public function setDbTable($dbTable) {
        if(is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if(!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Ungültiges Table Data Gateway angegeben');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable() {
        if(null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_MatrixConfig');
        }
        return $this->_dbTable;
    }
    
    public function save(Application_Model_MatrixConfig $entry) {
        $id = $entry->getId();
        $data = $entry->getData();
        if(null === $id) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    
    public function updateCell($id, $field, $value) {
        $data = array($field => $value);
        $this->getDbTable()->update($data, array('id = ?' => $id));
    }
    
    public function deleteRow($id, $templateName) {
        $data = array('id = ?' => $id, 'template_name = ?' => $templateName);
        $this->getDbTable()->delete($data);
    }
    
    public function insertRow($templateName) {
        $data = array('template_name' => $templateName);
        return $this->getDbTable()->insert($data);
    }
    
    public function fetchAllByTemplateName($name, $availableOptions) {
        $db = $this->getDbTable();
        $select = $db->select()->where('template_name = ?', $name);
        $resultSet = $db->fetchAll($select);
        $entries = $this->_assignEntries($resultSet, $availableOptions);
        return $entries;
    }
    
    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = $this->_assignEntries($resultSet);
        return $entries;
    }
    
    // ****
    
    protected function _assignEntries($resultSet, $availableOptions) {
        $entries = array();
        foreach($resultSet as $row) {
            $entry = new Application_Model_MatrixConfig();
            $entry->setId($row->id)
                ->setTemplateOptions($availableOptions)
                ->setTemplateName($row->template_name)
                ->setDeliveryTime($row->delivery_time)
                ->setImage($row->image_front)
                ->setData($row->toArray());
            $entries[] = $entry;
        }
        return $entries;
    }
}

