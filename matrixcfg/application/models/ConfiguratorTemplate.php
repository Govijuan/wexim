<?php

class Application_Model_ConfiguratorTemplate {
    protected $_id;
    protected $_title;
    protected $_templateOptions;
    protected $_templateData;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        $this->$method($value);
    }
 
    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        return $this->$method();
    }
 
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId() {
        return $this->_id;
    }
    
    public function setTitle($title) {
        $this->_title = $title;
        return $this;
    }

    public function getTitle() {
        return $this->_title;
    }
    
    public function setTemplateOptions() {
        $this->_templateOptions = $this->_getTemplateOptions();
        return $this;
    }

    public function getTemplateOptions() {
        return $this->_templateOptions;
    }
    
    public function setTemplateData() {
        $this->_templateData = $this->_getTemplateData();
        return $this;
    }
    
    public function getTemplateData() {
        return $this->_templateData;
    }
    
    // ****
    
    protected function _getTemplateOptions() {
        $options = new Application_Model_ConfiguratorOptionMapper();
        return $options->fetchAllByTemplateId($this->_id);
    }
    
    protected function _getTemplateData() {
        $data = new Application_Model_MatrixConfigMapper();
        return $data->fetchAllByTemplateName($this->_title, $this->_templateOptions);
    }
}

