<?php

class Application_Model_ConfiguratorOption {
    protected $_id;
    protected $_templateId;
    protected $_title;
    protected $_altTitle;
    
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        $this->$method($value);
    }
 
    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Ungültige Klassen-Eigenschaft');
        }
        return $this->$method();
    }
 
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId() {
        return $this->_id;
    }
    
    public function setTemplateId($templateId) {
        $this->_templateId = $templateId;
        return $this;
    }

    public function getTemplateId() {
        return $this->_templateId;
    }
    
    public function setTitle($title) {
        $this->_title = $title;
        return $this;
    }

    public function getTitle() {
        return $this->_title;
    }
    
    public function setAltTitle($altTitle) {
        $this->_altTitle = $altTitle;
        return $this;
    }

    public function getAltTitle() {
        return $this->_altTitle;
    }
}

