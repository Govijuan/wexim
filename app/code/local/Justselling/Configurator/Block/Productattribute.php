<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright � 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
**/
 
class Justselling_Configurator_Block_Productattribute extends Justselling_Configurator_Block_Default
{	
    public function getProductAttributeValue($_product) {
        $value = 0;

        if ($_product->getResource()->getAttribute($this->getOption()->getProductAttribute())) {
            $value = $_product->getResource()->getAttribute($this->getOption()->getProductAttribute())->getFrontend()->getValue($_product);
        }

        return $value;
    }
}