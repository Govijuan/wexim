<?php
class Justselling_Configurator_Block_List_Items extends Mage_Catalog_Block_Product_Abstract
{ 
    protected $_allLists = null;
    
    public function getAllLists($exclude=0)
    {
        if (is_null($this->_allLists)){
            $this->_allLists = Mage::getResourceModel('configurator/list_list_collection')
                ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
                ->addFieldToFilter('list_id', array('neq' => $exclude))
                ->load(); 
        }
        return $this->_allLists;
    }
    
    public function getList()
    {
        return Mage::registry('current_list');
    }

}