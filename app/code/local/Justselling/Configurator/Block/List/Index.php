<?php
class Justselling_Configurator_Block_List_Index extends Mage_Core_Block_Template 
{ 
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        $lists = Mage::getResourceModel('configurator/list_list_collection')
            ->addCustomerFilter(Mage::getSingleton('customer/session')->getCustomerId())
            ->load();
            
        $this->setLists($lists);
        
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('My Configurations'));
        }
    }
}