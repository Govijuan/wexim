<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Block_Loading extends Mage_Core_Block_Template
{
    const PACKAGE_NAME = "productconfigurator";
    const MODULE_NAME = "Justselling_Configurator";
    const PRODUCT_ID = "3";

    const API_USER = 'license';
    const API_KEY = 'ru474r784trefugfu';
    const MODE_PRODUCTION = 'production';
    const MODE_DEVELOPMENT = 'development';
    //const SECURE_URL_HASH_KEY = 'aHR0cDovL2xvY2FsaG9zdC9qdXN0c2VsbGluZy13ZWIvaW5kZXgucGhwL2FwaS9pbmNob28vanNvbg=='; // http://localhost/justselling-web/api/inchoo/json
    const SECURE_URL_HASH_KEY = 'aHR0cDovL3d3dy5wcm9kdWN0LWNvbmZpZ3VyYXRvci5iaXovYXBpL2luY2hvby9qc29u'; // Live
    //const SECURE_URL_HASH_KEY = 'aHR0cDovL2JsYXV3YWwuanVzdHNlbGxpbmcuZGUvanVzdHNlbGxpbmctc3RvcmUvYXBpL2luY2hvby9qc29u'; // Test blauwal

    private $activationDir = "";
    private $activationFile = "";
    private $activationString = "";
    private $defaultWebsite = NULL;
    private $defaultStoreGroup = NULL;
    private $defaultStore = NULL;

    protected function _toHtml()
    {
        $html = parent::_toHtml();

        try {
            $checkLicsense = $this->checkLicense();
        } catch (Exception $e) {
            $checkLicsense = false;
        }

        if (!$checkLicsense) {
            $nolicenseJs = $this->getNoLicenseJsHtml();
            $html = $nolicenseJs . $html;
        }
        return $html;
    }

    public function checkLicense()
    {
        $serverApiUri = $this->getMageSecureUrlHashKey();
        $key =  Mage::getStoreConfig(self::PACKAGE_NAME .'/general/licensekey');

        if (!$key) {
            return 0;
        }

        $domains = $this->getMageDomains();

        // Build activation string for all domains
        $activation_string = md5($key) ;
        foreach ($domains as $domain) {
            $ip = gethostbyname($domain);
            $activation_string .=  md5($domain).sha1($ip);
        }
        $this->activationString = $activation_string . sha1(substr($serverApiUri, 4, 11));

        // Check if activation file is already there
        $this->activationDir = $this->getActivationDir();
        $this->activationFile = $this->getActivationFile();
        if (file_exists($this->activationFile)) {
            $act_key = "";
            $af = fopen($this->activationFile, "r");
            if ($af) {
                $act_key = fgets($af);
                fclose($af);
            }

            if ($act_key == md5("P") .$this->activationString) {
                Mage::getSingleton('core/session')->setEdition("P");
                return 1;
            } elseif ($act_key == md5("U") .$this->activationString) {
                Mage::getSingleton('core/session')->setEdition("U");
                return 1;
            } else {
                if ($act_key == md5("B") .$this->activationString) {
                    return 1;
                } else {
                    // activation not yet done, do http request
                    $httpJsonCheck = $this->httpJsonCheck($key);
                    return $httpJsonCheck;
                }
            }
        }

        // activation not yet done, do http request
        $httpJsonCheck = $this->httpJsonCheck($key);
        return $httpJsonCheck;
    }


    private function getNoLicenseJsHtml()
    {
        $nolicenseText = "No valid license key for magento extension: <strong>" . self::MODULE_NAME . '</strong>.<br/>Get one on <a style="color:black" href="http://www.mage-extension-store.com/">www.mage-extension-store.com</a>.';
        $divStyle = '"text-align: center; background: red; position: absolute; top: 0; bottom: 0; width: 100%; height: 20px; padding: 20px;z-index:99999;"';
        $div = "var div = '<div style=" . $divStyle . ">" . $nolicenseText . "</div>';";
        $nolicenseJs = "<script type='text/javascript'>"
            . $div
            . "$$('body')[0].insert(div);
                       </script>";
        return $nolicenseJs;
    }

    private function getActivationDir()
    {
        return Mage::getBaseDir('var') . "/jslicense";
    }

    private function getActivationFile()
    {
        return $this->getActivationDir() . "/" . self::PACKAGE_NAME . ".txt";
    }

    private function getMagePackageName()
    {
        return self::PACKAGE_NAME;
    }

    private function getMagePackageVersion()
    {
        $moduleName = self::MODULE_NAME;
        return (string)Mage::getConfig()->getNode()->modules->$moduleName->version;
    }

    private function getMageSecureUrlHashKey()
    {
        return base64_decode(self::SECURE_URL_HASH_KEY);
    }

    private function getMageVersion()
    {
        $ver_info = Mage::getVersionInfo();
        $mag_version = $ver_info['major'].".".$ver_info['minor'].".".$ver_info['revision'].".".$ver_info['patch'];

        return $mag_version;
    }

    private function getMageLocale()
    {
        $localeComponents = explode('_', Mage::app()->getLocale()->getLocale());
        return strtolower($localeComponents[0]);
    }

    private function getMageEditionName()
    {
        if ($this->isMageProfessionalEdition())
            return 'professional';

        if ($this->isMageEnterpriseEdition())
            return 'enterprise';

        if ($this->isMageCommunityEdition())
            return 'community';

        if ($this->isMageGoEdition())
            return 'magento go';

        return 'undefined';
    }

    private function isMageGoEdition()
    {
        return class_exists('Saas_Db', false);
    }

    private function isMageProfessionalEdition()
    {
        if ($this->isMageGoEdition())
            return false;

        if (in_array('Professional_License', $this->getMageModules()))
            return true;

        return false;
    }

    private function isMageEnterpriseEdition()
    {
        if ($this->isMageGoEdition())
            return false;

        if (in_array('Enterprise_License', $this->getMageModules()))
            return true;

        return false;
    }

    private function isMageCommunityEdition()
    {
        if ($this->isMageGoEdition() || $this->isMageProfessionalEdition() || $this->isMageEnterpriseEdition())
            return false;

        return true;
    }

    private function getMageDbTables()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read')->listTables();
    }

    private function getMageDbTablesPrefix()
    {
        return ( string )Mage::getConfig()->getTablePrefix();
    }

    private function getMageDbVersion()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read')->getServerVersion();
    }

    private function getMageDbApiName()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read')->getConnection();
        return $connection instanceof PDO ? $connection->getAttribute(PDO::ATTR_CLIENT_VERSION) : 'N/A';
    }

    private function getMageDbSettings()
    {
        $sqlQuery = "SHOW VARIABLES WHERE `Variable_name` IN ('connect_timeout','wait_timeout')";

        $settingsArray = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($sqlQuery);

        $settings = array();
        foreach ($settingsArray as $settingItem) {
            $settings [$settingItem ['Variable_name']] = $settingItem ['Value'];
        }

        return $settings;
    }

    private function getMageDbName()
    {
        return ( string )Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
    }

    private function getMageModules()
    {
        return array_keys(( array )Mage::getConfig()->getNode('modules')->children());
    }

    private function getMageBaseCurrency()
    {
        return ( string )Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
    }

    private function getMageDefaultWebsite()
    {
        if (is_null($this->defaultWebsite)) {
            $this->defaultWebsite = Mage::getModel('core/website')->load(1, 'is_default');
            if (is_null($this->defaultWebsite->getId())) {
                $this->defaultWebsite = Mage::getModel('core/website')->load(0);
                if (is_null($this->defaultWebsite->getId())) {
                    throw new Exception ('Failed: Getting default website');
                }
            }
        }
        return $this->defaultWebsite;
    }

    private function getMageDomains()
    {
        $results = array();
        foreach (Mage::getModel('core/store')->getCollection() as $store) {
            $result =  Mage::app()->getStore($store->getId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
            $result = preg_replace("/http:\/\//", "", $result);
            $result = preg_replace("/https:\/\//", "", $result);
            $result = preg_replace("/\/.*$/", "", $result);
            $results [] = $result;
        }
        $results = array_unique($results);
        return $results;
    }

    private function getMageDomainsUnique()
    {
        return sizeof(array_unique($this->getMageDomains()));
    }

    private function getMageDefaultWebsiteId()
    {
        return ( int )$this->getMageDefaultWebsite()->getId();
    }

    private function getMageDefaultStoreGroup()
    {
        if (is_null($this->defaultStoreGroup)) {

            $defaultWebsite = $this->getMageDefaultWebsite();
            $defaultStoreGroupId = $defaultWebsite->getMageDefaultGroupId();

            $this->defaultStoreGroup = Mage::getModel('core/store_group')->load($defaultStoreGroupId);
            if (is_null($this->defaultStoreGroup->getId())) {
                $this->defaultStoreGroup = Mage::getModel('core/store_group')->load(0);
                if (is_null($this->defaultStoreGroup->getId())) {
                    throw new Exception ('Failed: Getting default store group');
                }
            }
        }
        return $this->defaultStoreGroup;
    }

    private function getMageDefaultStoreGroupId()
    {
        return ( int )$this->getMageDefaultStoreGroup()->getId();
    }

    private function getMageDefaultStore()
    {
        if (is_null($this->defaultStore)) {

            $defaultStoreGroup = $this->getMageDefaultStoreGroup();
            $defaultStoreId = $defaultStoreGroup->getMageDefaultStoreId();

            $this->defaultStore = Mage::getModel('core/store')->load($defaultStoreId);
            if (is_null($this->defaultStore->getId())) {
                $this->defaultStore = Mage::getModel('core/store')->load(0);
                if (is_null($this->defaultStore->getId())) {
                    throw new Exception ('Failed: Getting default store');
                }
            }
        }
        return $this->defaultStore;
    }

    private function getMageDefaultStoreId()
    {
        return ( int )$this->getMageDefaultStore()->getId();
    }

    private function isMageSingleStoreMode()
    {
        return Mage::getModel('core/store')->getCollection()->getSize() <= 2;
    }

    private function isMageMultiStoreMode()
    {
        return !$this->isMageSingleStoreMode();
    }

    private function getMageHost()
    {
        $domain = $this->getMageDomain();
        return $domain == '' ? $this->getIp() : $domain;
    }

    private function getMageDomain()
    {
        $serverDomain = isset ($_SERVER ['HTTP_HOST']) ? $_SERVER ['HTTP_HOST'] : NULL;

        if (!is_null($serverDomain)) {
            strpos($serverDomain, 'www.') === 0 && $serverDomain = substr($serverDomain, 4);
            return strtolower(trim($serverDomain));
        }

        return "undefined";
    }

    private function getMageIp()
    {
        $serverIp = isset ($_SERVER ['SERVER_ADDR']) ? $_SERVER ['SERVER_ADDR'] : NULL;
        is_null($serverIp) && $serverIp = isset ($_SERVER ['LOCAL_ADDR']) ? $_SERVER ['LOCAL_ADDR'] : NULL;

        if (!is_null($serverIp)) {
            return strtolower(trim($serverIp));
        }

        return "undefined";
    }

    private function isMageDeveloperMode()
    {
        return ( bool )Mage::getIsDeveloperMode();
    }

    private function getMageOs()
    {
        return php_uname();
    }

    private function getMagePhpVersion()
    {
        return @phpversion();
    }

    private function getMagePhpApiName()
    {
        return @php_sapi_name();
    }

    private function getMagePhpSettings()
    {
        return array(
            'memory_limit' => $this->getMageMemoryLimit(),
            'max_execution_time' => @ini_get('max_execution_time')
        );
    }

    private function getMageMemoryLimit($inMegabytes = true)
    {
        $memoryLimit = trim(ini_get('memory_limit'));

        if ($memoryLimit == '') {
            return 0;
        }

        $lastMemoryLimitLetter = strtolower(substr($memoryLimit, -1));
        switch ($lastMemoryLimitLetter) {
            case 'g' :
                $memoryLimit *= 1024;
            case 'm' :
                $memoryLimit *= 1024;
            case 'k' :
                $memoryLimit *= 1024;
        }

        if ($inMegabytes) {
            $memoryLimit /= 1024 * 1024;
        }

        return $memoryLimit;
    }

    private function getMageRequest($cmd = NULL)
    {
        $req = array(
            'mode' => $this->isMageDeveloperMode() ? self::MODE_DEVELOPMENT : self::MODE_PRODUCTION,
            'client' => array(
                'platform' => array(
                    'edition' => $this->getMageEditionName(),
                    'version' => $this->getMageVersion(),
                    'os' => $this->getMageOs(),
                    'multi' => $this->isMageMultiStoreMode(),
                    'domains' => $this->getMageDomains(),
                    'unique' => $this->getMageDomainsUnique()
                ),
                'module' => array(
                    'name' => $this->getMagePackageName(),
                    'version' => $this->getMagePackageVersion()
                ),
                'php' => array(
                    'version' => $this->getMagePhpVersion(),
                    'settings' => $this->getMagePhpSettings()
                ),
                'location' => array(
                    'domain' => $this->getMageDomain(),
                    'ip' => $this->getMageIp(),
                    'base' => str_replace('index.php/', '', Mage::getBaseUrl())
                ),
                'db' => array(
                    'name' => $this->getMageDbName(),
                    'version' => $this->getMageDbVersion(),
                    'prefix' => $this->getMageDbTablesPrefix(),
                    'settings' => $this->getMageDbSettings()
                ),
                'locale' => array(
                    'locale' => $this->getMageLocale(),
                    'currency' => $this->getMageBaseCurrency()
                )
            ),
            'auth' => array(),
            'command' => $cmd
        );
        return $req;
    }

    private function httpJsonCheck($l = NULL)
    {
        $sessionId = "";
        $params = array(
            'jsonrpc' => '2.0',
            'method' => 'login',
            'params' => array(
                self::API_USER,
                self::API_KEY
            ),
            'id' => time()
        );

        $http = new Zend_Http_Client ();
        $http->setUri($this->getMageSecureUrlHashKey());
        $http->setMethod(Zend_Http_Client::POST);
        $http->setRawData(json_encode($params));
        $result = json_decode($http->request()->getBody());
        if ($result)
            $sessionId = $result->result;

        if ($sessionId) {
            $params = array(
                'jsonrpc' => '2.0',
                'method' => 'call',
                'params' => array(
                    $sessionId,
                    'justselling.activatelicensev2',
                    array(
                        'licensekey' => sprintf("%004s", self::PRODUCT_ID) . '-' .$l,
                        'params' => $this->getMageRequest()
                    )
                ),
                'id' => time()
            );

            $http->setRawData(json_encode($params));
            $result = json_decode($http->request()->getBody());
            $status = $result->result;
            if ($status->status) {
                $edition = $status->edition;
                if ($edition == '2') {
                    $edition = 'P';
                    Mage::getSingleton('core/session')->setEdition("P");
                    Mage::getModel('core/config')->saveConfig('productconfigurator/general/edition', "Professionel ".Mage::helper("configurator")->getConfiguratorVersion());
                    Mage::app()->getCacheInstance()->cleanType("config")->clean();
                } elseif ($edition == '3') {
                    $edition = 'U';
                    Mage::getSingleton('core/session')->setEdition("U");
                    Mage::getModel('core/config')->saveConfig('productconfigurator/general/edition', "Ultimate ".Mage::helper("configurator")->getConfiguratorVersion() );
                    Mage::app()->getCacheInstance()->cleanType("config")->clean();
                } else {
                    $edition = 'B';
                    Mage::getSingleton('core/session')->setEdition("B");
                    Mage::getModel('core/config')->saveConfig('productconfigurator/general/edition', "Basic ".Mage::helper("configurator")->getConfiguratorVersion() );
                    Mage::app()->getCacheInstance()->cleanType("config")->clean();
                }
                $activation_string = md5($edition) . $this->activationString;


                if (!file_exists($this->activationDir)) {
                    mkdir($this->activationDir, 0777);
                }

                $af = fopen($this->activationFile, "w");
                if (!$af) {
                    Mage::getModel('core/config')->saveConfig('productconfigurator/general/edition', "Unable to write activation file, check permissions!");
                    Mage::app()->getCacheInstance()->cleanType("config")->clean();
                    Mage::throwException($this->__('Unable to write activation file, check permissions!'));
                    return 0;
                } else {
                    Mage::Log("activation: " . $activation_string);
                    fwrite($af, $activation_string);
                    fclose($af);
                    return "ok " . var_export($result, true);
                }
            } else {
                $status = $result->result;
                $message = $status->message;
                Mage::getModel('core/config')->saveConfig('productconfigurator/general/edition', "Activation failed: ".$message);
                Mage::app()->getCacheInstance()->cleanType("config")->clean();
                Mage::throwException($this->__('Product configurator activation problem 02: ' . var_export($result, true)));
                return 0;
            }
        }
    }
}