<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright © 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_CombinedimageController extends Mage_Core_Controller_Front_Action
{

    public function refreshAction()
    {
        $params = $this->getRequest()->getParams();

        $product = null;
        $productId = $params['product'];
        if ($productId) {
            $product = Mage::getModel("catalog/product")->load($productId);
            Mage::register('current_product', $product);
        }

        $js_template_option = null;
        if (isset($params['jstemplateoption'])) {
            $js_template_option = $params['jstemplateoption'];
        }

        $productOptionId = null;
        if (isset($params['productoptionid'])) {
            $productOptionId = $params['productoptionid'];
            $productOption = Mage::getModel('catalog/product_option')->load($productOptionId);
        }

        // Check Dynamic Value and store in Session
        if( isset($params['dynamics']) ) {
            foreach($params['dynamics'] as $templateId => $template) {
                Mage::getSingleton('core/session')->setDynamics($template);
            }
        }

        $templateOptions = array();
        if( isset($params['options']) ) {
            foreach($params['options'] as $optionId => $option) {
                $keys = array_keys($option);
                if (is_array($option) && ((string)$keys[0] == (string)$params['jstemplateoption'])) {
                    foreach($option as $configId => $config) {
                        if ($configId == $params['jstemplateoption']) {
                            if (is_array($option)) {
                                if( isset($config['template']) ) {
                                    $productOptionId = $optionId;
                                    foreach($config['template'] as $templateOptionId => $templateOptionValue) {
                                        $templateOptions[$templateOptionId] = $templateOptionValue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $templateId = Mage::getModel('configurator/template')->getLinkedTemplateId($productOptionId);
        $template = Mage::getModel('configurator/template')->load($templateId);

        $body = "";
        if ($template->getCombinedProductImage()) {
            $body = Mage::helper('configurator/Combinedimage')->getProductImageFromMatrix(
                $product,
                $template,
                $templateOptions,
                $js_template_option
            );
        }

        $this->getResponse()->setHeader('Content-type', 'application/html');
        $this->getResponse()->setBody($body);


    }
}


