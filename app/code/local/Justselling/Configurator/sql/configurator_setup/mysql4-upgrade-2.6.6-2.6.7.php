<?php
$this->startSetup();

$this->run("

		ALTER TABLE  `configurator_template` ADD listimage_mouseover int(11) NOT NULL DEFAULT 0;
		UPDATE `configurator_template` SET listimage_mouseover = 0;
");

$this->endSetup();