<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright � 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
**/

class Justselling_Configurator_Helper_Image extends Mage_Core_Helper_Abstract
{
	public function resize($filename,$type,$width,$height=null)
	{
		//Zend_Debug::dump($filename);
		$src = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."configurator/".$filename;			
		
		$width = (int) $width;
		if( $width ) {
			
			$path = Mage::getBaseDir('media').DS."configurator".DS;
			$file = $path.$filename;
			
			$pathParts = pathinfo($file);
			
			$newFilename = $pathParts['filename']."_".$width."_".$type.".".$pathParts['extension'];
			$newFile = $path.$newFilename;
			
			if( file_exists($newFile) ) return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."configurator/".$newFilename;
			
			$height = $height ? $height : null;

			try {
				$image = new Varien_Image($file);
				$image->keepTransparency(true);
				$image->keepAspectRatio(TRUE);
				$image->resize($width,$height);
				$image->save($newFile);
				$src = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."configurator/".$newFilename;
			} catch (Exception $e) {
			}

		}
		
		return $src;
	}
}