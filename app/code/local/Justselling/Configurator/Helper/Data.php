<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** @var array instance cache [bschulte] */
    private $_cache = array();

    const MAGE_CACHE_NAME = 'productconfigurator';

    protected function prepareForCache($data) {
        if ($data) {
            if ($data instanceof Varien_Data_Collection) {
                $data->load();
            }
            $serialized = serialize($data);
            return $serialized;
        }
        return "";
    }

    protected function prepareFromCache($data) {
        if ($data) {
            $unserialized = unserialize($data);
            return $unserialized;
        }
        return "";
    }

    public function writeToCache($value, $key, $tags, $serialize = true) {
        if (Mage::app()->useCache(self::MAGE_CACHE_NAME)) {
            $cache = Mage::app()->getCache();
            if ($serialize) {
                $value = $this->prepareForCache($value);
            }
            return $cache->save(
                $value,
                $key,
                $tags
            );
        }
        return false;
    }

    public function readFromCache($key, $serialize = true) {
        if (Mage::app()->useCache(self::MAGE_CACHE_NAME)) {
            $cache = Mage::app()->getCache();
            if (!$cache->load($key)) {
                return false;
            }
            $value = $cache->load($key);
            if ($serialize) {
                $value = $this->prepareFromCache($cache->load($key));
            }
            return $value;
        }
        return false;
    }

	public function getConfiguratorVersion()
	{
		return (string) Mage::getConfig()->getNode()->modules->Justselling_Configurator->version;
	}

	public function isMageEnterprise(){
		return Mage::getConfig()->getModuleConfig('Enterprise_Enterprise') && Mage::getConfig()->getModuleConfig('Enterprise_AdminGws') && Mage::getConfig()->getModuleConfig('Enterprise_Checkout') && Mage::getConfig()->getModuleConfig('Enterprise_Customer');
	}

	public function getMagentoVersion()
	{
		$ver_info = Mage::getVersionInfo();
		$mag_version	= "{$ver_info['major']}.{$ver_info['minor']}.{$ver_info['revision']}.{$ver_info['patch']}";

		return $mag_version;
	}

	public function getDeepLink($product, $values) {
		if ($product && $product->getproductUrl())
			return $product->getproductUrl()."?".$this->getDeepLinkParameters($values);

		return "";
	}

	public function getDeepLinkParameters($values) {
		$values = unserialize($values);
		$options = $values['options'];
		$deeplink = "";

        if ($options) {
            foreach ( $options as $optionId => $option ) {
                if (is_array ($option)) {
                    foreach ($option as $config) {
                        if (is_array ($option)) {
                            if (isset ($config ['template'])) {
                                foreach ($config ['template'] as $templateOptionId => $templateOptionValue) {
                                    if ($templateOptionId && $templateOptionValue) {
                                        if ($deeplink) {
                                            $deeplink .= "&";
                                        }
                                        $deeplink = $deeplink . "o_" . $optionId . "_" . $templateOptionId . "=" . $templateOptionValue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

		return $deeplink;
	}

	public function getListUrl() {
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."configurator/list";
	}


	public function getUploadImage($filename) {
		$parts = explode(".",$filename);
		$extension = $parts[sizeof($parts)-1];
		Mage::Log("extension=".$extension);
		if (in_array($extension, array("gif","jpeg","jpg","png","tiff","tif"))) {
			if ($extension == "jpg") $extension="jpeg";
			if ($extension == "tif") $extension="tiff";
			return "image/".$extension.".png";
		}
		if (in_array($extension, array("doc","docx","pdf","psd","zip","vnd"))) {
			if ($extension == "docx") $extension="msword";
			if ($extension == "doc") $extension="msword";
			if ($extension == "psd") $extension="x-photoshop";
			if ($extension == "vnd") $extension="msword";
			return Mage::getDesign()->getSkinUrl('images/justselling/application/' .$extension .'.png');
		}
		if (in_array($extension, array("xml"))) {
			return Mage::getDesign()->getSkinUrl('images/justselling/text/' .$extension .'.png');
		}
        return Mage::getDesign()->getSkinUrl('images/justselling/file.png');
	}

	public function getMimetypArrayByOptionId($optionId){
		$_option = Mage::getModel("configurator/option")->load($optionId);

		$_filetypes = $_option->getUploadFiletypes();
		if($_filetypes) {
			$_filetypes = str_replace(' ','',$_filetypes);
		}
		$_configurationFiletype = Mage::getStoreConfig('fileuploader/general/filetype');
		$_configurationFiletype = str_replace(' ','',$_configurationFiletype);
		if(!$_filetypes && $_configurationFiletype) {
			$_filetypes = $_configurationFiletype;
		} elseif (!$_filetypes && !$_configurationFiletype) {
			return '';
		}
		$_filetypesArray = explode(";", $_filetypes);
        $mimeTypeArray = array();
        if(is_array($_filetypesArray)) {
            foreach($_filetypesArray as $fn) {
                $mimeType = self::getMimeType(str_replace('*','',$fn));
                if($mimeType){
                    $mimeTypeTempArray = explode(";", $mimeType);
                    foreach($mimeTypeTempArray as $mt) {
                        $mimeTypeArray[] =  $mt;
                    }
                }
            }
        } else {
            $mimeType = self::getMimeType(str_replace('*','',$_filetypes));
            if($mimeType) {
                $mimeTypeTempArray = explode(";", $mimeType);
                foreach($mimeTypeTempArray as $mt) {
                    $mimeTypeArray[] =  $mt;
                }
            }
        }

        $mimeTypeJSON =  json_encode($mimeTypeArray);
		return $mimeTypeJSON;
	}


	function getMimeType($filename){
		switch ($filename)
		{
			case ".zip": $mime="application/zip"; break;
			case ".ez":  $mime="application/andrew-inset"; break;
			case ".hqx": $mime="application/mac-binhex40"; break;
			case ".cpt": $mime="application/mac-compactpro"; break;
			case ".doc": $mime="application/msword"; break;
			case ".bin": $mime="application/octet-stream"; break;
			case ".dms": $mime="application/octet-stream"; break;
			case ".lha": $mime="application/octet-stream"; break;
			case ".lzh": $mime="application/octet-stream"; break;
			case ".exe": $mime="application/octet-stream"; break;
			case ".class": $mime="application/octet-stream"; break;
			case ".so":  $mime="application/octet-stream"; break;
			case ".dll": $mime="application/octet-stream"; break;
			case ".oda": $mime="application/oda"; break;
			case ".pdf": $mime="application/pdf"; break;
			case ".ai":  $mime="application/postscript"; break;
			case ".eps": $mime="application/postscript"; break;
			case ".ps":  $mime="application/postscript"; break;
			case ".smi": $mime="application/smil"; break;
			case ".smil": $mime="application/smil"; break;
			case ".xls": $mime="application/vnd.ms-excel"; break;
			case ".ppt": $mime="application/vnd.ms-powerpoint"; break;
			case ".wbxml": $mime="application/vnd.wap.wbxml"; break;
			case ".wmlc": $mime="application/vnd.wap.wmlc"; break;
			case ".wmlsc": $mime="application/vnd.wap.wmlscriptc"; break;
			case ".bcpio": $mime="application/x-bcpio"; break;
			case ".vcd": $mime="application/x-cdlink"; break;
			case ".pgn": $mime="application/x-chess-pgn"; break;
			case ".cpio": $mime="application/x-cpio"; break;
			case ".csh": $mime="application/x-csh"; break;
			case ".dcr": $mime="application/x-director"; break;
			case ".dir": $mime="application/x-director"; break;
			case ".dxr": $mime="application/x-director"; break;
			case ".dvi": $mime="application/x-dvi"; break;
			case ".spl": $mime="application/x-futuresplash"; break;
			case ".gtar": $mime="application/x-gtar"; break;
			case ".hdf": $mime="application/x-hdf"; break;
			case ".js":  $mime="application/x-javascript"; break;
			case ".skp": $mime="application/x-koan"; break;
			case ".skd": $mime="application/x-koan"; break;
			case ".skt": $mime="application/x-koan"; break;
			case ".skm": $mime="application/x-koan"; break;
			case ".latex": $mime="application/x-latex"; break;
			case ".nc":  $mime="application/x-netcdf"; break;
			case ".cdf": $mime="application/x-netcdf"; break;
			case ".sh":  $mime="application/x-sh"; break;
			case ".shar": $mime="application/x-shar"; break;
			case ".swf": $mime="application/x-shockwave-flash"; break;
			case ".sit": $mime="application/x-stuffit"; break;
			case ".sv4cpio": $mime="application/x-sv4cpio"; break;
			case ".sv4crc": $mime="application/x-sv4crc"; break;
			case ".tar": $mime="application/x-tar"; break;
			case ".tcl": $mime="application/x-tcl"; break;
			case ".tex": $mime="application/x-tex"; break;
			case ".texinfo": $mime="application/x-texinfo"; break;
			case ".texi": $mime="application/x-texinfo"; break;
			case ".t":   $mime="application/x-troff"; break;
			case ".tr":  $mime="application/x-troff"; break;
			case ".roff": $mime="application/x-troff"; break;
			case ".man": $mime="application/x-troff-man"; break;
			case ".me":  $mime="application/x-troff-me"; break;
			case ".ms":  $mime="application/x-troff-ms"; break;
			case ".ustar": $mime="application/x-ustar"; break;
			case ".src": $mime="application/x-wais-source"; break;
			case ".xhtml": $mime="application/xhtml+xml"; break;
			case ".xht": $mime="application/xhtml+xml"; break;
			case ".zip": $mime="application/zip"; break;
			case ".au":  $mime="audio/basic"; break;
			case ".snd": $mime="audio/basic"; break;
			case ".mid": $mime="audio/midi"; break;
			case ".midi": $mime="audio/midi"; break;
			case ".kar": $mime="audio/midi"; break;
			case ".mpga": $mime="audio/mpeg"; break;
			case ".mp2": $mime="audio/mpeg"; break;
			case ".mp3": $mime="audio/mpeg"; break;
			case ".aif": $mime="audio/x-aiff"; break;
			case ".aiff": $mime="audio/x-aiff"; break;
			case ".aifc": $mime="audio/x-aiff"; break;
			case ".m3u": $mime="audio/x-mpegurl"; break;
			case ".ram": $mime="audio/x-pn-realaudio"; break;
			case ".rm":  $mime="audio/x-pn-realaudio"; break;
			case ".rpm": $mime="audio/x-pn-realaudio-plugin"; break;
			case ".ra":  $mime="audio/x-realaudio"; break;
			case ".wav": $mime="audio/x-wav"; break;
			case ".pdb": $mime="chemical/x-pdb"; break;
			case ".xyz": $mime="chemical/x-xyz"; break;
			case ".bmp": $mime="image/bmp"; break;
			case ".gif": $mime="image/gif"; break;
			case ".ief": $mime="image/ief"; break;
			case ".jpeg": $mime="image/jpeg"; break;
			case ".jpg": $mime="image/jpeg"; break;
			case ".jpe": $mime="image/jpeg"; break;
			case ".png": $mime="image/png"; break;
			case ".tiff": $mime="image/tiff"; break;
			case ".tif": $mime="image/tiff"; break;
			case ".djvu": $mime="image/vnd.djvu"; break;
			case ".djv": $mime="image/vnd.djvu"; break;
			case ".wbmp": $mime="image/vnd.wap.wbmp"; break;
			case ".ras": $mime="image/x-cmu-raster"; break;
			case ".pnm": $mime="image/x-portable-anymap"; break;
			case ".pbm": $mime="image/x-portable-bitmap"; break;
			case ".pgm": $mime="image/x-portable-graymap"; break;
			case ".ppm": $mime="image/x-portable-pixmap"; break;
			case ".rgb": $mime="image/x-rgb"; break;
			case ".xbm": $mime="image/x-xbitmap"; break;
			case ".xpm": $mime="image/x-xpixmap"; break;
			case ".xwd": $mime="image/x-xwindowdump"; break;
			case ".igs": $mime="model/iges"; break;
			case ".iges": $mime="model/iges"; break;
			case ".msh": $mime="model/mesh"; break;
			case ".mesh": $mime="model/mesh"; break;
			case ".silo": $mime="model/mesh"; break;
			case ".wrl": $mime="model/vrml"; break;
			case ".vrml": $mime="model/vrml"; break;
			case ".css": $mime="text/css"; break;
			case ".html": $mime="text/html"; break;
			case ".htm": $mime="text/html"; break;
			case ".asc": $mime="text/plain"; break;
			case ".txt": $mime="text/plain"; break;
			case ".rtx": $mime="text/richtext"; break;
			case ".rtf": $mime="text/rtf"; break;
			case ".sgml": $mime="text/sgml"; break;
			case ".sgm": $mime="text/sgml"; break;
			case ".tsv": $mime="text/tab-separated-values"; break;
			case ".wml": $mime="text/vnd.wap.wml"; break;
			case ".wmls": $mime="text/vnd.wap.wmlscript"; break;
			case ".etx": $mime="text/x-setext"; break;
			case ".xml": $mime="text/xml"; break;
			case ".xsl": $mime="text/xml"; break;
			case ".mpeg": $mime="video/mpeg"; break;
			case ".mpg": $mime="video/mpeg"; break;
			case ".mpe": $mime="video/mpeg"; break;
			case ".qt":  $mime="video/quicktime"; break;
			case ".mov": $mime="video/quicktime"; break;
			case ".mxu": $mime="video/vnd.mpegurl"; break;
			case ".avi": $mime="video/x-msvideo"; break;
			case ".movie": $mime="video/x-sgi-movie"; break;
			case ".asf": $mime="video/x-ms-asf"; break;
			case ".asx": $mime="video/x-ms-asf"; break;
			case ".wm":  $mime="video/x-ms-wm"; break;
			case ".wmv": $mime="video/x-ms-wmv"; break;
			case ".wvx": $mime="video/x-ms-wvx"; break;
			case ".ice": $mime="x-conference/x-cooltalk"; break;
			case ".csv": $mime="text/csv;text/comma-separated-values;application/csv"; break;
		}

		return $mime;
	}

	public function maskadeWysiwyg($html) {
	
		$html = str_replace("\"", "'", $html);
		return $html;
	}

    public function getPrice() {

    }

    /**
     * @param $option
     * @param $price
     * @param null $product_id
     * @return float|int
     */
    public function getDiscountPrice($option, $price, $product_id = NULL) {
		if ($option->getApplyDiscount()) {
			if (is_null($product_id) && Mage::registry('current_product')) {
                $product_id = Mage::registry('current_product')->getId();
			}
				
			if(Mage::getSingleton( 'customer/session' )->isLoggedIn()) {
				$group_id = Mage::getSingleton('customer/session')->getCustomerGroupId();
            } else {
                $group_id = null;
            }

			$website_id = Mage::app()->getStore()->getWebsiteId();
			$store_id = Mage::app()->getStore()->getStoreId();
			$store_timestamp = Mage::app()->getLocale()->storeTimeStamp($store_id);
			
			$rules = Mage::getResourceModel('catalogrule/rule')->getRulesFromProduct($store_timestamp, $website_id, $group_id, $product_id);
			foreach ($rules as $rule) {
				if (isset($rule['simple_action'])) { // Magento 1.4
					$operator = $rule['simple_action'];
					$amount = $rule['discount_amount'];
				}
				if (isset($rule['action_operator'])) { // Magento 1.5+
					$operator = $rule['action_operator'];
					$amount = $rule['action_amount'];
				}				
				
				if (isset($operator) && $operator == 'by_percent') {
					$price -= $price * $amount/100;;
				}
				if (isset($operator) && $operator == 'to_percent') {
					$price = $price * $amount/100;
				}
				if (isset($operator) && $operator == 'to_fixed') {
					$price = 0;
				}
			}
		}	

		return $price;
	}

    public function getOptionSku($params, $skuDelimiter)
    {
        $sku = '';
        foreach ($params as $js_template_id => $optionValue) {
            if( is_array($optionValue) ) {
                $optionValue = serialize(array($js_template_id => array('template' => $optionValue)));
                $templateOptions = $this->getTemplateOption($optionValue);
            } else {
                $templateOptions = $this->getTemplateOption($optionValue);
            }

            if( is_array($templateOptions) && count($templateOptions) > 0) {
                $skus = array();
                foreach($templateOptions as $templateOption) {
                    if( !empty($templateOption['value']['sku']) ) {
                        $skus[] = $templateOption['value']['sku'];
                    }
                }
                $sku = implode($skuDelimiter, $skus);
            }
        }
        return $sku;
    }

    public function getSelectedTemplateOptions($params) {
        $templateOptions = array();
        if( isset($params['options']) ) {
            foreach($params['options'] as $option) {
                if (is_array($option)) {
                    foreach($option as $configId => $config) {
                        if (is_array($option)) {
                            if( isset($config['template']) ) {
                                foreach($config['template'] as $templateOptionId => $templateOptionValue) {
                                    $templateOptions[$configId][$templateOptionId] = $templateOptionValue;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $templateOptions;
    }

    public function getTemplateOption($optionValue) {
        $arr =  unserialize($optionValue);
        if (!is_array($arr))
            return false;

        $options = array();
        foreach ($arr as $optionValues) {

            foreach($optionValues as $valueKey => $optionValue) {
                if ($valueKey == "postprice") {
                    $options[] = array(
                        'option' => array("title" => "postprice", "is_visible" => 0),
                        'value' => array(
                            'title'=> "__postprice",
                            'price'=> $optionValue,
                            'sku' => ""
                        )
                    );
                }
                if( $valueKey == "template" ) {
                    foreach ($optionValue as $optionId => $valueId ) {
                        $optionModel = Mage::getModel('configurator/option')->load($optionId);
                        if( !empty($valueId) ) {

                            /* Get option price */
                            switch ($optionModel->getType()) {
                                case "area":
                                case "text":
                                case "static":
                                case "combi":
                                case "matrixvalue":
                                case "expression":
                                case "http":
                                case "textimage":
                                case "checkbox":
                                case "date":
                                    $price =  $optionModel->getCalculatedPrice($valueId,$optionValue);
                                    break;
                                case "selectcombi":
                                case "listimagecombi":
                                    $price = $optionModel->getCalculatedPrice($valueId,$optionValue);
                                    break;
                                case "listimage":
                                case "select":
                                case "radiobuttons":
                                    $valueModel = Mage::getModel('configurator/value')->load($valueId);
                                    $price = $valueModel->getPrice();
                                    break;
                            }

                            if ($this instanceof Mage_Catalog_Model_Product_Option) {
                                $product_id = $this->getOption()->getProductId();
                                $price = Mage::helper('configurator')->getDiscountPrice($optionModel, $price, $product_id);
                            } else {
                                $price = Mage::helper('configurator')->getDiscountPrice($optionModel, $price);
                            }

                            /* build option array */
                            switch ($optionModel->getType()) {
                                case "area":
                                case "text":
                                case "static":
                                case "combi":
                                case "matrixvalue":
                                case "expression":
                                case "http":
                                case "textimage":
                                case "checkbox":
                                case "date":
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=>$valueId,
                                            'price'=> $price,
                                            'sku' => $optionModel->getSku()
                                        )
                                    );
                                    break;
                                case "selectcombi":
                                case "listimagecombi":
                                    $valueModel = Mage::getModel('configurator/value')->load($valueId);
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=> $valueModel->getTitle(),
                                            'price'=> $price,
                                            'sku'=> $valueModel->getSku()
                                        )
                                    );
                                    break;
                                case "listimage":
                                case "select":
                                case "radiobuttons":
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=> $valueModel->getTitle(),
                                            'price'=> $price,
                                            'sku' => $valueModel->getSku()
                                        )

                                    );
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return $options;
    }

    public function getOptionValueById($id){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $connection->select()
            ->from(
                array("co" => "configurator_option"),
                array("id","template_id","parent_id","title","type","sort_order","is_require","is_visible","apply_discount","max_characters","min_value","max_value",
                    "text_validate","sku","value", "placeholder","option_group_id",
                    "upload_type","upload_maxsize", "upload_filetypes",
                    "price","operator","alt_title","operator_value_price","decimal_place","product_id","expression","url","font","font_size","font_angle",
                    "font_color","font_pos_x","font_width_x","font_width_y","font_pos_y","option_image","default_value","option_group_id","product_attribute",
                    "matrix_dimension_x","matrix_operator_x","matrix_dimension_y","matrix_operator_y","matrix_csv_delimiter",
                    "listimage_hover","listimage_style","listimage_items_per_line","matrix_filename"
                )
            )
            ->joinLeft(
                array("cp" => "configurator_pricelist"),
                "co.id = cp.option_id",
                array(
                    "id as cp_id",
                    "option_id as cp_option_id",
                    "operator as cp_operator",
                    "value as cp_value",
                    "price as cp_price"
                )
            )
            ->joinLeft(
                array("cov" => "configurator_option_value"),
                "co.id = cov.option_id",
                array(
                    "id as cov_id",
                    "title as cov_title",
                    "value as cov_value",
                    "sku as cov_sku",
                    "option_id as cov_option_id",
                    "price as cov_price",
                    "sort_order as cov_sort_order"
                )
            )
            ->where('co.id = ?',$id)
            ->order(array("co.sort_order ASC","co.id ASC","cov.sort_order ASC","cp.value ASC","cov.value ASC"));

        $items = $connection->fetchAll($select);

		if(count($items) > 0){
			$templateId = $items[0]['template_id'];
			$groups = Mage::getModel("configurator/optiongroup")->getCollection();
			$groups->addFilter('template_id',$templateId);
		}else{
			$groups = array();
		}


        $lastKey = null;
        foreach ($items as $i => $item) {
            $values = array();
            $pricelist = array();

            foreach($item as $key => $value) {
                if( strpos($key,"cov") !== false ) {
                    $values[ substr($key,4) ] = $value;
                    unset($items[$i][$key]);
                }
                if( strpos($key,"cp") !== false ) {
                    $pricelist[ substr($key,3) ] = $value;
                    unset($items[$i][$key]);
                }
            }

            if( $lastKey !== null ) {
                if( $items[$i]['id'] == $items[$lastKey]['id'] ) {
                    foreach($items[$lastKey]['values'] as $value) {
                        if( !isset($items[$i]['values']) || (is_array($items[$i]['values']) && !in_array($value,$items[$i]['values'])))
                            $items[$i]['values'][] = $value;
                    }
                    foreach($items[$lastKey]['pricelist'] as $pricel) {
                        if(!isset($items[$i]['pricelist']) || (is_array($items[$i]['pricelist']) && !in_array($value,$items[$i]['pricelist'])))
                            $items[$i]['pricelist'][] = $pricel;
                    }
                    unset($items[$lastKey]);
                }
            }

			if( !isset($items[$i]['values']) || (is_array($items[$i]['values']) && !in_array($values,$items[$i]['values']))) {
				$items[$i]['values'][] = $values;
			}

			if( !isset($items[$i]['pricelist']) || (is_array($items[$i]['pricelist']) && !in_array($pricelist,$items[$i]['pricelist'])))
				$items[$i]['pricelist'][] = $pricelist;

            $lastKey = $i;
        }

        foreach($items as $key => $item) {
			$parentId = $item['parent_id'];
			if($parentId){
				$parentOption = Mage::getModel('configurator/option')->load($parentId);
				$items[$i]['parent_title'] = $parentOption->getTitle();
			}else{
				$items[$i]['parent_title'] = Mage::helper('configurator')->__('None');
			}
			$default_value = $item['default_value'];
			if($default_value){
				$parentOption = Mage::getModel('configurator/value')->load($default_value);
				$items[$i]['default_title'] = $parentOption->getTitle();
			}else{
				$items[$i]['default_title'] = Mage::helper('configurator')->__('no default');
			}
			$optionGroupId = $item['option_group_id'];
			$optionGroupTitle = "";
			if($optionGroupId){
				foreach ($groups as $group) {
					if($group->getId() == $optionGroupId){
						$optionGroupTitle =  $group->getTitle();
						break;
					}
				}
			}
			$items[$i]['option_group'] = $optionGroupTitle;

            if( $item['values'][0]['id'] == null ) {
                $items[$key]['values'] = array();
            } else {
            }
            if( $item['pricelist'][0]['id'] == null ) {
                $items[$key]['pricelist'] = array();
            } else {
            }
        }


		$items = array_values($items);
		if(!empty($items)){
			$result = $items[0];
			return $result;
		}
		return false;
    }


}