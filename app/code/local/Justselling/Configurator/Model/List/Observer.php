<?php
class Justselling_Configurator_Model_List_Observer extends Mage_Core_Model_Abstract
{
    public function processCartAddProductComplete($observer)
    {
        $request = $observer->getRequest();
        $messages = Mage::getSingleton('checkout/session')->getListPendingMessages();
        $urls = Mage::getSingleton('checkout/session')->getListPendingUrls();
        if ($request->getParam('list_next') && count($urls)) {
            $url = array_shift($urls);
            $message = array_shift($messages);

            Mage::getSingleton('checkout/session')->setListPendingUrls($urls);
            Mage::getSingleton('checkout/session')->setListPendingMessages($messages);

            Mage::getSingleton('checkout/session')->addNotice($message);
            
            $observer->getResponse()->setRedirect($url);
            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
        }
    }     
    
}