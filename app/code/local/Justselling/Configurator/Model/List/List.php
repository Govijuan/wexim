<?php
class Justselling_Configurator_Model_List_List extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('configurator/list_list');
    }

    public function getItems()
    {
        $items = $this->getData('items');
        if (is_null($items)) {
            $collection = Mage::getResourceModel('configurator/list_item_collection') 
                ->addFieldToFilter('list_id', $this->getId())
                ->setOrder('item_id')
                ->load();
        
            $products = $this->_getProductsArray($collection); 
       
            $items = array();
            foreach ($collection as $item) {
                if (isset($products[$item->getProductId()])){
                    $item->setProduct($products[$item->getProductId()]);
                    $items[] = $item;
                }
            } 
            $this->setData('items', $items);
        }
        return $items;  
    }
    
    protected function _getProductsArray($items)
    {
        $productIds = array();
        foreach ($items as $item) {
            $productIds[] = $item->getProductId();
        }
        $productIds = array_unique($productIds);
         
        $collection = Mage::getModel('catalog/product')->getResourceCollection()
             ->addIdFilter($productIds)
             ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes());
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $collection->load();
        
        $products = array(); 
        foreach ($collection as $prod) {
            $products[$prod->getId()] = $prod; 
        }
        
        return $products;
    }

    public function getLastListId($customerId)
    {
         return $this->_getResource()->getLastListId($customerId); 
    }
    
    public function addItem($productId, $customOptions)
    {
        $item = Mage::getModel('configurator/list_item')
            ->setProductId($productId)
            ->setListId($this->getId())
            ->setQty(1);
        
        if ($customOptions) {
             foreach ($customOptions as $product) {
                 $options = $product->getCustomOptions();
                 foreach ($options as $option) {
                    if ($option->getProductId() == $productId  && $option->getCode() == 'info_buyRequest'){
                        $v = unserialize($option->getValue());
                        
                        $qty = isset($v['qty']) ? max(0.01, $v['qty']) : 1;
                        $item->setQty($qty);
                        
                        // to be able to compare request in future
                        $unusedVars = array('list', 'qty', 'list_next', 'related_product');
                        foreach($unusedVars as $k){
                            if (isset($v[$k])){
                                unset($v[$k]);
                            }
                        }
                        $item->setBuyRequest(serialize($v));
                    }
                 }
            }
        } 
        // check if we already have the same item in the list.
        // if yes - set it's id to the current item
        $id = $item->findDuplicate();
        if ($id) {
            $item->setId($id);    
        }           
        else { 
            $item->save();
        }
        return $item;
    }
    
    public function saveDefault()
    {
        $this->_getResource()->clearDefault($this->getCustomerId());
        $this->setIsDefault(1);
        $this->save();
         
        return $this;
    }
    
    public function getListIdByTitle($customer_id, $title) {
    	Mage::Log("getListIdByTitle ".$customer_id." ".$title);
    	if (!$customer_id)
    		return false;
    	$lists = Mage::getModel("configurator/list_list")->getCollection();
    	$lists->addFieldToFilter("customer_id", $customer_id);
    	$lists->addFieldToFilter("title", $title);
    	if ($lists->getSize() == 0)
    		return false;
    	return $lists->getFirstItem()->getListId();
    }
}