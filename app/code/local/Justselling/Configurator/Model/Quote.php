<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright � 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Model_Quote extends Mage_Sales_Model_Quote
{
    /**
     * (non-PHPdoc)
     * @see Mage_Sales_Model_Quote::addProduct($product, $request)
     */
    public function addProduct(Mage_Catalog_Model_Product $product, $request = null)
    {
        $item = NULL;
        $conf_product = false;

        if( $request instanceof Varien_Object ) {

            $requestData = $request->getData();
            $qty = $request->getQty();
            if ($qty == 0) {
                $qty = 1;
            }
            $options = $product->getOptions();

            $altProducts = array();
            $altCheckout = false;
            $optionsLeft = 0;

            foreach($options as $option) {
                if( $option->getType() == 'configurator' ) {
                    $conf_product = true;

                    $optionId = $option->getOptionId();
                    $templateId = Mage::getModel('configurator/template')->getLinkedTemplateId($optionId);
                    $templateModel = Mage::getModel('configurator/template')->load($templateId);
                    $altCheckout = (bool) $templateModel->getAltCheckout();

                    if( isset($requestData['options'][$optionId]))	{
                        foreach($requestData['options'][$optionId] as $templateJsId => $template) {
                            $custom_option = new Justselling_Configurator_Model_Product_Option_Type_Custom();
                            $custom_options = $custom_option->getTemplateOption(serialize($requestData['options'][$optionId]));
                            $option_price = array();
                            foreach ($custom_options as $opt) {
                                if (isset($opt['option']['id'])) {
                                    $option_price[$opt['option']['id']] = $opt['value']['price'];
                                }
                            }
                            $templateOptionValues = $template['template'];

                            if ($altCheckout) {
                                foreach ($templateOptionValues as $templateOptionId => $templateOptionValue) {
                                    $templateOptionModel = Mage::getModel('configurator/option')->load($templateOptionId);
                                    $productId = $templateOptionModel->getProductId();
                                    $price = 0.0;
                                    if (isset($option_price[$templateOptionId])) {
                                        $price = $option_price[$templateOptionId];
                                    }

                                    if (in_array($templateOptionModel->getType(), array('select', 'radiobuttons', 'listimage', 'selectcombi', 'listimagecombi', 'checkbox', "overlayimage", 'overlayimagecombi', 'textimage'))) {
                                        $valueModel = Mage::getModel('configurator/value')->load($templateOptionValue);
                                        if ($valueModel->getProductId())
                                            $productId = $valueModel->getProductId();
                                    }

                                    if (isset($templateOptionModel) && $productId !== null) {
                                        unset($requestData["options"][$optionId][$templateJsId]["template"][$templateOptionId]);
                                        $alt_product = Mage::getModel('catalog/product')->load($productId);
                                        $type = $templateOptionModel->getType();
                                        $value = null;
                                        switch ($type) {
                                            case 'select':
                                            case 'listimage':
                                            case 'selectcombi':
                                            case 'selectcombi':
                                            case 'textimage':
                                            case 'listimagecombi':
                                            case 'radiobuttons':
                                            case 'checkbox':
                                            case 'overlayimage' :
                                            case 'overlayimagecombi' :
                                                $valueModel = Mage::getModel('configurator/value')->load($templateOptionValue);
                                                $value = 0.0 + $valueModel->getValue();
                                                break;
                                            default:
                                                $value = $templateOptionValue;
                                                $value= str_replace(",",".",$value);
                                                $value = 0.0 + $value;
                                                break;
                                        }

                                        if ($value !== null && $value != 0) {
                                            $item_qty = $value * $qty;
                                        } else {
                                            $item_qty =  $qty;
                                        }
                                        if ($value === 0 || $value == '0') {
                                            $item_qty = 0;
                                        }

                                        if ($item_qty > 0) {
                                            if (Mage::getStoreConfig('productconfigurator/stock/minordercheck')) {
                                                $stock_item = $alt_product->getStockItem();
                                                if ($stock_item) {
                                                    $min_sale_qty = $stock_item->getMinSaleQty();
                                                    if ($min_sale_qty > $item_qty) {
                                                        $item_qty = $min_sale_qty;
                                                    }
                                                }
                                            }
                                            if (Mage::getStoreConfig('productconfigurator/stock/maxordercheck')) {
                                                $stock_item = $alt_product->getStockItem();
                                                if ($stock_item) {
                                                    $max_sale_qty = $stock_item->getMaxSaleQty();
                                                    if ($item_qty > $max_sale_qty) {
                                                        $item_qty = $max_sale_qty;
                                                    }
                                                }
                                            }

                                            if (Mage::getStoreConfig('productconfigurator/stock/incrementsordercheck')) {
                                                $stock_item = $alt_product->getStockItem();
                                                if ($stock_item && $stock_item->getEnableQtyIncrements()) {
                                                    $increments = $stock_item->getQtyIncrements();
                                                    if ($increments) {
                                                        $in = floor($item_qty / $increments);
                                                        if ($item_qty - ($in * $increments) != 0) {
                                                            $item_qty = (round($item_qty / $increments,0)+1)*$increments;
                                                        }
                                                    }
                                                }
                                            }

                                            $altProducts[] = array('product' => $alt_product, 'qty' =>  $item_qty, 'price' => $price);
                                        }
                                    }
                                }
                            }
                            if (sizeof($requestData["options"][$optionId][$templateJsId]["template"]) > 0) {
                                $optionsLeft += sizeof($requestData["options"][$optionId][$templateJsId]["template"]);
                            }
                        }
                    }
                }
            }

            // Delete all options with value 0
            if ($altCheckout)
                foreach ($requestData["options"][$optionId][$templateJsId]["template"] as $option_id => $option_value_id) {
                    $option = Mage::getModel("configurator/option")->load($option_id);
                    $value = NULL;
                    switch ($option->getType()) {
                        case 'select':
                        case 'listimage':
                        case 'selectcombi':
                        case 'selectcombi':
                        case 'listimagecombi':
                        case 'radiobuttons':
                        case 'checkbox':
                        case 'overlayimage' :
                        case 'overlayimagecombi' :
                            $valueModel = Mage::getModel('configurator/value')->load($option_value_id);
                            $value = $valueModel->getValue();
                            break;
                        default:
                            $value = $option_value_id;
                            break;
                    }
                    if ($value == 0) {
                        unset($requestData["options"][$optionId][$templateJsId]["template"][$option_id]);
                        $optionsLeft--;
                    }
                }

            /* When there are alt products then add them to the cart */
            if( count($altProducts) > 0 ) {
                $item = null;
                foreach($altProducts as $altProduct) {
                    if ($altProduct['qty'] >= 1) {
                        $product = $altProduct['product'];
                        $item = parent::addProduct($altProduct['product'],$altProduct['qty']);
                        if (Mage::getStoreConfig('productconfigurator/stock/price')) {
                            $item->setCustomPrice($altProduct['price']);
                            $item->setOriginalCustomPrice($altProduct['price']);
                        }
                    }
                }
            }

            /* If there are any other configurator options then add a configuration product to the cart */
            if (count($altProducts)  == 0 || ($optionsLeft && Mage::getStoreConfig('productconfigurator/stock/configuration'))) {
                $request->setData($requestData);
                $item =  parent::addProduct($product,$request);
            }

            if ($conf_product) {
                return $item;
            }
        }

        return parent::addProduct ( $product, $request );
    }

}
