<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Model_Option extends Mage_Core_Model_Abstract
{
    /**
     *
     * Template
     * @var Justselling_Configurator_Model_Template
     */
    protected $_template;
    public $children = array();
    public $values = array();

    /** @var local cache */
    private $_cacheOptionChildren;
    private $_value_tag_array;

    protected $_optionPricelistCache;

    protected function _construct()
    {
        parent::_construct();
        $this->_init('configurator/option');
    }


	public function load($id, $field = null, &$check_loop = null){
		$option = parent::load($id, $field);
		if($option->getParentId()){
            if (!is_array($check_loop)) {
                $check_loop = array();
            }
            if (!in_array($id, $check_loop)) {
                $check_loop[] = $id;
                $optionParent = Mage::getModel('configurator/option')->load($option->getParentId(), $field, $check_loop);
                $option->setParentTitle($optionParent->getTitle());
            }
		}else{
			$option->setParentTitle(Mage::helper('configurator')->__('None'));
		}
		if($option->getDefaultValue()){
			$optionValue = Mage::getModel('configurator/value')->load($option->getDefaultValue());
			$option->setDefaultTitle($optionValue->getTitle());
		}else{
			$option->setDefaultTitle(Mage::helper('configurator')->__('no default'));
		}
		return $option;

	}

    /**
     *
     * set Template
     *
     * @param Justselling_Configurator_Model_Template $template
     * @return Justselling_Configurator_Model_Template
     */
    public function setTemplate(Justselling_Configurator_Model_Template $template)
    {
        $this->_template = $template;
        return $this;
    }

    /**
     *
     * get Template
     *
     * @return Justselling_Configurator_Model_Template
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     *
     * Get all values for this option
     *
     * @return Justselling_Configurator_Model_Mysql4_Value_Collection
     */
    public function getValueCollection()
    {
        $collection = Mage::getModel('configurator/value')->getCollection()
            ->addFieldToFilter('option_id', $this->getId())
            ->setOrder('sort_order', 'ASC');

        return $collection;
    }

    public function isCombi($option_id = NULL)
    {
        if ($option_id == NULL) {
            $option_id = $this->getId();
        }

        $collection = Mage::getModel('configurator/childoptionstatus')->getCollection();
        $collection->addFieldToFilter("child_option_id", $option_id);
        $collection->addFieldToFilter("is_combi", 1);
        $collection->load();
        $count = count($collection);
        if ($count > 0)
            return true;

        return false;

    }

    /**
     * Check if this option has children or not
     *
     * @return boolean
     */
    public function hasChildren()
    {
        return (count($this->children) == 0) ? false : true;
    }

    /**
     * Reads the pricelist data for the given value-id
     * If value-id is null a list of all the values from this option will be returned
     *
     * @param integer $valueId
     * @return Justselling_Configurator_Model_Mysql4_Pricelistvalue_Collection
     */
    public function getPricelistData($valueId)
    {
        $_plStart = microtime(true);
        $items = array();
        if (!is_null($valueId)) {
            $collection = Mage::getModel('configurator/pricelistvalue')->getCollection()
                ->addFieldToFilter('option_value_id', $valueId)
                ->setOrder('value', 'ASC');
            $items = $collection->getItems();
        } else {

            $ids = $this->getValueCollection()->getAllIds();
            $collection = Mage::getModel('configurator/pricelistvalue')->getCollection()
                ->addFieldToFilter('option_value_id', array('in' => $ids))
                ->setOrder('value', 'ASC');
            $items = $collection->getItems();
        }

        Js_Log::log('time getPricelistData:' . (microtime(true) - $_plStart), "profile", Zend_Log::DEBUG, true);
        return $items;
    }

    public function getPricelistDataAsArray($option_id)
    {
        $values = Mage::getModel('configurator/value')->getCollection();
        $values->addFieldToFilter('option_id', $option_id);
        $pricelist_array = array();

        foreach ($values as $value) {
            $pricelist = $this->getPricelistData($value->getId());
            $pricelist_array[$value->getId()] = array();
            foreach ($pricelist as $item) {
                $pricelist_array[$value->getId()][] = array("operator" => $item->getOperator(), "value" => $item->getValue(), "price" => Mage::app()->getStore()->convertPrice($item->getPrice()));
            }
        }
        return $pricelist_array;
    }

    public function getValuePricelistDataAsArray($option_id)
    {
        return $this->getPricelistDataAsArray($option_id);
    }

    public function getOptionPricelistData($option_id)
    {
        $_plStart = microtime(true);
        $items = array();
        if (!is_null($option_id)) {
            if (!isset($this->_optionPricelistCache[$option_id])) {
                $collection = Mage::getModel('configurator/pricelist')->getCollection()
                    ->addFieldToFilter('option_id', $option_id)
                    ->setOrder('value', 'ASC');
                $items = $collection->getItems();

                if (!is_array($this->_optionPricelistCache)) {
                    $this->_optionPricelistCache = array();
                }
                $this->_optionPricelistCache[$option_id] = $items;
            } else {
                $items = $this->_optionPricelistCache[$option_id];
            }
        } else {
            if (!isset($this->_optionPricelistCache['none'])) {
                $ids = $this->getValueCollection()->getAllIds();
                $collection = Mage::getModel('configurator/pricelistvalue')->getCollection()
                    ->addFieldToFilter('option_value_id', array('in' => $ids))
                    ->setOrder('value', 'ASC');
                $items = $collection->getItems();

                if (!is_array($this->_optionPricelistCache)) {
                    $this->_optionPricelistCache = array();
                }
                $this->_optionPricelistCache['none'] = $items;
            } else {
                $items = $this->_optionPricelistCache['none'];
            }
        }

        Js_Log::log('time getOptionPricelistData:' . (microtime(true) - $_plStart), "profile", Zend_Log::DEBUG, true);
        return $items;
    }

    protected function sortByValueAsc($a, $b) {
        return floatval($a['value']) - floatval($b['value']);
    }

    public function getOptionPricelistDataAsArray($option_id)
    {
        $_plStart = microtime(true);
        $pricelist_array = array();

        $pricelist = $this->getOptionPricelistData($option_id);
        foreach ($pricelist as $item) {
            $pricelist_array[] = array("operator" => $item->getOperator(), "value" => $item->getValue(), "price" => Mage::app()->getStore()->convertPrice($item->getPrice()));
        }
        usort($pricelist_array, array("Justselling_Configurator_Model_Option", "sortByValueAsc"));

        Js_Log::log('time getOptionPricelistDataAsArray:' . (microtime(true) - $_plStart), "profile", Zend_Log::DEBUG, true);
        return $pricelist_array;
    }

    public function hasPricelist($id = null)
    {
        if (count($this->getPricelistData($id)) > 0) return true;
        return false;
    }


    public function getBlacklistData($valueId)
    {
        $children = $this->getChildrenArray();

        $optionIds = array();
        foreach ($children as $child) $optionIds[] = $child['id'];

        $conn = $this->getResource()->getReadConnection();
        $select = $conn->select();
        $select->from(array('cov' => 'configurator_option_value'), array('id', 'title', 'value', 'option_id'));
        $select->where('cov.option_id IN (?)', $optionIds);
        $select->order('cov.option_id ASC');
        $childValues = $conn->fetchAll($select);

        $select2 = $conn->select();
        $select2->from(array('covb' => 'configurator_option_value_blacklist'), array('covb.*'));
        $blacklist = $conn->fetchAll($select2);

        $data = array();
        foreach ($children as $child) {
            $data[$child['id']] = array();
            $data[$child['id']]['selected'] = false;
            foreach ($blacklist as $bl) {
                if ($bl['child_option_id'] == $child['id'] && $bl['option_value_id'] == $valueId) {
                    $data[$child['id']]['selected'] = true;
                    $data[$child['id']]['selected_option_id'] = $bl['option_value_id'];
                }
            }
            $data[$child['id']]['option_id'] = $child['id'];
            foreach ($childValues as $childValue) {
                foreach ($blacklist as $bl) {
                    if ($valueId == $bl['option_value_id'] && $childValue['id'] == $bl['child_option_value_id']) {
                        $childValue['selected'] = true;
                    }
                }
                if ($child['id'] == $childValue['option_id']) {
                    $data[$child['id']]['values'][] = $childValue;
                }
            }
        }
        //Zend_debug::dump($data);
        return $data;
    }

    public function getOptionBlacklistdata($valueId)
    {
        $children = $this->getChildrenArray();

        $optionIds = array();
        foreach ($children as $child) {
            if (in_array($child["type"], array("text", "combi", "expression", "http")))
                $optionIds[] = $child['id'];
        }

        $options = Mage::getModel("configurator/option")->getCollection();
        $options->addFieldToFilter("id", array("in" => $optionIds));

        $data = array();
        foreach ($options as $option) {
            $blacklistValues = Mage::getModel("configurator/optionblacklist")->getCollection();
            $blacklistValues->addFieldToFilter("option_id", $option->getId());
            $blacklistValues->addFieldToFilter("child_option_value_id", $valueId);
            $blacklistValue = $blacklistValues->getFirstItem();

            $data[$option->getTitle()] = array();
            if ($blacklistValue) {
                $data[$option->getTitle()]["operator"] = $blacklistValue->getOperator();
                $data[$option->getTitle()]["value"] = $blacklistValue->getValue();
                $data[$option->getTitle()]["option_id"] = $option->getId();
                $data[$option->getTitle()]["blacklist_id"] = $blacklistValue->getId();
            }
        }

        // Mage::Log("getOptionBlacklist data=".var_export($data,true));
        return $data;
    }

    public function getChildrenStatus()
    {
        $childOptions = Mage::getModel("configurator/childoptionstatus")->getCollection()
            ->addFieldToFilter("option_id", $this->getId());

        $children = $this->getChildrenArray();
        foreach ($children as $key => $child) {
            foreach ($childOptions as $childOption) {
                if ($child['id'] == $childOption['child_option_id']) {
                    $children[$key]['status'] = $childOption['status'];
                    $children[$key]['is_combi'] = $childOption['is_combi'];
                }
            }
        }

        return $children;
    }

    public function getChildrenArray()
    {
        if (isset($this->_cacheOptionChildren)) {
            return $this->_cacheOptionChildren;
        }
        $_caStart = microtime(true);
        $collection = Mage::getModel("configurator/option")->getCollection();
        $collection->addFieldToFilter('template_id', $this->getTemplateId());
        $options = array();
        foreach ($collection as $item) {
            $options[] = array("id" => $item->getId(), "parent_id" => $item->getParentId(), "title" => $item->getTitle(),
                "type" => $item->getType(), "sort_order" => $item->getSortOrder());
        }

        $children = array();
        foreach ($options as $option) {
            if ($option['id'] == $this->getId()) {
                $children = $this->_getChildren($option, $options);
            }
        }
        usort($children, array($this, "compare"));

        $this->_cacheOptionChildren = $children;
        return $this->_cacheOptionChildren;
    }

    public function getParentArray($option)
    {
        $result = array();
        $i = 0;

        while ($option->getParentId()) {
            $result[] = $option->getParentId();
            $option = Mage::getModel("configurator/option")->load($option->getParentId());
            $i++;
            if ($i == 1000) // Prevent loops
                $error = "Found recursion in option ".$option->getTitle()." (".$option->getId().")";
                Js_Log::log($error, $this, Zend_Log::ERR, true);
                return $result;
        }

        return $result;
    }

    protected static function compare($option1, $option2)
    {
        $pos1 = (int)$option1['sort_order'];
        $pos2 = (int)$option2['sort_order'];
        if ($pos1 == $pos2) return 0;
        return ($pos1 < $pos2) ? -1 : 1;
    }

    protected function _checkIfNodeIsInArray($needle, $haystack) {
        foreach ($haystack as $element) {
            if ($element['id'] == $needle['id']) {
                $error = "Found recursion in configuration option: ".$needle['title']." (".$needle['id'].")";
                Js_Log::log($error, $this, Zend_Log::ERR, true);
                return true;
            }
        }
        return false;
    }

    private function _getChildren($node, $items)
    {
        $children = array();
        for ($i = 0; $i < count($items); $i++) {
            if ($node['id'] == $items[$i]['parent_id']) {
                if ($this->_checkIfNodeIsInArray($items[$i], $children)) {  // We have a recursion problem!

                } else {
                    $children[] = & $items[$i];
                    $children = array_merge($children, $this->_getChildren($items[$i], $items));
                }
            }
        }
        return $children;
    }

    protected function removeOptionLinks($option_id)
    {
        /* get all options that have this option as parent */
        $linked_options = Mage::getModel("configurator/option")->getCollection();
        $linked_options->addFieldToFilter("parent_id", $option_id);
        foreach ($linked_options as $linked_option) {
            $linked_option->setParentId(NULL);
            $linked_option->save();
        }
    }

    public function saveTemplateOptions(array $options)
    {
        foreach ($options as $option) {
            $this->saveTemplateOption($option);
        }
    }

    public function saveTemplateOption($option, $templateId = false)
    {
        $optionModel = Mage::getModel("configurator/option")->load($option['id']);

        if(!$templateId){
            if($this->getTemplate()){
                $templateId = $this->getTemplate()->getId();
            }else{
                $templateId = -1;
            }
        }

        if ($optionModel->template_id != $templateId) {
            $optionModel = Mage::getModel("configurator/option");
        }

        if ($option['is_delete'] == "1") {

            $optionModel->removeOptionLinks($optionModel->getId());
            $optionModel->delete();
			return 'delete';

        } else {
            $optionModel->setTemplateId($templateId);
            $optionModel->setTitle($option['title']);
            $optionModel->setType($option['type']);
            $optionModel->setData('alt_title', $option['alt_title']);
            $optionModel->setSortOrder($option['sort_order']);

            if (isset($option['is_require'])) {
                $optionModel->setIsRequire($option['is_require']);
            }
            if (isset($option['is_visible'])) {
                $optionModel->setIsVisible($option['is_visible']);
            }
            if (isset($option['placeholder'])) {
                $optionModel->setPlaceholder($option['placeholder']);
            }
            if (isset($option['upload_type'])) {
                $optionModel->setUploadType($option['upload_type']);
            }
            if (isset($option['apply_discount'])) {
                $optionModel->setApplyDiscount($option['apply_discount']);
            }
            if (isset($option['option_group'])) {
                if ($option['option_group']) {
                    $optionModel->setOptionGroupId($option['option_group']);
                } else {
                    $optionModel->setOptionGroupId(NULL);
                }
            }
            if (isset($option['sku'])){
                if ($option['sku']){
                    $optionModel->setSku($option['sku']);
                } else{
                    $optionModel->setSku(NULL);
                }
            }

            if (isset($option['value']))
                if ($option['value'])
                    $optionModel->setValue($option['value']);
                else
                    $optionModel->setValue(NULL);
            if (isset($option['product_attribute']))
                if ($option['product_attribute'])
                    $optionModel->setProductAttribute($option['product_attribute']);
                else
                    $optionModel->setProductAttribute(NULL);

            if (isset($option['max_characters']))
                if ($option['max_characters'])
                    $optionModel->setMaxCharacters((int)$option['max_characters']);
                else
                    $optionModel->setMaxCharacters(NULL);

            if (isset($option['min_value']))
                if ($option['min_value'])
                    $optionModel->setMinValue((int)$option['min_value']);
                else
                    $optionModel->setMinValue(NULL);

            if (isset($option['max_value']))
                if ($option['max_value'])
                    $optionModel->setMaxValue((int)$option['max_value']);
                else
                    $optionModel->setMaxValue(NULL);

            if (isset($option['upload_maxsize']))
                if ($option['upload_maxsize'])
                    $optionModel->setUploadMaxsize((int)$option['upload_maxsize']);
                else
                    $optionModel->setUploadMaxsize(NULL);

            if (isset($option['upload_filetypes']))
                if ($option['upload_filetypes'])
                    $optionModel->setUploadFiletypes($option['upload_filetypes']);
                else
                    $optionModel->setUploadFiletypes(NULL);

            if (isset($option['listimage_items_per_line']))
                if ($option['listimage_items_per_line'])
                    $optionModel->setListimageItemsPerLine($option['listimage_items_per_line']);
                else
                    $optionModel->setListimageItemsPerLine(NULL);

            if (isset($option['listimage_hover'])) {
                $optionModel->setListimageHover($option['listimage_hover']);
            }

            if (isset($option['listimage_style'])) {
                $optionModel->setListimageStyle($option['listimage_style']);
            }

            if (!empty($option['text_validate']))
                $optionModel->setTextValidate($option['text_validate']);

            if (isset($option['expression']))
                if ($option['expression'])
                    $optionModel->setExpression($option['expression']);
                else
                    $optionModel->setExpression(NULL);

            if (isset($option['url']))
                if ($option['url'])
                    $optionModel->setUrl($option['url']);
                else
                    $optionModel->setUrl(NULL);

            if (isset($option['font_size']))
                if ($option['font_size'])
                    $optionModel->setFontSize((int)$option['font_size']);
                else
                    $optionModel->setFontSize(NULL);

            if (empty($option['font_angle']))
                $optionModel->setFontAngle(0);
            else
                $optionModel->setFontAngle($option['font_angle']);

            if (isset($option['font_color']))
                if ($option['font_color'])
                    $optionModel->setFontColor($option['font_color']);
                else
                    $optionModel->setFontColor(NULL);

            if (!empty($option['font']))
                $optionModel->setFont($option['font']);

            if (isset($option['font_pos_x']))
                if ($option['font_pos_x'])
                    $optionModel->setFontPosX((int)$option['font_pos_x']);
                else
                    $optionModel->setFontPosX(NULL);

            if (isset($option['font_pos_y']))
                if ($option['font_pos_y'])
                    $optionModel->setFontPosY((int)$option['font_pos_y']);
                else
                    $optionModel->setFontPosY(NULL);

            if (isset($option['font_width_x']))
                if ($option['font_width_x'])
                    $optionModel->setFontWidthX((int)$option['font_width_x']);
                else
                    $optionModel->setFontWidthX(NULL);

            if (isset($option['font_width_y']))
                if ($option['font_width_y'])
                    $optionModel->setFontWidthY((int)$option['font_width_y']);
                else
                    $optionModel->setFontWidthY(NULL);

            if (isset($option['matrix_dimension_x']))
                if ($option['matrix_dimension_x'])
                    $optionModel->setMatrixDimensionX($option['matrix_dimension_x']);
                else
                    $optionModel->setMatrixDimensionX(NULL);

            if (isset($option['matrix_dimension_y']))
                if ($option['matrix_dimension_y'])
                    $optionModel->setMatrixDimensionY($option['matrix_dimension_y']);
                else
                    $optionModel->setMatrixDimensionY(NULL);

            if (!empty($option['matrix_operator_x']))
                $optionModel->setMatrixOperatorX($option['matrix_operator_x']);

            if (!empty($option['matrix_operator_y']))
                $optionModel->setMatrixOperatorY($option['matrix_operator_y']);

            if (isset($option['matrix_csv_delimiter']))
                if ($option['matrix_csv_delimiter'])
                    $optionModel->setMatrixCsvDelimiter($option['matrix_csv_delimiter']);
                else
                    $optionModel->setMatrixCsvDelimiter(";");

            if (isset($option['default_value'])) {
                switch ($option['type']) {
                    case "checkbox":
                        $optionModel->setDefaultValue($option['default_value']);
                        break;
                    default:
                        if ($option['default_value']) {
                            $optionModel->setDefaultValue($option['default_value']);
                        } else {
                            $optionModel->setDefaultValue(NULL);
                        }
                        break;
                }
            }

            if (empty($option['price']))
                $optionModel->setPrice(0);
            else
                $optionModel->setPrice($option['price']);

            if (empty($option['parent_id']))
                $optionModel->setParentId(null);
            else
                $optionModel->setParentId($option['parent_id']);

            if (!empty($option['operator']))
                $optionModel->setOperator($option['operator']);

            if (!empty($option['operator_value_price']))
                $optionModel->setOperatorValuePrice($option['operator_value_price']);

            if (isset($option['decimal_place']))
                if ($option['decimal_place'])
                    $optionModel->setDecimalPlace((int)$option['decimal_place']);
                else
                    $optionModel->setDecimalPlace(NULL);

            if (isset($option['product_id'])) {
                if ($option['product_id']) {
                    $optionModel->setProductId($option['product_id']);
                } else {
                    $optionModel->setProductId(NULL);
                }
            }

            if (isset($option['add_info']))
                if ($option['add_info'])
                    $optionModel->setInfo($option['add_info']);
                else
                    $optionModel->setInfo("");

            if (isset($option['more_info']))
                if ($option['more_info'])
                    $optionModel->setMoreInfo($option['more_info']);
                else
                    $optionModel->setMoreInfo(NULL);



			if (!empty($option['image'])) {
				try {
					$tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
					$tempFile = rtrim($tempFolder, '/') . '/' . $option['image'];

					if (file_exists($tempFile)) {
						$targetFolder = Mage::getBaseDir('media') ;
						if (!file_exists(str_replace('//', '/', $targetFolder))) {
							mkdir(str_replace('//', '/', $targetFolder), 0755, true);
						}
						$targetFileName = 'configurator' .DS .$option['image'];
						$targetFile = rtrim($targetFolder, '/') . '/' . $targetFileName;
						rename($tempFile, $targetFile);
						$optionModel->setOptionImage($targetFileName);
					}
				} catch (Exception $e) {
                    Js_Log::log("Could not access uploaded file: temp=".$tempFile." target=".$targetFile, $this, Zend_Log::NOTICE, true);
				}
			} else {
                if ($optionModel->getOptionImage()) {
                    $file = Mage::getBaseDir('media') . DS . 'configurator' . DS . $optionModel->getOptionImage();
                    if (file_exists($file)) {
                        if (!unlink($file)) {
                            Js_Log::log("Deleting file not possible: ".$file, $this, Zend_Log::NOTICE, true);
                        }
                    }
                    $optionModel->setOptionImage(null);
                }
			}

            $result = $optionModel->save();

            if (!empty($option['details'])) {
                $childoptionModel = Mage::getModel("configurator/childoptionstatus");

                $coCollection = $childoptionModel->getCollection();
                $coCollection->addFilter("option_id", $option['id']);
                $coItems = $coCollection->getItems();

                if (count($coItems) > 0) {
                    foreach ($coItems as $coItem) {
                        $coItem->delete();
                    }
                }

                foreach ($option['details'] as $coId => $detail) {

                    if (!empty($detail['status']) || !empty($detail['is_combi'])) {
                        $childoptionModel = Mage::getModel("configurator/childoptionstatus");

                        $childoptionModel->option_id = $option['id'];
                        $childoptionModel->child_option_id = $coId;

                        if (!empty($detail['status']))
                            $childoptionModel->setStatus($detail['status']);
                        if (!empty($detail['is_combi']))
                            $childoptionModel->is_combi = (int)$detail['is_combi'];

                        $childoptionModel->save();
                    }

                }
            }

            if (!empty($option['pricelist'])) {
                foreach ($option['pricelist'] as $pricelist) {
                    $pricelistModel = Mage::getModel("configurator/pricelist")->load($pricelist['id']);

                    if ($pricelist['is_delete'] == "1") {
                        $pricelistModel->delete();
                    } else {
                        $pricelistModel->setOptionId($optionModel->id);
                        $pricelistModel->setOperator($pricelist['operator']);
                        $pricelistModel->setValue($pricelist['value']);
                        $pricelistModel->setPrice($pricelist['price']);
                        $pricelistModel->save();
                    }
                }
            }

            if (!empty($option['values'])) {
                foreach ($option['values'] as $value) {
                    $this->saveTemplateOption($value, $optionModel->id, $option["template_id"]);

                }
            }
            return $result;
        }


    }

    public function saveTemplateOptionValue($value, $optionId, $templateId)
    {
        $valueModel = Mage::getModel("configurator/value")->load($value['id']);

        if ($value['is_delete'] == "1") {
			$default_value = $value['id'];
            $valueModel->delete();
			$option = Mage::getModel('configurator/option')->load($optionId);
			if($option->getDefaultValue()){
				if($option->getDefaultValue() == $default_value){
					$option->setDefaultValue(null);
					$option->save();
				}
			}

        } else {

            if (!empty($value['thumbnail'])) {
                try {
                    $tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
                    $tempFile = rtrim($tempFolder, '/') . '/' . $value['thumbnail'];

                    if (file_exists($tempFile)) {
                        $targetFolder = Mage::getBaseDir('media') . DS . 'configurator';
                        if (!file_exists(str_replace('//', '/', $targetFolder))) {
                            mkdir(str_replace('//', '/', $targetFolder), 0755, true);
                        }
                        $targetFileName = $value['id'] . '-' . $value['thumbnail'];
                        $targetFile = rtrim($targetFolder, '/') . '/' . $targetFileName;
                        rename($tempFile, $targetFile);
                        $valueModel->setThumbnail($targetFileName);
                    }
                } catch (Exception $e) {
                }
            } else {
                if (!empty($value->thumbnail)) {
                    $file = Mage::getBaseDir('media') . DS . 'configurator' . DS . $valueModel->thumbnail;
                    if (file_exists($file)) {
                        unlink($file);
                    }
                    $valueModel->thumbnail = null;
                }
            }

            // deleted thumbnail alt ... should be not longer in use

            if (!empty($value['image'])) {
                try {
                    $tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
                    $tempFile = rtrim($tempFolder, '/') . '/' . $value['image'];

                    if (file_exists($tempFile)) {
                        $targetFolder = Mage::getBaseDir('media') . DS . 'configurator';
                        if (!file_exists(str_replace('//', '/', $targetFolder))) {
                            mkdir(str_replace('//', '/', $targetFolder), 0755, true);
                        }
                        $targetFileName = $value['id'] . '-' . $value['image'];
                        $targetFile = rtrim($targetFolder, '/') . '/' . $targetFileName;
                        rename($tempFile, $targetFile);
                        $valueModel->setImage($targetFileName);
                    }
                } catch (Exception $e) {
                }
            } else {
                if (!empty($value->image)) {
                    $file = Mage::getBaseDir('media') . DS . 'configurator' . DS . $valueModel->image;
                    if (file_exists($file)) {
                            unlink($file);
                    }
                    $valueModel->image = null;
                    Mage::getModel("configurator/template")->clearProductCache($templateId);
                }
            }

            $valueModel->setOptionId($optionId);
            $valueModel->setTitle($value['title']);
            $valueModel->setValue($value['value']);
            $valueModel->setSortOrder($value['sort_order']);
            $valueModel->setPrice($value['price']);
            $valueModel->setSku($value['sku']);

            if (isset($value['product_id'])) {
                if ($value['product_id']) {
                    $valueModel->setProductId($value['product_id']);
                } else {
                    $valueModel->setProductId(NULL);
                }
            }

            if (!empty($value['thumbnail_size_x']))
                $valueModel->setThumbnailSizeX($value['thumbnail_size_x']);
            if (isset($value['thumbnail_size_x']) && $value['thumbnail_size_x'] == "") {
                $valueModel->setData("thumbnail_size_x", null);
            }

            if (!empty($value['thumbnail_size_y']))
                $valueModel->setThumbnailSizeY($value['thumbnail_size_y']);
            if (isset($value['thumbnail_size_y']) && $value['thumbnail_size_y'] == "") {
                $valueModel->setData("thumbnail_size_y", null);
            }

            if (!empty($value['image_size_x']))
                $valueModel->setImageSizeX($value['image_size_x']);
            if (isset($value['image_size_x']) && $value['image_size_x'] == "") {
                $valueModel->setData("image_size_x", null);
            }

            if (!empty($value['image_size_y']))
                $valueModel->setImageSizeY($value['image_size_y']);
            if (isset($value['image_size_y']) && $value['image_size_y'] == "") {
                $valueModel->setData("image_size_y", null);
            }

            if (!empty($value['image_offset_x']))
                $valueModel->setImageOffsetX($value['image_offset_x']);
            if (isset($value['image_offset_x']) && $value['image_offset_x'] == "") {
                $valueModel->setData("image_offset_x", null);
            }

            if (!empty($value['image_offset_y']))
                $valueModel->setImageOffsetY($value['image_offset_y']);
            if (isset($value['image_offset_y']) && $value['image_offset_y'] == "") {
                $valueModel->setData("image_offset_y", null);
            }

            if (!empty($value['info']))
                $valueModel->setInfo($value['info']);
            if (isset($value['info']) && $value['info'] == "") {
                $valueModel->setData("info", null);
            }

            if (!empty($value['more_info']))
                $valueModel->setMoreInfo($value['more_info']);
            if (isset($value['more_info']) && $value['more_info'] == "") {
                $valueModel->setData("more_info", null);
            }

            $valueModel->save();

            if (!empty($value['status'])) {

                foreach ($value['status'] as $i => $vs) {
                    $valuestatusModel = Mage::getModel("configurator/valuechildstatus")->load($vs['id']);
                    $valuestatusModel->option_value_id = $valueModel->getId();
                    $valuestatusModel->option_id = $vs['option_id'];

                    if (isset($vs['is_require']))
                        $valuestatusModel->is_require = (int)$vs['is_require'];

                    if (isset($vs['price']))
                        $valuestatusModel->price = $vs['price'];

                    if (isset($vs['status']))
                        $valuestatusModel->status = $vs['status'];

                    if (isset($vs['min_value']) && !empty($childOptionValue['min_value']))
                        $valuestatusModel->min_value = $vs['min_value'];

                    if (isset($childOptionValue['max_value']) && !empty($childOptionValue['max_value']))
                        $valuestatusModel->max_value = $vs['max_value'];

                    //Zend_Debug::dump($vs); Zend_Debug::dump($valuestatusModel->getData()); exit;

                    $valuestatusModel->save();
                }

            }

            /* Value Tags */
            if (isset($value ['tags']) && !empty ($value ['tags'])) {
                $tag_array = explode(" ", trim($value ['tags']));

                /* Delete old Tags first */
                $collection = Mage::getModel("configurator/valuetag")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                $collection->addFieldToFilter('tag', array('nin' => $tag_array));
                foreach ($collection as $tag) {
                    $tag->delete();
                }

                /* Insert new or edited tags */
                foreach ($tag_array as $tag_title) {
                    $tag = Mage::getModel("configurator/valuetag");
                    $tag->loadByOptionValueIdAndTag($valueModel->getId(), $tag_title);
                    if (!$tag->getId()) {
                        $tag->setOptionValueId($valueModel->getId());
                        $tag->setTag($tag_title);
                        $tag->save();
                    }
                }
            } elseif (isset($value ['details'])) { // Delete all existing tags if details are open
                $collection = Mage::getModel("configurator/valuetag")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                foreach ($collection as $tags) {
                    $tags->delete();
                }
            }

            /* Blacklist */
            if (isset($value ['blacklist']) && !empty ($value ['blacklist'])) {
                /* Delete old Configuration first... */
                /* values */
                $collection = Mage::getModel("configurator/blacklist")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                $collection->addFieldToFilter("child_option_id", array('null' => true));
                $collection->addFieldToFilter('child_option_value_id', array('nin' => $value ['blacklist']));
                foreach ($collection as $blacklist) {
                    $blacklist->delete();
                }

                /* Insert blacklisted values */
                foreach ($value ['blacklist'] as $bl) {
                    $blacklist = Mage::getModel("configurator/blacklist");
                    $blacklist->loadByOptionValueIdAndChildOptionValueId($valueModel->getId(), $bl);
                    if (!$blacklist->getId()) {
                        $blacklist->option_value_id = $valueModel->getId();
                        $blacklist->child_option_value_id = $bl;
                        $blacklist->save();
                    }
                }
            } elseif (isset($value ['details'])) { // Delete all existing blacklist values if details are open
                $collection = Mage::getModel("configurator/blacklist")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                $collection->addFieldToFilter("child_option_id", array('null' => true));
                foreach ($collection as $blacklist) {
                    $blacklist->delete();
                }
            }

            /* Insert blacklisted options */
            if (isset($value['blacklistoption']) && !empty($value['blacklistoption'])) {
                /* delete old options */
                $collection = Mage::getModel("configurator/blacklist")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                $collection->addFieldToFilter("child_option_value_id", array('null' => true));
                $collection->addFieldToFilter('child_option_id', array('nin' => array_keys($value['blacklistoption'])));
                foreach ($collection as $blacklist) {
                    $blacklist->delete();
                }

                foreach ($value ['blacklistoption'] as $child_option_id => $val) {
                    $blacklist = Mage::getModel("configurator/blacklist");
                    $blacklist->loadByOptionValueIdAndChildOptionId($valueModel->getId(), $child_option_id);
                    if (!$blacklist->getId()) {
                        $blacklist->setOptionValueId($valueModel->getId());
                        $blacklist->setChildOptionId($child_option_id);
                        $blacklist->save();
                    }
                }
            } elseif (isset($value ['details'])) { // Delete all existing blacklist values if details are open
                $collection = Mage::getModel("configurator/blacklist")->getCollection();
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                $collection->addFieldToFilter("child_option_value_id", array('null' => true));
                foreach ($collection as $blacklist) {
                    $blacklist->delete();
                }
            }

            if (isset($value['optionblacklist']) && !empty($value['optionblacklist'])) {
                Mage::Log("SAVING " . var_export($value['optionblacklist'], true));
                Mage::Log("child value id " . $valueModel->getId());

                $num = 0;
                foreach ($value['optionblacklist']["optionid"] as $opt) {
                    if ($value['optionblacklist']["operator"][$num] && $value['optionblacklist']["value"][$num] !== null) {
                        Mage::Log("setting blacklist value");
                        $blacklist = Mage::getModel("configurator/optionblacklist");
                        if (isset($value['optionblacklist']["blacklistid"][$num])) // Update, load before save
                        $blacklist->load($value['optionblacklist']["blacklistid"][$num]);
                        $blacklist->setOptionId($value['optionblacklist']["optionid"][$num]);
                        $blacklist->setOperator($value['optionblacklist']["operator"][$num]);
                        $blacklist->setValue($value['optionblacklist']["value"][$num]);
                        $blacklist->setChildOptionValueId($valueModel->getId());
                        $blacklist->save();
                    }

                    if (!$value['optionblacklist']["operator"][$num] && isset($value['optionblacklist']["blacklistid"][$num])) {
                        // Delete existing record
                        $blacklist = Mage::getModel("configurator/optionblacklist")->load($value['optionblacklist']["blacklistid"][$num]);
                        $blacklist->delete();
                    }
                    $num++;
                }
            }

            if (isset($value['optionvaluetagblacklist']) && !empty($value['optionvaluetagblacklist'])) {
                Mage::Log("SAVING " . var_export($value['optionvaluetagblacklist'], true));
                Mage::Log("option id " . $optionId);
                Mage::Log("related option id " . $value['optionvaluetagblacklist']["related_option_id"]);
                Mage::Log("child value id " . $valueModel->getId());

                $collection = Mage::getModel("configurator/valuetagblacklist")->getCollection();
                $collection->addFieldToFilter("option_id", $optionId);
                $collection->addFieldToFilter("related_option_id", $value['optionvaluetagblacklist']["related_option_id"]);
                $collection->addFieldToFilter("option_value_id", $valueModel->getId());
                foreach ($collection as $item)
                    $item->delete();

                foreach ($value['optionvaluetagblacklist']["tag"] as $opt) {
                    Mage::Log("setting optionvaluetagblacklist value " . var_export($opt, true));
                    $blacklist = Mage::getModel("configurator/valuetagblacklist");
                    $blacklist->setOptionId($optionId);
                    $blacklist->setOptionValueId($valueModel->getId());
                    $blacklist->setRelatedOptionId($value['optionvaluetagblacklist']["related_option_id"]);
                    $blacklist->setTag($opt);
                    $blacklist->save();
                }
            }

            if (isset($value['pricelist']) && !empty($value['pricelist'])) {
                foreach ($value['pricelist'] as $i => $pl) {

                    $pricelistvalueModel = Mage::getModel("configurator/pricelistvalue")->load($pl['id']);

                    if ($pl['is_delete'] == "1") {
                        $pricelistvalueModel->delete();
                    } else {
                        $pricelistvalueModel->option_value_id = $valueModel->getId();
                        $pricelistvalueModel->price = $pl['price'];
                        $pricelistvalueModel->operator = $pl['operator'];
                        $pricelistvalueModel->value = $pl['value'];

                        $pricelistvalueModel->save();
                    }
                }
            }
        }
        return $valueModel;
    }

    public function saveMatrixCsv($filename, $optionId, $delimiter = ",")
    {
		$message = array();
		if($optionId == 0){
			$message[0] = 'error';
			$message[1] = "Für den Csv-Import muss die Option zuerst gespeichert werden.";
			return $message;
		}

		$tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
		$tempFile = rtrim($tempFolder, '/') . '/' . $filename;

		if (file_exists($tempFile)) {
			Mage::Log("ok, matrixvalue file is set");
			try {
				// find and move file
				$targetFolder = Mage::getBaseDir('media') . DS . 'configurator' . DS . 'matrixvalue';
				if (!file_exists(str_replace('//', '/', $targetFolder))) {
					mkdir(str_replace('//', '/', $targetFolder), 0755, true);
				}
				$targetFile = rtrim($targetFolder, '/') . '/' . $filename;
				rename($tempFile, $targetFile);


				$matrix = array();

				// Do something with the file
				if ($matrixfile = fopen($targetFile, "r")) {
					$x = fgetcsv($matrixfile, 0, $delimiter);
					unset($x[0]);
					$row = 0;
					while ($line = fgetcsv($matrixfile, 0, $delimiter)) {
						$row++;
						$y = $line[0];
						unset($line[0]);

						$i = 0;
						foreach ($line as $value) {
							if (!isset($matrix[$x[$i + 1]])) $matrix[$x[$i + 1]] = array();

							/* Check if we have a number with "," instead of "." */
							if (preg_match("/[0-9,]+/", $value)) {
								$value = str_replace(",", ".", $value);
							}

							$matrix[$x[$i + 1]][$y] = $value;
							$i++;
						}
					}

				    $column = count($x);

					if($column > 0 && $row > 0){
						$matrixmodel = Mage::getModel("configurator/optionmatrix")->loadByOptionId($optionId);
						$matrixmodel->setOptionId($optionId);
						$matrixmodel->setMatrix(json_encode($matrix));
						$matrixmodel->save();

						$valueModel = Mage::getModel("configurator/option")->load($optionId);
						$valueModel->setMatrixFilename($filename);
						$valueModel->setMatrixCsvDelimiter($delimiter);
						$valueModel->save();


						$message[0] = 'success';
						$message[1] = "Ihre Daten wurden erfolgreich importiert. Es wurden dabei " . $row ." Zeilen und " .$column ." Spalten importiert.";
						return $message;
					}else{
						$message[0] = 'error';
						$message[1] = "Fehler: Es konnten keine Daten erfolgreich importiert werden. Bitte überprüfen Sie Ihre Datei oder den Delimiter.";
						return $message;
					}
					Mage::Log("model=" . var_export($matrixmodel->getData(), true));
				}
			} catch (Exception $e) {
				Mage::Log("exception=" . var_export($e, true));
				$message[0] = 'error';
				$message[1] = "Fehler: Es konnten keine Daten erfolgreich importiert werden. Bitte überprüfen Sie Ihre Datei.";
				return $message;
			}
		}

	}


    public function saveOptionValueCsv($filename, $optionType, $optionId)
    {
		$message = array();
		if($optionId == 0){
			$message[0] = 'error';
			$message[1] = "Für den Csv-Import muss die Option zuerst gespeichert werden.";
			$message[2] = array();
			return $message;
		}

        $tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
        $tempFile = rtrim($tempFolder, '/') . '/' . $filename;

        if (file_exists($tempFile)) {
            $optionValues = array();
            Mage::Log("ok, selectcsv file is set");
            try {
                // find and move file
                $targetFolder = Mage::getBaseDir('media') . DS . 'configurator' . DS . 'csvoption';
                if (!file_exists(str_replace('//', '/', $targetFolder))) {
                    mkdir(str_replace('//', '/', $targetFolder), 0755, true);
                }
                $targetFile = rtrim($targetFolder, '/') . '/' . $filename;
                rename($tempFile, $targetFile);


                $delimiter = ",";
                // Do something with the file
                if ($selectcsvfile = fopen($targetFile, "r")) {
                    while ($line = fgetcsv($selectcsvfile, 0, $delimiter)) {
                        $optionvalue = Mage::getModel("configurator/value");
                        $optionvalue->setOptionId($optionId);
                        $optionvalue->setTitle($line[0]);
                        $optionvalue->setValue($line[1]);
                        $optionvalue->setPrice($line[2]);
                        $optionvalue->setSku($line[3]);
                        $optionvalue->setSortOrder($line[4]);
                        $optionvalue->setInfo($line[5]);
                        $i = 6;

                        /* Get additional Image Information  */
                        if (in_array($optionType, array('selectimage', 'overlayimage', 'overlayimagecombi','listimage', 'listimagecombi'))) {
                            $optionvalue->setThumbnailSizeX($line[6]);
                            $optionvalue->setThumbnailSizeY($line[7]);
                            $thumb_url = $line[8];
                            $optionvalue->setThumbnail(basename($thumb_url));
                            $optionvalue->setImageSizeX($line[9]);
                            $optionvalue->setImageSizeY($line[10]);
                            $image_url = $line[11];
                            $optionvalue->setImage(basename($image_url));

                            /* Load Images from URL */
                            if ($thumb_url && $optionvalue->getThumbnail())
                                $this->createImageFromUrl($thumb_url, $optionvalue->getThumbnail(), $optionvalue->getThumbnailSizeX(), $optionvalue->getThumbnailSizeY());
                            if ($image_url && $optionvalue->getImage())
                                $this->createImageFromUrl($image_url, $optionvalue->getImage(), $optionvalue->getImageSizeX(), $optionvalue->getImageSizeY());

                            $i = 12;
                        }

                        if (in_array($optionType, array('select', 'radiobuttons'))) {
                            $optionvalue->setImageSizeX($line[6]);
                            $optionvalue->setImageSizeY($line[7]);
                            $image_url = $line[8];
                            $optionvalue->setImage(basename($image_url));

                            /* Load Images from URL */
                            if ($image_url && $optionvalue->getImage())
                                $this->createImageFromUrl($image_url, $optionvalue->getImage(), $optionvalue->getImageSizeX(), $optionvalue->getImageSizeY());

                            $i = 9;
                        }

                        $optionvalue = $optionvalue->save();

                        $optionArray = array();
                        $optionArray['id'] = $optionvalue->getId();
                        $optionArray['title'] = $optionvalue->getTitle();
                        $optionArray['value'] = $optionvalue->getValue();
                        $optionArray['price'] = $optionvalue->getPrice();
                        $optionArray['sku'] = $optionvalue->getSku();
                        $optionArray['sort_order'] = $optionvalue->getSortOrder();

                        $optionValues[] = $optionArray;

                        // Get option value tags
                        while (isset($line[$i])) {
                            if ($line[$i]) {
                                $valuetag = Mage::getModel("configurator/valuetag");
                                $valuetag->setOptionValueId($optionvalue->getId());
                                $valuetag->setTag($line[$i]);
                                $valuetag->save();
                                Mage::Log("Add valuetag " . $line[$i]);
                            }
                            $i++;
                        }
                    }
                    fclose($selectcsvfile);
                }
            } catch (Exception $e) {
                Mage::Log("exception=" . var_export($e, true));
				$message[0] = 'error';
				$message[1] = "Fehler: Es konnten keine Daten erfolgreich importiert werden. Bitte überprüfen Sie Ihre Datei.";
				$message[2] = array();
            }
			$message[0] = 'success';
			$message[1] = "Ihre Daten wurden erfolgreich importiert.";
			$message[2] = $optionValues;
            return $message;
        }
    }

    /* Function to handle the image import from CSV */
    protected function createImageFromUrl($url, $filename, $sizex, $sizey)
    {
        $path = Mage::getBaseDir('media') . DS . 'configurator' . DS;
        $filename = $path . $filename;

        $func = $this->_getImageReadFunc($this->_getFileExtension($url));
        if (!$func) return false;
        $image = $func($url);

        if ($sizex and $sizey)
            $image = $this->_resizeImage($image, $sizex, $sizey);

        $func = $this->_getImageWriteFunc($this->_getFileExtension($url));
        if (!$func) return false;
        $func($image, $filename);

        Mage::Log("Create image: " . $filename);
    }

    protected function _resizeImage($origimage, $x, $y)
    {
        $image = imagecreatetruecolor($x, $y);
        $image_w = imagesx($origimage);
        $image_h = imagesy($origimage);

        if ($image_w == $x) {
            $left = 0;
        } else {
            $left = ($image_w - $x) / 2;
        }
        if ($image_h == $y) {
            $top = 0;
        } else {
            $top = ($image_h - $y) / 2;
        }

        if (($image_w == $x) && ($image_h == $y)) {
            return $origimage;
        }

        imagecopy($image, $origimage, 0, 0, $left, $top, $x, $y);
        return $image;
    }

    protected function _getImageReadFunc($filetype)
    {
        switch ($filetype) {
            case "jpg":
            case "jpeg":
                return "imagecreatefromjpeg";
                break;
            case "png":
                return "imagecreatefrompng";
                break;
            case "gif":
                return "imagecreatefromgif";
                break;
        }

        return false;
    }

    protected function _getImageWriteFunc($filetype)
    {
        switch ($filetype) {
            case "jpg":
            case "jpeg":
                return "imagejpeg";
                break;
            case "png":
                return "imagepng";
                break;
            case "gif":
                return "imagegif";
                break;
        }

        return false;
    }

    protected function _getFileExtension($file_name)
    {
        return substr(strrchr($file_name, '.'), 1);
    }

    public function getTemplateOptions($templateId)
    {
        $collection = $this->getCollection();
        $collection->addFilter('template_id', $templateId);
        return $collection;
    }

    public function toOptionArray($templateId = null)
    {
        $collection = Mage::getModel("configurator/option")->getCollection()->addFieldToFilter("template_id", $templateId);

        $result = array(null => "-- " . Mage::helper('configurator')->__("None") . " --");

        foreach ($collection->getItems() as $item) {
            $result[$item->id] = $item->title;
        }

        return $result;
    }

    public function toOptionArrayWithId($templateId = null)
    {
        $collection = Mage::getModel("configurator/option")->getCollection()->addFieldToFilter("template_id", $templateId);

        $result = array(null => "-- " . Mage::helper('configurator')->__("None") . " --");

        foreach ($collection->getItems() as $item) {
            $result[$item->id] = $item->title .' (' .$item->id .')';
        }

        return $result;
    }

    public function getPrice()
    {
        return $this->getData('price');
    }

    public function getCalculatedPrice($value = null, $selectedOptions)
    {
        $value = (float)str_replace(",", ".", $value);

        if ($this->hasData('id')) {
            switch ($this->getType()) {
                case "select":
                case "radiobuttons":
                case "listimage":
                case "overlayimage":
                    $values = $this->getValueCollection();
                    if (count($values) > 0) {
                        foreach ($values as $v) {
                            if ($v->getId() == $value) {
                                return $v->getPrice();
                            }
                        }
                    }
                    break;
                case "text":
                case "area":
                case "static":
                case "checkbox":
                case "date":
                    return $this->getPrice();
                    break;
                case "combi":
                case "expression":
                case "matrixvalue":
                    $pricelistModel = Mage::getModel('configurator/pricelist');
                    $collection = $pricelistModel->getCollection();
                    $collection->getSelect()->order('(0+value) ASC');
                    $collection->addFilter('option_id', $this->getId());

                    $pricelistItems = $collection->getItems();

                    $price = 0;
                    if (count($pricelistItems) > 0) {
                        foreach ($pricelistItems as $pricelistItem) {

                            if ($this->getOperatorValuePrice() != 'none')
                                $tmpPrice = $pricelistItem->getPrice() * $value;
                            else
                                $tmpPrice = $pricelistItem->getPrice();

                            switch ($pricelistItem->getOperator()) {

                                case ">":
                                    if ($value > $pricelistItem->getValue()) $price = $tmpPrice;
                                    break;
                                case ">=":
                                    if ($value >= $pricelistItem->getValue()) $price = $tmpPrice;
                                    break;
                                case "<":
                                    if ($value < $pricelistItem->getValue()) return $tmpPrice;
                                    break;
                                case "<=":
                                    if ($value <= $pricelistItem->getValue()) return $tmpPrice;
                                    break;
                                case "==":
                                    if ($value == $pricelistItem->getValue()) return $tmpPrice;
                                    break;
                            }
                        }
                    }
                    return $price;
                    break;
                case "selectcombi":
                case "listimagecombi":
                case "overlayimagecombi":
                    $price = 0;

                    //Zend_Debug::dump($selectedOptions);
                    //Zend_Debug::dump("selectcombi price");

                    $values = $this->getValueCollection();
                    if (count($values) > 0 && count($selectedOptions) > 0) {
                        foreach ($values as $v) {
                            if ($v->getId() == $value) {

                                if ($this->getOperator() == '*') {
                                    $cValue = 1;
                                } else if ($this->getOperator() == '+') {
                                    $cValue = 0;
                                } else if ($this->getOperator() == 'string') {
                                    $cValue = '';
                                }

                                foreach ($this->getChildrenStatus() as $child) {
                                    if (isset($child['is_combi']) && $child['is_combi']) {

                                        $childValue = $selectedOptions[$child['id']];

										// In case the child is one of these types, the $childValue represents the ID of the OptionValue type. So we need to
										// take the value of the OptionValue.
                                        if (in_array($child['type'], array('select', 'listimage', 'selectcombi', 'listimagecombi', 'overlayimagecombi', 'overlayimage'))) {
                                            $valueModel = Mage::getModel('configurator/value')->load($childValue);
                                            $childValue = $valueModel->getValue();
                                        }

                                        if ($this->getOperator() == '*') {
                                            $cValue *= (float)$childValue;
                                        } else if ($this->getOperator() == '+') {
                                            $cValue += (float)$childValue;
                                        } else if ($this->getOperator() == 'string') {
                                            $cValue .= (float)$childValue;
                                        }
                                    }
                                }

                                if ($this->getOperator() == '*') {
                                    $cValue *= (float)$v->getValue();
                                } else if ($this->getOperator() == '+') {
                                    $cValue += (float)$v->getValue();
                                } else if ($this->getOperator() == 'string') {
                                    $cValue .= (float)$v->getValue();
                                }

                                //Zend_Debug::dump($cValue);

                                if ($this->hasPricelist($v->getId())) {

                                    foreach ($this->getPricelistData($v->getId()) as $pricelistItem) {

                                        $tmpPrice = $pricelistItem->getPrice();

                                        //Zend_Debug::dump($cValue . " " .$tmpPrice);

                                        switch ($pricelistItem->getOperator()) {
                                            case ">":
                                                if ($cValue > $pricelistItem->getValue()) $price = $tmpPrice;
                                                break;
                                            case ">=":
                                                if ($cValue >= $pricelistItem->getValue()) $price = $tmpPrice;
                                                break;
                                            case "<":
                                                if ($cValue < $pricelistItem->getValue()) return $tmpPrice;
                                                break;
                                            case "<=":
                                                if ($cValue <= $pricelistItem->getValue()) return $tmpPrice;
                                                break;
                                            case "==":
                                                if ($cValue == $pricelistItem->getValue()) return $tmpPrice;
                                                break;
                                        }
                                    }


                                } else {
                                    $price = $v->getPrice() * $cValue;
                                }
                            }
                        }
                    }

                    return $price;
                    break;
            }
        }
        return 0;
    }

    public function getValueTagArray()
    {
        if ($this->_value_tag_array) {
            return $this->_value_tag_array;
        }

        $result = array();
        $values = Mage::getModel("configurator/value")->getCollection();
        $values->addFieldToFilter("option_id", $this->getId());
        $count = $values->count();
        foreach ($values as $value) {
            $tags = Mage::getModel("configurator/valuetag")->getCollection();
            $tags->addFieldToFilter("option_value_id", $value->getId());
            foreach ($tags as $tag) {
                $result[] = $tag->getTag();
            }
        }
        $this->_value_tag_array = array_unique($result);
        return $this->_value_tag_array;
    }

    public function getValueTagBlacklistArray($option_id, $option_value_id, $related_option_id)
    {
        $result = array();
        $values = Mage::getModel("configurator/valuetagblacklist")->getCollection();
        $values->addFieldToFilter("option_id", $option_id);
        $values->addFieldToFilter("option_value_id", $option_value_id);
        $values->addFieldToFilter("related_option_id", $related_option_id);
        foreach ($values as $value) {
            $result[] = $value->getTag();
        }
        return array_unique($result);
    }

    public function getAllValues()
    {
        $result = array();
        $values = Mage::getModel("configurator/value")->getCollection();
        $values->addFieldToFilter("option_id", $this->getId());
        foreach ($values as $value)
            $result[] = $value->getTitle();
        return $result;
    }

    public function getDeepLink($product_options_id, $selectedTemplateOptions)
    {
        $deeplink = "";
        foreach ($selectedTemplateOptions as $option_id => $value) {
            if (strlen($deeplink) > 0)
                $deeplink .= "&";
            $deeplink .= "o_" . $product_options_id . "_" . $option_id . "=" . $value;
        }

        return $deeplink;
    }

}