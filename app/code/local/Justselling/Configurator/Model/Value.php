<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright � 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
**/
 
class Justselling_Configurator_Model_Value extends Mage_Core_Model_Abstract
{
	/**
	 * 
	 * Template
	 * @var Justselling_Configurator_Model_Template
	 */
	protected $_template;
	
	protected function _construct()
	{
		parent::_construct();
		$this->_init('configurator/value');
	}
	
	public function getChildOptionValueStatus($optionId) {
		$valuestatusModel = Mage::getModel('configurator/valuechildstatus');		
		$collection = $valuestatusModel->getCollection();
		//exit;
		
		$collection->addFilter('option_value_id',$this->getId());
		$collection->addFilter('option_id',$optionId);
		
		return $collection->getFirstItem();	
	}
	
	public function getBlacklistValue($childOptionId) {
		$blacklistModel = Mage::getModel('configurator/blacklist');		
		$collection = $blacklistModel->getCollection();
		
		$collection->addFilter('option_value_id',$this->getId());
		$collection->addFilter('child_option_value_id',$childOptionId);
		
		return $collection->getFirstItem();	
	}
	
	public function getPrice() {
		$price = $this->price;
	
		$option = Mage::getModel('configurator/option')->load($this->getOptionId());
		$price = Mage::helper('configurator')->getDiscountPrice($option, $price);
	
		return $price;
	}

	public function getTags() {
		$collection = Mage::getModel("configurator/valuetag")->getCollection();
		$collection->addFieldToFilter("option_value_id",$this->getId());
		$collection->setOrder("id","ASC");

		return $collection;
	}
	
	public function getTagsArray() {
		$collection = $this->getTags();
		$result = array();
		foreach ($collection as $item) {
			$result[] = $item->getTag();
		}
		
		return $result;
	}
	
	public function getTagsString() {
		$collection = $this->getTags();
		$result = "";
		foreach ($collection as $item) {
			if ($result) $result .= " ";
			$result .= $item->getTag();
		}
	
		return $result;
	}	
	

}