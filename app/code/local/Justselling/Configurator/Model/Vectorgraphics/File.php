<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2013 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Model_Vectorgraphics_File extends Mage_Core_Model_Abstract
{

    const STATUS_CREATED         	    = 0;
    const STATUS_ASSIGNED_TO_QUOTE      = 1;
    const STATUS_ASSIGNED_TO_ORDER      = 2;


    protected function _construct()
    {
        parent::_construct();
        $this->_init('configurator/vectorgraphics_file');
    }


}
