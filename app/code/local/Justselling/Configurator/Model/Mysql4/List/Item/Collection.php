<?php
class Justselling_Configurator_Model_Mysql4_List_Item_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('configurator/list_item');
    } 
    
    public function joinProductName()
    {
        $entityTypeId = Mage::getResourceModel('catalog/config')
                ->getEntityTypeId();
        $attribute = Mage::getModel('catalog/entity_attribute')
            ->loadByCode($entityTypeId, 'name');

        $this->getSelect()
            ->join(
                array('product_name_table' => $attribute->getBackendTable()),
                'product_name_table.entity_id=main_table.product_id' .
                    ' AND product_name_table.store_id=0' .
                    ' AND product_name_table.attribute_id=' . $attribute->getId().
                    ' AND product_name_table.entity_type_id=' . $entityTypeId,
                array('value'=>'product_name_table.value')
            );

        return $this;
    }
    
    public function joinList()
    {
        $this->getSelect()
            ->join(
                array('list' => $this->getTable('configurator/list_list')),
                'list.list_id = main_table.list_id',
                array('title'=>'list.title')
            );

        return $this;
    }    
}
