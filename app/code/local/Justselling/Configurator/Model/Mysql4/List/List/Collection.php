<?php
class Justselling_Configurator_Model_Mysql4_List_List_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('configurator/list_list');
    }
    
    public function addCustomerFilter($customerId){
        $this->addFieldToFilter('customer_id', $customerId);
        return $this;
    }
}