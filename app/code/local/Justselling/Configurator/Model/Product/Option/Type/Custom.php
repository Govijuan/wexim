<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright (C) 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
 **/

class Justselling_Configurator_Model_Product_Option_Type_Custom extends Mage_Catalog_Model_Product_Option_Type_Default
{

    public function getJsTemplateId($optionValue) {
        $arr =  unserialize($optionValue);

        if (!is_array($arr))
            return false;

        foreach ($arr as $key=>$optionValues) {
            return $key;
        }
    }

    public function getDynamics($jsTemplateId, $optionId, $optionValue) {
        $arr =  unserialize($optionValue);

        if (!is_array($arr))
            return false;

        if (isset($arr[$jsTemplateId]['dynamics']) && isset($arr[$jsTemplateId]['dynamics'][$optionId])) {
            return $arr[$jsTemplateId]['dynamics'][$optionId];
        }

        return false;
    }

	protected function setDynamics($js_template_id, $option_id, $value) {
        $dynamics = Mage::getSingleton('core/session')->getDynamics();
        $dynamics[$js_template_id][$option_id] = $value;
        Mage::getSingleton('core/session')->setDynamics($dynamics);
    }

    public function getTemplateOption($optionValue)
    {
        $arr =  unserialize($optionValue);
        if (!is_array($arr))
            return false;

        $options = array();
        $selectedTemplateOptions = array();
        $hasTextimage = false;

        foreach ($arr as $js_template_id =>$optionValues) {


            foreach($optionValues as $valueKey => $optionValue) {
                if ($valueKey == "postprice") {
                    $options[] = array(
                        'option' => array("title" => "postprice", "is_visible" => 0),
                        'value' => array(
                            'title'=> "__postprice",
                            'price'=> $optionValue,
                            'sku' => ""
                        )
                    );
                }
                if( $valueKey == "template" ) {
                    foreach ($optionValue as $optionId => $valueId ) {
                        $optionModel = Mage::getModel('configurator/option')->load($optionId);
                        $template_id = $optionModel->getTemplateId();
                        if( !empty($valueId) ) {

                            /* Get option price */
                            switch ($optionModel->getType()) {
                                case "textimage":
                                    $price =  $optionModel->getCalculatedPrice($valueId,$optionValue);
                                    $hasTextimage = true;
                                    break;
                                case "area":
                                case "text":
                                case "static":
                                case "combi":
                                case "matrixvalue":
                                case "expression":
                                case "http":
                                case "checkbox":
                                case "date":
                                    $price =  $optionModel->getCalculatedPrice($valueId,$optionValue);
                                    break;
                                case "selectcombi":
                                case "listimagecombi":
                                case "overlayimagecombi":
                                    $price = $optionModel->getCalculatedPrice($valueId,$optionValue);
                                    break;
                                case "listimage":
                                case "select":
                                case "radiobuttons":
                                case "selectimage":
                                case "overlayimage":
                                    $valueModel = Mage::getModel('configurator/value')->load($valueId);
                                    $price = $valueModel->getPrice();
                                    break;
                            }

                            $option = null;
                            try {
                                $option = $this->getOption();
                            } catch (Exception $e) {
                            }
                            if ($option && $option instanceof Mage_Catalog_Model_Product_Option) {
                                $product_id = $this->getOption()->getProductId();
                                $price = Mage::helper('configurator')->getDiscountPrice($optionModel, $price, $product_id);
                            } else {
                                $price = Mage::helper('configurator')->getDiscountPrice($optionModel, $price);
                            }

                            /* build option array */
                            switch ($optionModel->getType()) {
                                case "area":
                                case "text":
                                case "static":
                                case "combi":
                                case "matrixvalue":
                                case "expression":
                                case "http":
                                case "textimage":
                                case "checkbox":
                                case "date":
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=>$valueId,
                                            'price'=> $price,
                                            'sku' => $optionModel->getSku()
                                        )
                                    );
                                    $selectedTemplateOptions[$optionModel->getId()] = $valueId;
                                    break;
                                case "selectcombi":
                                case "listimagecombi":
                                case "overlayimagecombi":
                                    $valueModel = Mage::getModel('configurator/value')->load($valueId);
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=> $valueModel->getTitle(),
                                            'price'=> $price,
                                            'sku'=> $valueModel->getSku()
                                        )
                                    );
                                    $selectedTemplateOptions[$optionModel->getId()] = $valueModel->getId();
                                    break;
                                case "listimage":
                                case "select":
                                case "selectimage":
                                case "overlayimage":
                                case "radiobuttons":
                                    $options[] = array(
                                        'option' => $optionModel->getData(),
                                        'value' => array(
                                            'title'=> $valueModel->getTitle(),
                                            'price'=> $price,
                                            'sku' => $valueModel->getSku()
                                        )

                                    );
                                    $selectedTemplateOptions[$optionModel->getId()] = $valueModel->getId();
                                    break;
                            }
                        }
                    }
                }
            }
        }

        /* store select template options and js-option in registry */
        if ($selectedTemplateOptions) {
            if (Mage::registry("selected_template_options") != null) {
                Mage::unregister("selected_template_options");
            }
            Mage::register("selected_template_options", $selectedTemplateOptions);
        }
        if ($js_template_id) {
            if (Mage::registry("js_template_id") != null) {
                Mage::unregister("js_template_id");
            }
            Mage::register("js_template_id", $js_template_id);
        }
        if ($template_id) {
            if (Mage::registry("template_id") != null) {
                Mage::unregister("template_id");
            }
            Mage::register("template_id", $template_id);
        }
        if ($hasTextimage) {
            if (Mage::registry("has_textimage") != null) {
                Mage::unregister("has_textimage");
            }
            Mage::register("has_textimage", $hasTextimage);
        }


        return $options;
    }

    /**
     * (non-PHPdoc)
     * @see Mage_Catalog_Model_Product_Option_Type_Default::getOptionPrice()
     */
    public function getOptionPrice($optionValue, $basePrice)
    {
        $templateOptions = NULL;
        if( is_array($optionValue) ) {
            $optionValue = serialize(array($this->getJsTemplateOption($optionValue) => array('template' => $optionValue)));
            $templateOptions = $this->getTemplateOption($optionValue);
        } else {
            $templateOptions = $this->getTemplateOption($optionValue);
        }

        $optionPrice = 0;
        foreach ($templateOptions as $templateOption) {
            $optionPrice += (float) $templateOption['value']['price'];
        }
        return $optionPrice;
    }

    public function setUserValue($value)
    {
        if (Mage::getSingleton('core/session')->getDynamics()) {
            foreach (Mage::getSingleton('core/session')->getDynamics() as $templateid => $configurator) {
                $value[$templateid]['dynamics'] = $configurator;
            }
        }

        $result = $this->setData('user_value',serialize($value));
        return $result;
    }

    /**
     * Validate user input for option
     *
     * @throws Mage_Core_Exception
     * @param array $values All product option values, i.e. array (option_id => mixed, option_id => mixed...)
     * @return Mage_Catalog_Model_Product_Option_Type_Default
     */
    public function validateUserValue($values)
    {
        //Justselling_Debug::dump("validateUserValue");

        $option = $this->getOption();
        $this->setUserValue( $values[$option->getId()] );
        //$this->setUserValue( serialize($values[$option->getId()]) );
        $this->setIsValid(true);
        return $this;
    }

    /**
     * Prepare option value for cart
     *
     * @return mixed Prepared option value
     */
    public function prepareForCart()
    {
        Mage::Log("prepareForCart start ".$this->getUserValue());

        if ($this->getIsValid() && strlen($this->getUserValue()) > 0 && $this->getUserValue() != 'N;') {
            return $this->getUserValue();
        } else {
            return null;
        }
    }


    public function getFormattedDynamicsValue($key, $value) {
        switch ($key) {
            case "font":
                $font = Mage::getModel("configurator/font")->load($value);
                $typestring = array(0=>'Regular' ,1=>'Italic',2=>'Bold',3=>'Bold-Italic');
                $value = $font->getTitle()." ".$typestring[$font->getFontType()];
                break;
            case "font_size":
                $value = $value."pt";
                break;
            case "font_angle":
                $value = $value." ".Mage::helper('configurator')->__("degree");
                break;
            case "font_color":
                $font = Mage::getModel("configurator/optionfontcolor")->getColorByCode($value);
                $value = $font->getColorTitle();
                break;
            case "font_pos":
                $font = Mage::getModel("configurator/optionfontposition")->getPositionByXY($value);
                $value = $font->getPosTitle();
                break;
            case "text_alignment":
                $aligntring = array(1=>'Left' ,2=>'Center',3=>'Right');
                $value = $aligntring[$value];
                break;
        }
        return $value;
    }

    /**
     * Return formatted option value for quote option
     *
     * @param string $value Prepared for cart option value
     * @return string
     */
    public function getFormattedOptionValue($value)
    {
        if ($value == 'N;') { return ""; }

        $jsTemplateId = $this->getJsTemplateId($value);
        $templateOptions = $this->getTemplateOption($value);
		$session_id =  Mage::getSingleton('core/session')->getSessionId();

        $html = '<ul>';

        foreach ($templateOptions as $templateOption) {
            if (isset($templateOption['option']['template_id']))  {
                $template = Mage::getModel('configurator/template')->load($templateOption['option']['template_id']);

                $price = ' +'. number_format(Mage::app()->getStore()->convertPrice($templateOption['value']['price']),2,",",""). ' ' . Mage::app()->getStore()->getCurrentCurrency()->getCode();
                if ($templateOption['option']['is_visible']) {
                    $html.= '<li><strong>'.$templateOption['option']['title'].'</strong> ';
                    $html.= $templateOption['value']['title'];

                    if ($template->getOptionValuePrice() != 2) { // 2 means show price
                        if ($template->getOptionValuePriceZero() || $price > 0)
                            $html .= $price;
                    }

                    // Check for dynamics value
                    $dynamics = $this->getDynamics($jsTemplateId, $templateOption['option']['id'],$value);
                    if($dynamics) {
                        $this->setDynamics($jsTemplateId, $templateOption['option']['id'], $dynamics);
                        $html .= "<li><strong>".Mage::helper('configurator')->__('Font')."</strong> ";
                        $texthtml = "";
                        foreach ($dynamics as $key=>$value) {
                            if ($texthtml) $texthtml .= ", ";
                            $value = $this->getFormattedDynamicsValue($key, $value);
                            $texthtml .= Mage::helper('configurator')->__($key)." ".Mage::helper('configurator')->__($value);
                        }
                        $html .= $texthtml."</li>";
                    }

                    $html .= '</li>';
                }

            }

        }

		$data = $this->getData();
		if($data){
			$configurationItem = $data['configuration_item'];
			$configurationItemOption = $data['configuration_item_option'];
			if($configurationItem){
				$configurationItemData = $configurationItem->getData();
				if($configurationItemData){
					$quoteItemId = $configurationItemData['item_id'];
					if($quoteItemId){
						$html = $this->getUploadLiElement($session_id, $jsTemplateId, $quoteItemId, $html);
					}
				}
			}elseif($configurationItemOption){
				$configurationItemData = $configurationItemOption->getData();
				if($configurationItemData){
					$quoteItemId = $configurationItemData['item_id'];
					if($quoteItemId){
						$html = $this->getUploadLiElement($session_id, $jsTemplateId, $quoteItemId, $html);
					}
				}
			}
		}


        $html.= '</ul>';

        return $html;
    }

    public function getPrintableOptionValue($value)
    {
        //	Mage::Log("getPrintableOptionValue start value=".var_export($value,true));
        if ($value == 'N;') { return ""; }

        $jsTemplateId = $this->getJsTemplateId($value);
        $templateOptions = $this->getTemplateOption($value);
		$session_id =  Mage::getSingleton('core/session')->getSessionId();
        //Mage::Log("templateOptions=".var_export($templateOptions,true));

        $text = '';

        foreach ($templateOptions as $templateOption) {
            if (isset($templateOption['option']['template_id']))
                $template = Mage::getModel('configurator/template')->load($templateOption['option']['template_id']);
            // Mage::Log("id=".$templateOption['option']['title']." ".$templateOption['value']['price']);
            $price = ' +'. number_format(Mage::app()->getStore()->convertPrice($templateOption['value']['price']),2,",",""). ' ' . Mage::app()->getStore()->getCurrentCurrency()->getCode();
            if ($templateOption['option']['is_visible']) {
                // Mage::Log("visible is true");

                $text.= $templateOption['option']['title'].': ';
                $text.= $templateOption['value']['title'];
                if ($template->getOptionValuePrice() != 2) {
                    if ($template->getOptionValuePriceZero() || $price > 0)
                        $text.= $price;
                }

                // Check for dynamics value
                if ($dynamics = $this->getDynamics($jsTemplateId, $templateOption['option']['id'],$value)) {
                    $text .= " (".Mage::helper('configurator')->__('Font').": ";
                    $subtext = "";
                    foreach ($dynamics as $key=>$value) {
                        if ($subtext) $subtext .= ", ";
                        $value = $this->getFormattedDynamicsValue($key, $value);
                        $subtext .= Mage::helper('configurator')->__($key)." ".$value;
                    }
                    $text .= $subtext.") ";
                }

                $text .= ", ";
            }

        }


		$data = $this->getData();
		if($data){
			$configurationItem = $data['configuration_item'];
			$configurationItemOption = $data['configuration_item_option'];
			if($configurationItem){
				$configurationItemData = $configurationItem->getData();
				if($configurationItemData){
					$quoteItemId = $configurationItemData['item_id'];
					if($quoteItemId){
						$text = $this->getUploadTextElement($session_id, $jsTemplateId, $quoteItemId, $text);
					}
				}
			}elseif($configurationItemOption){
				$configurationItemData = $configurationItemOption->getData();
				if($configurationItemData){
					$quoteItemId = $configurationItemData['item_id'];
					if($quoteItemId){
						$text = $this->getUploadTextElement($session_id, $jsTemplateId, $quoteItemId, $text);
					}
				}
			}
		}



		if (strlen($text) > 0) {
            $text = substr($text, 0, strlen($text)-2);
        }
        $text.= '';
        return $text;
    }

    public function getOptionSku($optionValue, $skuDelimiter)
    {
        $sku = '';

        if( is_array($optionValue) ) {
            $optionValue = serialize(array($this->getJsTemplateOption($optionValue) => array('template' => $optionValue)));
            $templateOptions = $this->getTemplateOption($optionValue);
        } else {
            $templateOptions = $this->getTemplateOption($optionValue);
        }

        if( is_array($templateOptions) && count($templateOptions) > 0) {
            $skus = array();
            foreach($templateOptions as $templateOption) {
                if( !empty($templateOption['value']['sku']) ) {
                    $skus[] = $templateOption['value']['sku'];
                }
            }
            if (sizeof($skus) > 0) {
                $sku = implode($skuDelimiter, $skus);
            }
        }

        return $sku;
    }

	/**
	 * @param $session_id
	 * @param $jsTemplateId
	 * @param $quoteItemId
	 * @param $html
	 * @return string
	 */
	private function getUploadLiElement($session_id, $jsTemplateId, $quoteItemId, $html){
		$uploads = Mage::getModel("configurator/upload")->getCollection();
		$uploads->addFieldToFilter("session_id", array('eq' => $session_id));
		$uploads->addFieldToFilter("js_template_id", array('eq' => $jsTemplateId));
		$uploads->addFieldToFilter("quote_item_id", $quoteItemId);

		if(count($uploads) > 0){
			foreach($uploads as $upload){
				$texthtml = '';
				$option = Mage::getModel("configurator/option")->load($upload->getOptionId());
				$optionTitle = $option->getTitle();
				$html .= '<li><strong>' . $optionTitle . '</strong> ';
				$texthtml .= basename($upload->getFile());

				$html .= $texthtml;
				$template = Mage::getModel('configurator/template')->load($option->getTemplateId());
				if ($template->getOptionValuePrice() != 2) { // 2 means show price
					$price = ' +'. number_format(Mage::app()->getStore()->convertPrice($option->getPrice()),2,",",""). ' ' . Mage::app()->getStore()->getCurrentCurrency()->getCode();
					if ($template->getOptionValuePriceZero() || $price > 0)
						$html .= $price;
				}
				$html .= "</li>";
			}
			return $html;
		}
		return $html;
	}
	private function getUploadTextElement($session_id, $jsTemplateId, $quoteItemId, $text){
		$uploads = Mage::getModel("configurator/upload")->getCollection();
		$uploads->addFieldToFilter("session_id", array('eq' => $session_id));
		$uploads->addFieldToFilter("js_template_id", array('eq' => $jsTemplateId));
		$uploads->addFieldToFilter("quote_item_id", $quoteItemId);

		if(count($uploads) > 0){
			foreach($uploads as $upload){
				$texthtml = '';
				$option = Mage::getModel("configurator/option")->load($upload->getOptionId());
				$optionTitle = $option->getTitle();

				$text.= $optionTitle.': ';
				$texthtml .= basename($upload->getFile());
				$text.= $texthtml;

				$template = Mage::getModel('configurator/template')->load($option->getTemplateId());
				if ($template->getOptionValuePrice() != 2) { // 2 means show price
					$price = ' +'. number_format(Mage::app()->getStore()->convertPrice($option->getPrice()),2,",",""). ' ' . Mage::app()->getStore()->getCurrentCurrency()->getCode();
					if ($template->getOptionValuePriceZero() || $price > 0)
						$text .= $price;
				}
				$text .= ", ";
			}
			return $text;
		}
		return $text;
	}

}
