<?php

/**
 * justselling Germany Ltd. EULA
 * http://www.justselling.de/
 * Read the license at http://www.justselling.de/lizenz
 *
 * Do not edit or add to this file, please refer to http://www.justselling.de for more information.
 *
 * @category    justselling
 * @package     justselling_configurator
 * @copyright   Copyright � 2012 justselling Germany Ltd. (http://www.justselling.de)
 * @license     http://www.justselling.de/lizenz
**/
 
class Justselling_Configurator_Model_Optiongroup extends Mage_Core_Model_Abstract
{
	/**
	 * 
	 * Template
	 * @var Justselling_Configurator_Model_Template
	 */
	protected $_template;
	
	public $children = array();
	
	public $values = array();
	
	protected function _construct()
	{
		parent::_construct();
		$this->_init('configurator/optiongroup');
	}
	
	/**
	 * 
	 * set Template
	 * @param Justselling_Configurator_Model_Template $template
	 */
	public function setTemplate(Justselling_Configurator_Model_Template $template)
	{
		$this->_template = $template;
		return $this;
	}
	
	/**
	 * 
	 * get Template
	 * @return Justselling_Configurator_Model_Template
	 */
	public function getTemplate()
	{
		return $this->_template;
	}
	
	public function saveTemplateGroups(array $groups)
	{		
		// Zend_Debug::dump($groups); exit;
		 		
		foreach($groups as $group) {			
			$groupModel = Mage::getModel("configurator/optiongroup")->load($group['id']);
			
			
			if( $groupModel->template_id != $this->getTemplate()->getId() ) {
				$groupModel = Mage::getModel("configurator/optiongroup");
			}
			
			if( $group['is_delete'] == "1" ) {
				
				$groupModel->delete();
				
			} else {				
				$groupModel->setTemplateId( $this->getTemplate()->getId() );
				$groupModel->setTitle( $group['title'] );
				$groupModel->setSortOrder( $group['sort_order'] );

				if (!empty($group['groupimage'])) {
					$targetFileName = 'group-image-' .$group['id'] .$group['groupimage'];
					if (strpos($targetFileName,'configurator/') !== false) {
						$group_image = $group['groupimage'];
					}else{
						$group_image = "configurator/".$targetFileName;
					}
					try {
						$tempFolder = Mage::getBaseDir('media') . '/tmp/upload/admin';
						$tempFile = rtrim($tempFolder, '/') . '/' . $group['groupimage'];

						if (file_exists($tempFile)) {
							$targetFolder = Mage::getBaseDir('media') . DS . 'configurator';
							if (!file_exists(str_replace('//', '/', $targetFolder))) {
								mkdir(str_replace('//', '/', $targetFolder), 0755, true);
							}
							$targetFile = rtrim($targetFolder, '/') . '/' . $targetFileName;
							rename($tempFile, $targetFile);
						}
					} catch (Exception $e) {
						$group_image = null;
					}
				} else {
					$group_image = null;
				}
				$groupModel->setGroupImage($group_image);

				$result = $groupModel->save();
				Mage::Log("RES=".var_export($result->getData(),true));
				// Zend_Debug::dump($groupModel->getData()); exit;					
			}			
		}
	}
	
	public function saveTemplateGroup(array $group)
	{
		
	}
	
	public function getTemplateGroups($templateId)
	{
		$collection = $this->getCollection();	
		$collection->addFilter('template_id',$templateId);		
		return $collection;
	}
}