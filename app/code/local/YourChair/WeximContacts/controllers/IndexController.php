<?php

class YourChair_WeximContacts_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        // Wir brauchen keine Indexaktionen
    }
    
    public function perfectChairPostAction() {
        $post = $this->getRequest()->getPost();
        if($post) {
            $this->_performTransmit("YourChair: Der perfekte Stuhl (Template)", $post);
        }
    }
    
    public function contactUsPostAction() {
        $post = $this->getRequest()->getPost();
        if($post) {
            $this->_performTransmit("YourChair: Kontaktformular (Template)", $post);
        }
    }
    
    // ****
    
    protected function _performTransmit($templateName, $params) {
        try {
            $collection =  Mage::getResourceSingleton('core/email_template_collection');
            $templateId = NULL;
            foreach($collection as $value) {
                if(!strcmp(strtolower($value->getTemplateCode()), strtolower($templateName))) {
                    $templateId = $value->getTemplateId();
                    break;
                }
            }
            if ($templateId === NULL) {
                echo "FALSE";
                throw new Exception("E-Mail-Template \"YourChair: Der perfekte Stuhl (Template)\" wurde nicht gefunden.");
                return;
            }
            
            $sender = array(
                'name'  => Mage::getStoreConfig('trans_email/ident_general/name'),
                'email' => Mage::getStoreConfig('trans_email/ident_general/email')
            );
            $storeId = Mage::app()->getStore()->getId();
            $translate = Mage::getSingleton('core/translate');
            $translate->setTranslateInline(FALSE);
            
            $mailTemplate = Mage::getModel('core/email_template');
            $mailTemplate->sendTransactional(
                    $templateId,
                    $sender,
                    "m.werner@planet108.de", //Mage::getStoreConfig('trans_email/ident_general/email'),
                    "Mario Werner",
                    $params,
                    $storeId
            );
                
            $translate->setTranslateInline(TRUE);

            if (!$mailTemplate->getSentSuccess()) {
                echo "FALSE";
                throw new Exception();
                return;
            }
            
            echo "TRUE";
            return;
        } catch (Exception $e) {
            echo "FALSE";
            
            $translate->setTranslateInline(TRUE);

            Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
            return;
        }
    }
    
}
?>