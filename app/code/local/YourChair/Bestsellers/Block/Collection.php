<?php

class YourChair_Bestsellers_Block_Collection extends Mage_Catalog_Block_Product_Abstract {    
    protected $_productName;
    
    public function __construct(){
        parent::__construct();        
        $this->_productName = '*';
    }
    
    public function setProductName($name) {
        $this->_productName = $name;
        return $this;
    }
    
    public function getProductCollection() {
        $storeId = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('reports/product_sold_collection');
        
        $products->addOrderedQty();
        if($this->_productName != '*')
            $products->addAttributeToFilter('name', $this->_productName);
        $products->addAttributeToSelect('*')            
            ->addAttributeToSelect(array('name', 'price', 'small_image'))
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder('ordered_qty', 'desc');
            
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
 
        $products->setPageSize(10)->setCurPage(1);
        $this->setProductCollection($products);
        
        $collection = parent::getProductCollection();
        //$collection->printlogquery(true);
        
        return $collection;
    }
    
    function getProductCollectionItems($collection) {
        $items = $collection->getItems();
        return $this->_getBestsellersConfigurations($items);
    }
    
    protected function _getBestsellersConfigurations($items) {        
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('sales_flat_order_item');
        $new_items = array();
        foreach($items as $item) {
            $query = "SELECT `product_options`, `sku` FROM `{$tableName}` WHERE `product_id` = {$item->getId()} GROUP BY `sku`";
            $rows = $readConnection->fetchAll($query);
            foreach($rows as $configuration) {
                $new_item = clone $item;
                $new_item['product_options'] = unserialize($configuration['product_options']);
                $new_item['order_sku'] = $configuration['sku'];
                $new_items[] = $new_item;
            }
        }
        return $new_items;
    }
}