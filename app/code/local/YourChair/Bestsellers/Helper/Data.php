<?php

class YourChair_Bestsellers_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $_resource;
    protected $_readConnection;
    protected $_templateName;
    protected $_templateId;
    protected $_options;
    protected $_translatedOptions;
    protected $_product;
    
    function prettify($string) {
        if(!strlen($string))
            return '';
            
        $temp = $string;
        $temp = str_replace('ue', 'ü', $temp);
        $temp = str_replace('ae', 'ä', $temp);
        $temp = str_replace('oe', 'ö', $temp);
        $temp[0] = strtoupper($temp[0]);
        
        return $temp;
    }
    
    function uglyfiy($string) {
        if(!strlen($string))
            return '';
            
        $temp = $string;
        $temp = str_replace('ü', 'ue', $temp);
        $temp = str_replace('ä', 'ae', $temp);
        $temp = str_replace('ö', 'oe', $temp);
        $temp = str_replace(' ', '_', $temp);
        
        return strtolower($temp);
    }

    public function setCurrentProduct($product) {
        $this->_product = $product;
        return $this;
    }    
        
    public function getTemplateId() {
        $tableName = $this->_resource->getTableName('configurator_template');
        $query = "SELECT `id` FROM `{$tableName}` WHERE (`title` = '{$this->_templateName}')";
        $query_data = $this->_readConnection->fetchAll($query);
        $this->_templateId = $query_data[0]['id'];
        return $this->_templateId;
    }
    
    public function getProductCustomOptions() {
        $options = unserialize($this->_product['product_options']['options'][0]['option_value']);        
        if(!is_array($options))
            return array();
        else        
            return $options[key($options)]['template'];
    }
    
    public function getRawOptions() {
        return $this->_options;
    }
    
    public function getDefaultProductOptionsTitles() {
        $query = "SELECT cov.`title` FROM `configurator_option_value` AS cov " .
            "JOIN `configurator_option` AS co ON co.`template_id` = " .
                "(SELECT cpot.`conf_template_id` FROM `configurator_product_option_template` AS cpot WHERE cpot.`catalog_product_option_option_id` = '{$this->_product->getId()}') " .
                "WHERE cov.`id` = co.`default_value`";        
        $rows = $this->_readConnection->fetchAll($query);
        return $rows;
    }
    
    public function translateOptions() {
        $translatedOptions = array();
        $options = $this->_options;
        $tableName = $this->_resource->getTableName('configurator_option');
        $query = "SELECT `id`, `title`, `alt_title` FROM `{$tableName}` WHERE (`template_id` = '{$this->_templateId}')";
        $rows = $this->_readConnection->fetchAll($query);
        $tableName = $this->_resource->getTableName('configurator_option_value');
        foreach($rows as $row) {
            foreach($options as $key => $option) {
                if($key == $row['id']) {                    
                    $query = "SELECT `title`, `price` FROM `{$tableName}` WHERE (`id` = '{$option}') AND (`option_id` = '{$key}')";
                    $value_rows = $this->_readConnection->fetchAll($query);
                    $value = $value_rows[0];
                    $translatedOptions[$row['alt_title']] = array('value' => $value['title'], 'price' => $value['price']);
                    break;
                }
            }
        }
        
        return $translatedOptions;
    }
    
    public function getTranslatedOptions() {
        return $this->_translatedOptions;
    }
    
    public function checkTranslatedOptionsIntegrity($options = NULL) {
        if($options == NULL)
            $options = $this->_translatedOptions;
        if(!$options)
            return FALSE;
        $result = TRUE;
        foreach($options as $option) {
            if(!$option['value']) {
                $result = FALSE;
                break;
            }            
        }
        return $result;
    }
    
    public function getImageFromMatrix() {
        $tableName = $this->_resource->getTableName('configurator_matrix');
        $query = "SELECT `image_front` FROM `{$tableName}` WHERE ";
        $i = 0;
        foreach($this->_translatedOptions as $key => $value) {
            $query .= "(`{$key}` = '{$value['value']}')";
            if ($i++ < count($this->_translatedOptions) - 1)
                $query .= " AND ";
        }        
        $rows = $this->_readConnection->fetchAll($query);
        return $rows[0]['image_front'] . '.jpg';
    }    
    
}
