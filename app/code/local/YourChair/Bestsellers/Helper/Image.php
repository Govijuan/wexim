<?php

class YourChair_Bestsellers_Helper_Image extends YourChair_Bestsellers_Helper_Data /*Mage_Core_Helper_Abstract*/ {
    
    protected $_imageFileRoot;
    protected $_imageUrlRoot;
    protected $_imageResizedRoot;
    
    public function init($product) {
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        
        $this->setCurrentProduct($product);
        $this->_templateName = $this->_product->getName();
        $this->_imageFileRoot = Mage::getBaseDir('media') . DS . 'yourchair' . DS;
        $this->_imageUrlRoot = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'yourchair' . DS;
        $this->_imageResizedFileRoot = $this->_imageFileRoot . 'resized' . DS . $this->uglyfiy($this->_templateName) . '_';
        $this->_imageResizedUrlRoot = $this->_imageUrlRoot . 'resized' . DS . $this->uglyfiy($this->_templateName) . '_';
        $this->_templateId = $this->getTemplateId();

        $this->_translatedOptions = NULL;
        $this->_imageFile = NULL;
        $this->_options = $this->getProductCustomOptions();
        if(!$this->_options)
            return $this;
        
        $this->_translatedOptions = $this->translateOptions();
        $this->_imageFile = $this->getImageFromMatrix();
        
        return $this;
    }
    
    public function resize($width, $height = NULL) {
        if(!$this->_imageFile)
            return "";
            
        if($height === NULL)
            $height = $width;
        
        $imageFile = $this->_imageFileRoot . $this->_templateName . DS . $this->_imageFile;
        if(!file_exists($imageFile))
            return '';
        
        $imageResizedName = $width . 'x' . $height . '_';
        foreach($this->_options as $key => $value)
            $imageResizedName .= $key . '_' . $value;
        
        $imageResizedFile = $this->_imageResizedFileRoot . $imageResizedName . '.jpg';
        $imageResizedUrl = $this->_imageResizedUrlRoot . $imageResizedName . '.jpg';
        
        if (!file_exists($imageResizedFile) && file_exists($imageFile)) {
            $imageObj = new Varien_Image($imageFile);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            $imageObj->resize($width, $height);
            $imageObj->save($imageResizedFile);
        }
        
        return $imageResizedUrl;        
    }    

}
