/*
    Version 1.0.1.2
    Copyright (c) 2013 Mario Werner :: m.werner@planet108.de
*/
(function($) { 
    
    $.productsSlider = function(element, options) {
        this.options = {
            totalProductsCount: 0,
            positions: [],
            imageAutoWidth: 0,
            disabledAutoWidth: 0
        };
        
        this.init = function(element, options) {
            var productsCount = 0;        
            $(element).find('div.images>ul.items').find('li').each(function() {                
                productsCount++;    
            });
            if(!productsCount)
                return;

            this.options = $.extend(this.options, $.productsSlider.defaultOptions, options);
            var o = this.options;
            o.totalProductsCount = productsCount;                

            if(o.index == 'auto') {
                if (productsCount <= 2)
                    o.index = 0;
                else
                    o.index = Math.round(productsCount / 2) - 1;
            } else {
                if(o.index > productsCount - 1)
                    o.index = productsCount - 1;
                if((productsCount <= 2) && (o.index > 0))
                    o.index = 0;
            }
            
            if(o.imageWidth == 'auto') {
                $(window).bind('resize', function(e) {
                   o.imageAutoWidth = handleResize(element, o);
                   o.imageWidth = o.imageAutoWidth;
                   alignElements(element, o);
                });
            }
            
            $(window).trigger('resize');
            
            //alignElements(element, o);
            var index = o.index;

            var btnIndex = 0;
            var btnPrev = $('<div class="previous" style="left:' + (o.positions[btnIndex] - o.imageButtonWidth) + 'px; height:' + o.imageWidth + 'px"><span></span><img class="image-prev" src="' + o.imagePath + o.imageArrowPrev + '" title="' + o.captionPrev + '"></div>');
            var btnNext = $('<div class="next" style="left:' + (o.positions[btnIndex] + o.imageWidth) + 'px; height:' + o.imageWidth + 'px"><span></span><img class="image-next" src="' + o.imagePath + o.imageArrowNext + '" title="' + o.captionNext + '"></div>');
            $(element).find('div.images')
                .append(btnPrev)
                .append(btnNext)
                .after('<div style="clear:both">&nbsp;</div>');

            $(element).find('div.previous>img, div.next>img').each(function() {
                $(this).bind('click', function(event) {
                    o.index = handleClick(event, o, element);
                });
            });
        }
        
        this.init(element, options);        
    };
    
    $.fn.productsSlider = function(options) {
      return this.each(function() {
         (new $.productsSlider($(this), options));              
      });        
    };
    
    function handleResize(element, options) {
        var width = $(element).width();
        var imageAutoWidth = width / options.visibleImages;
        return imageAutoWidth;
    }

    function alignElements(element, options) {
        if((options.visibleImages == 'auto') || options.imageAutoWidth) {
            var containerWidth = '100%'; //$(element).parent().width();
        } else {
            var containerWidth = (options.imageWidth * options.visibleImages) + 'px';
        }

        $(element).css('width', containerWidth);
        //$(element).css('height', (options.imageWidth + 60) + 'px');
        containerWidth = $(element).width();
        var itemsListWidth = (options.totalProductsCount * options.imageWidth);
        $.productsSlider.itemsListLeftOffset = Math.round((containerWidth / 2) - (itemsListWidth / 2) + ((options.imageWidth * (options.totalProductsCount - 1)) / 2));

        if(options.disabledWidth == 'auto' || options.disabledAutoWidth) {
            options.disabledAutoWidth = options.imageWidth * 0.75;
            options.disabledWidth = options.disabledAutoWidth;
        }
                    
        getImagePositions(element, options);

        $(element).find('li.item-configuration-' + options.totalProductsCount.toString()).css('width', options.imageWidth + 'px');
        $(element).find('li.item-configuration-' + options.totalProductsCount.toString() + '>span').hide();

        $(element).find('ul.items').each(function() {                
            $(this).css('width', itemsListWidth.toString() + 'px');
            $(this).css('margin-left', $.productsSlider.itemsListLeftOffset.toString() + 'px');
        });

        $(element).find('div.images').css('height', options.imageWidth + 'px');

        $(element).find('img.item-image-' + options.index.toString()).css({'width': options.imageWidth, 'height': options.imageWidth, 'opacity': '1', 'cursor': 'auto', 'pointer-events': 'auto'});
        $(element).find('li.item-configuration-' + options.index.toString() + '>span').show();

        $(element).find('ul.items').each(function() {
            $(this).css('margin-left', options.positions[options.index] + 'px');
        });
                
        $(element).find('div.options li').each(function() {            
            $(this).css('height', ($(this).find('span').height() + 3) + 'px');
        });
        
        checkButtonsVisibility(options, element, false);
        
        var btnPrev = $(element).find('div.previous');
        var btnNext = $(element).find('div.next');
        if(undefined === btnPrev)
            return;
        var btnIndex = 0;
        btnPrev.css({'left': (options.positions[btnIndex] - options.imageButtonWidth) + 'px', 'height': options.imageWidth + 'px'});
        btnNext.css({'left': (options.positions[btnIndex] + options.imageWidth) + 'px', 'height': options.imageWidth + 'px'});
    }
    
    function getImagePositions(element, options) {
        var position;
        position = $.productsSlider.itemsListLeftOffset;
        var i = 0;
        options.positions.clear();
        $(element).find('div.images>ul.items').find('li').each(function() {                
            $(this).css('width', options.imageWidth + 'px');
            $(this).css('height', options.imageWidth + 'px');
            $(element).find('li.item-configuration-' + i.toString()).css('width', options.imageWidth + 'px');

            options.positions.push(position);
            position -= options.imageWidth;

            $(this).find('img.item-image-' + i.toString()).css({'width': options.disabledWidth, 'height': options.disabledWidth, 'opacity': '0.5', 'cursor': 'default', 'pointer-events': 'none'});
            $(element).find('li.item-configuration-' + i.toString() + '>span').hide();
            
            i++;    
        });
    }
    
    function checkButtonsVisibility(options, element, fade) {
        var button = $(element).find('div.images>div.previous>img');
        if(options.index == 0) {
            if(fade)
                button.stop().fadeOut(200);
            else
                button.hide();
        } else {
            if(fade)
                button.stop().fadeIn(200);
            else
                button.show();
        }
        var button = $(element).find('div.images>div.next>img');
        if(options.index == options.totalProductsCount - 1) {
            if(fade)
                button.stop().fadeOut(200);
            else
                button.hide();
        } else {
            if(fade)
                button.stop().fadeIn(200);
            else
                button.show();
        }
    }
    
    function handleClick(event, options, element) {
        var direction = -1;
        if(event.target.className == 'image-next')
            direction = 1;
        
        $(element).find('div.options li.item-configuration-' + options.index.toString() + '>span').stop().fadeOut(400);
        $(element).find('div.images img.item-image-' + options.index.toString())
            .stop()
            .animate({'width': options.disabledWidth, 'height': options.disabledWidth, 'opacity': '0.5'}, 400)
            .css({'cursor': 'default', 'pointer-events': 'none'});

        options.index += direction;
        if(options.index < 0)
            options.index = 0;
        if(options.index > options.totalProductsCount - 1)
            options.index = options.totalProductsCount - 1;

        checkButtonsVisibility(options, element, true);
        
        var l = options.positions[options.index];

        $(element).find('div.images img.item-image-' + options.index.toString())
            .stop()
            .animate({'width': options.imageWidth, 'height': options.imageWidth, 'opacity': '1'}, 400)
            .css({'cursor': 'auto', 'pointer-events': 'auto'});
        $(element).find('div.options li.item-configuration-' + options.index.toString() + '>span').stop().fadeIn(400);
        
        $(element).find('ul.items').each(function() {
            $(this).stop().animate({'margin-left': l + 'px'}, 400);
        });

        $(element).find('div.options li').each(function() {            
            $(this).css('height', ($(this).find('span').height() + 3) + 'px');
        });
        
        return options.index;
    };
    
    $.productsSlider.itemsListLeftOffset = 0;
    
    $.productsSlider.defaultOptions = {
        index: 'auto',
        imageWidth: 'auto',
        disabledWidth: 'auto',
        imagePath: './images/',
        imageButtonWidth: 14,
        imageArrowPrev: 'arrow-prev.png',
        imageArrowNext: 'arrow-next.png',
        captionPrev: 'Vorige Konfiguration',
        captionNext: 'Nächste Konfiguration',
        visibleImages: 3,
    };

})(jQuery);