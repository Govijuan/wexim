
// ======================================================================
//  * Hilfs-Plugins / -Funktionen
// ======================================================================

(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.get(0).clientHeight;
    }
})(jQuery);

function getScrollBarWidth() {
    return 16; // Scrollbar width is 12px in CSS
        
    var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
        $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
        inner = $inner[0],
        outer = $outer[0];
     
    jQuery('body').append(outer);
    var width1 = inner.offsetWidth;
    $outer.css('overflow', 'scroll');
    var width2 = outer.clientWidth;
    $outer.remove();
 
    return (width1 - width2);
}

// ======================================================================
//  * Doc Handler
// ======================================================================

jQuery(document).ready(function($) {

    // ======================================================================
    //  * Variablen
    // ======================================================================

    var PRICE_FILTER_VALUE = 1000;
    var HEIGHT_FILTER_INDEX = 1;
    var WEIGHT_FILTER_INDEX = 1;

    //var PRODUCT_PROPERTIES = [];
    
    var activeBand = 0;
    var BAND_BREAKING_POINTS = [0, 321, 481, 556, 769, 1025, 1281, 1921];
    
    var opc_section_index = 0;
    var filter_enabled = false;
    
    // ======================================================================
    //  * Hilfsfunktionen
    // ======================================================================
    
    function updateProductFilters() {
        if((window.PRODUCT_PROPERTIES === undefined) || (CURRENT_CATEGORY != 'Arbeiten'))
            return;
        
        var bodysize = HEIGHT_FILTER_INDEX; //$('#category-filter-bodysize').slider('value');
        var bodyweight = WEIGHT_FILTER_INDEX; //$('#category-filter-bodyweight').slider('value');
        var price = PRICE_FILTER_VALUE; //$('#category-filter-price').slider('value');
        var properties = new Array(
            $('#live-category-filter-properties-1').prop('checked'),
            $('#live-category-filter-properties-2').prop('checked'),
            $('#live-category-filter-properties-3').prop('checked'));
        var products_list = $('#products-list');
        var products_count = 0;
        var filtered_count = 0;
        
        products_list.find('li').each(function() {
            var id = parseInt($(this).attr('id').replace('product-item-', ''));
            var found = false;
            var index = -1;
            for(var i = 0; i < PRODUCT_PROPERTIES.length; i++) {
                if(PRODUCT_PROPERTIES[i]['id'] == id) {
                    found = true;
                    index = i;
                    break;
                }
            }
            
            if(found) {
                products_count++;
                var filter = false;
                if(filter_enabled) {
                    if(PRODUCT_PROPERTIES[index]['height'].indexOf(bodysize) == -1)
                        filter = true;
                    
                    if(!filter && (PRODUCT_PROPERTIES[index]['weight'].indexOf(bodyweight) == -1))
                        filter = true;

                    if(!filter && properties[0] && !PRODUCT_PROPERTIES[index]['properties'][0])
                        filter = true;

                    if(!filter && properties[1] && !PRODUCT_PROPERTIES[index]['properties'][1])
                        filter = true;

                    if(!filter && properties[2] && !PRODUCT_PROPERTIES[index]['properties'][2])
                        filter = true;
                    
                    if(!filter && (PRODUCT_PROPERTIES[index]['price'] > price))
                        filter = true;
                }
                
                if(filter) {
                    $(this).css({'display': 'none'});
                    filtered_count++;
                }
                else {
                    $(this).css({'display': 'block', 'opacity': 1});
                }
            }
        });
        
        if(filtered_count == products_count) {
            $('#products-no-filter-results').css('display', 'block');
        } else {
            $('#products-no-filter-results').css('display', 'none');
        }
    }
    
    function createBreakingPoints() {
        Response.create({ 
            prop: "width",
            breakpoints: BAND_BREAKING_POINTS,
            lazy: true
        });
    }
    
    function getActiveBand() {
        var lastActiveBand = activeBand;
        for(var i = 0; i < BAND_BREAKING_POINTS.length; i++) {
            if(Response.band(BAND_BREAKING_POINTS[i])) {
                activeBand = i;
                //break;
            }
        }
        
        var bandChanged = (lastActiveBand != activeBand);
        return bandChanged;
    }

    function showProductFilter(scrollSpeed) {
        if(undefined === $("#live-category-filter-wrapper"))
            return;
        $("#live-category-filter-wrapper").stop().animate({
            height: "380px",
        }, scrollSpeed);
        $("#live-category-filter-container").stop().animate({
            height: "340px",
        }, scrollSpeed, function() {
            $(this).stop();
            $(this).css({'overflow': 'visible'});
        });
    }
    
    function toggleProductFilter(enabled, fadeSpeed) {
        var overlay = $("#live-category-filter-container>div#filter-overlay");
        if(!enabled) {
            $("#live-category-filter-container").append('<div id="filter-overlay" style="position:absolute;top:0;left:0;width:100%;height:100%;background-color:#fff;opacity:0;z-index:999;">&nbsp;</div>');
            $("div#filter-overlay").animate({'opacity': '0.5'}, 'fast');
        }
            //$("#live-category-filter-container").animate({'opacity': '0.5'}, fadeSpeed);
        else
            overlay.stop().fadeOut('fast', function(){ $(this).remove() });
            //$("#live-category-filter-container").animate({'opacity': '1'}, fadeSpeed);
        /*each(function() {
            $(this).attr('disabled', !enabled);
        });*/
        /*if(!enabled) {
            $("#live-category-filter-wrapper").animate({
                height: "40px",
            }, 400);
            $('#live-category-filter-container').css({'overflow': 'hidden'});
            $("#live-category-filter-container").animate({
                height: "0px",
            }, 400, function() {
                $(this).stop();
                $('#live-category-filter-container').css({'overflow': 'hidden'});
            });
        } else {
            $("#live-category-filter-wrapper").animate({
                height: "380px",
            }, 400);
            $("#live-category-filter-container").animate({
                height: "340px",
            }, 400, function() {
                $(this).stop();
                $(this).css({'overflow': 'visible'});
            });
        }*/
        updateProductFilters();
    }

    function scrollToAnchorDiv(id, padding) {
        var aTag = $("div[id='"+ id +"']");
        var o = aTag.offset().top - padding;
        $('html,body').animate({scrollTop: o}, 400);
    }
    
    // ======================================================================
    //  * Hover-Handlers
    // ======================================================================
    
    $('#product-info').mouseover(function() {
        var position = $(this).position();
        var width = $('#product-information-hover').width();
        var height = $(this).height();
        var x = position.left - Math.round(width / 2) + Math.round($(this).width() / 2);
        var y = position.top + height + 8;
        
        $('#product-information-hover').css({left: x, top: y})
        $('#product-information-hover').show();
    }).mouseleave(function() {
        $('#product-information-hover').hide();
    });
    
    $('#products-list .item-wrapper').mouseover(function() {
        var overlay = $(this).find('div');
        overlay.stop().fadeTo('fast', 0.95);
    }).mouseleave(function() {
        var overlay = $(this).find('div');
        overlay.stop().fadeTo('fast', 0);
    });
    
    $('#contact-box-teaser').mouseover(function() {
        var overlay = $('#contact-box-info');
        $(this).css('z-index', '0');
        overlay.css('z-index', '1');
        overlay.stop().fadeTo('fast', 1);
        $(this).stop().fadeTo('fast', 0);
    });

    $('#contact-box-info').mouseleave(function() {
    }, function() {
        var teaser = $('#contact-box-teaser');
        $(this).css('z-index', '0');
        teaser.css('z-index', '1');
        teaser.stop().fadeTo('fast', 1);
        $(this).stop().fadeTo('fast', 0);
    });
    
    $('#shopinfo-box').mouseover(function() {
        var overlay = $('#shopinfo-details');
        $(this).css('z-index', '0');
        overlay.css('z-index', '1');
        $(this).stop().fadeTo('fast', 0);
        overlay.stop().fadeTo('fast', 1);
    });

    $('#shopinfo-details').mouseleave(function() {
        $('#shopinfo-box').css('z-index', '1');            
        $(this).css('z-index', '0');
        $(this).stop().fadeTo('fast', 0);
        $('#shopinfo-box').stop().fadeTo('fast', 1);
    });
    
    $('#cart-hover').mouseover(function(e) {
        if(!$(e.target).is('li#cart-hover') && !$(e.target).is('img'))
            return;
        var overlay = $('#cart-overlay');
        overlay.css({'z-index': '999', 'display': 'block'});
        overlay.stop().fadeTo('fast', 1);
    });

    $('#cart-overlay').mouseleave(function(e) {
        if($(e.target).is('li#cart-hover') || $(e.target).is('img'))
            return;
        $(this).stop().fadeOut('fast', function() {
            $(this).hide();
            $(this).css('z-index', '0');
        });
    });
    
    $('#mobile-navicon').toggle(
    	function(){
	    	$(this).find('img').attr('src', '/skin/frontend/wexim/default/images/layout/navigation/320_navicon_active.png');
	    	$('.nav-container').fadeIn('fast');
    	},
    	
    	function(){
	    	switch(activeBand) {
                case 0:
                case 1:
                case 2:
                case 3:
            	    $('#mobile-navicon').find('img').attr('src', '/skin/frontend/wexim/default/images/layout/navigation/320_navicon_inactive.png');
                    $('.nav-container').fadeOut('fast');
                    break;
    	    }
    	}
    );
    
    // ======================================================================
    //  * Slider
    // ======================================================================
    
    $('#category-filter-bodysize, #category-filter-bodyweight').slider({
        value: 1,
        min: 1,
        max: 3,
        step: 1,
        paddingMin: 0,
        paddingMax: 0
    });
    
    $('#category-filter-bodysize').slider({
        slide: function(event, ui) {
            HEIGHT_FILTER_INDEX = ui.value; 
            updateProductFilters();
        }
    });
    
    $('#category-filter-bodyweight').slider({
        slide: function(event, ui) {
            WEIGHT_FILTER_INDEX = ui.value; 
            updateProductFilters();
        }
    });
    
    $('#category-filter-price').slider({
        value: PRICE_FILTER_VALUE,
        min: 0,
        max: 2500,
        step: 50,
        paddingMin: 18,
        paddingMax: 18,
        slide: function(event, ui) {
            PRICE_FILTER_VALUE = ui.value;
            var str = ui.value.toString() + " €";
            $('#category-filter-price-label').text(str)
            
            updateProductFilters();
        }
    });
    
    $('#perfect-chair-form .slider').slider({
        value: 5,
        min: 0,
        max: 10,
        step: 1,
        paddingMin: 18,
        paddingMax: 18
    });
    
    // ======================================================================
    //  * Checkbox-Handler des Produktfilters
    // ======================================================================
    
    $('.live-category-filter-properties').change(function() {
        updateProductFilters();
    });
    
    $('#live-category-filter-enabled').change(function() {
        filter_enabled = $(this).prop('checked');
        toggleProductFilter(filter_enabled, 200);
    });
    
    // ======================================================================
    //  * OnClick-Handler
    // ======================================================================
    
    $('#your-perfect-chair').click(function() {
        var form = $('#perfect-chair-form');
        form.css({top:'50px', left:'50%',margin:'0 0 0 -'+(form.width() / 2)+'px'});

        form.parent().append('<div id="modalBlocker" class="modalBlocker">&nbsp;</div>');
        var modalBlocker = $('#modalBlocker');
        
        modalBlocker.css('z-index', '998');
        form.css('z-index', '999');
        
        form.fadeIn('fast');
        modalBlocker.fadeIn('fast');
    });
    
    $('#show-pickup-info').click(function() {
        var form = $('#pickup-info');
        form.css({top:'50px', left:'50%',margin:'0 0 0 -'+(form.width() / 2)+'px'});

        form.parent().append('<div id="modalBlocker" class="modalBlocker">&nbsp;</div>');
        var modalBlocker = $('#modalBlocker');
        
        modalBlocker.css('z-index', '998');
        form.css('z-index', '999');
        
        form.fadeIn('fast');
        modalBlocker.fadeIn('fast');
    });

    $('#close-pickup-info').click(function() {
        var form = $('#pickup-info');

        form.parent().append('<div id="modalBlocker" class="modalBlocker">&nbsp;</div>');
        var modalBlocker = $('#modalBlocker');
        
        form.fadeOut('fast');
        modalBlocker.fadeOut('fast', function() {
            $(this).remove();
        });
    });
    
    $('#perfect-chair-form .header .close').click(function() {
        var form = $('#perfect-chair-form');

        form.parent().append('<div id="modalBlocker" class="modalBlocker">&nbsp;</div>');
        var modalBlocker = $('#modalBlocker');
        
        form.fadeOut('fast');
        modalBlocker.fadeOut('fast', function() {
            $(this).remove();
        });
    });
    
    $('#perfect-chair-form .submit a').click(function() {
        var send_data = new Object();        
        var error = false;
        $('#perfect-chair-form .slider').each(function() {
            var value = $(this).slider('value');
            var label = $(this).parent().find('.label').text().toLowerCase();
            send_data[label] = value;
        });
        $('#perfect-chair-form').find('input').each(function() {
            if(error)
                return;
            if(!this.value.length) {
                alert("Sie müssen einen gültigen Wert angeben!");
                $(this).focus();                
                $(this).animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#e1e6e9'}, 100)
                    .animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#e1e6e9'}, 100)
                    .animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#e1e6e9'}, 100);
                error = true;
                return;
            }
            send_data[this.name] = this.value;
        });
        
        if(error)
            return;
        
        var parent = $(this).parent();
        var element = $(this);
        var $loading_img = $('<img src="/skin/frontend/wexim/default/images/loading.gif" alt="">');
        element.css('display', 'none');
        parent.append($loading_img);
        
        // **
        $.ajax({
            type: 'POST',
            url: '/yourchair-contacts/index/perfectChairPost',
            data: send_data
        }).done(function(result) {
            element.css('display', 'block');
            $loading_img.remove();
            $('#perfect-chair-form .thank-you').css('z-index', parseInt($('#perfect-chair-form').css('z-index')) + 1).fadeIn('fast');
        });
        // **                
    });
    
    $('#perfect-chair-form .close a').click(function() {
        $('#perfect-chair-form .header .close').triggerHandler('click');
    });

    $('#about-us input[type="submit"]').click(function(e) {
        e.preventDefault();
        
        var send_data = new Object();        
        var error = false;
        $('#about-us .contact-form').find('input, textarea').each(function() {
            if(error)
                return;
            if(!this.value.length) {
                alert("Sie müssen einen gültigen Wert angeben!");
                $(this).focus();                
                $(this).animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#fff'}, 100)
                    .animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#fff'}, 100)
                    .animate({backgroundColor: '#f00'}, 100)
                    .animate({backgroundColor: '#fff'}, 100);
                error = true;
                return;
            }
            send_data[this.name] = this.value;
        });
        
        if(error)
            return;
            
        var parent = $(this).parent();
        var element = $(this);
        var $loading_img = $('<div style="width:100%; text-align:center"><img src="/skin/frontend/wexim/default/images/loading.gif" alt=""></div>');
        element.css('display', 'none');
        parent.append($loading_img);
        
        // **
        $.ajax({
            type: 'POST',
            url: '/yourchair-contacts/index/contactUsPost',
            data: send_data
        }).done(function(result) {            
            $('#about-us .contact-form').remove();
            $('#about-us .contact-form-thank-you').fadeIn('fast');
        });
        // **                
    });
    
    $('#cart-hover').on('click', function() {
        $('#cart-overlay').mouseleave();
    });
    
    $('#mobile-navicon').on('click', function() {
        $('#nav').mouseleave();
    });
    
    $('.slide-scroller').on('click', function(event) {
        event.preventDefault();
        var id = $(this).attr('href').replace('#', '');
        scrollToAnchorDiv(id, $('#top_navigation').height());
    });
    
    $('#bestsellers a.image').on('click', function(event) {
        event.preventDefault();
        //scrollToAnchorDiv('top_navigation');
        $('html,body').animate({scrollTop: 0}, 400);
        var url = $(this).attr('href').replace('#?', '');
        var url_items = url.split('&');
        Configuration.config.preventImageRefresh = true;
        for(var i = 0; i < url_items.length; i++) {
            if(i == url_items.length - 1)
                Configuration.config.preventImageRefresh = false;
            var item = url_items[i];
            var option = item.split('=');
            var select_name = 'options[' + CURRENT_PRODUCT_ID + '][' + MAGE_JS_TEMPLATE_ID + '][template][' + option[0] + ']';
            $('select[name="' + select_name + '"]').val(option[1]).change();
        }
    });
    
    // ======================================================================
    //  * Responsive Handlers
    // ======================================================================
    
    Response.resize(function(){
        getActiveBand();            
        switch(activeBand) {
            default:
                console.log('activeBand = ' + activeBand.toString());
        }        
    });
    
    $(" div.std div#home-page div.wrapper ").last().addClass("last-wrapper");
    
     	
	
    // ======================================================================
    //  * Custom Handlers
    // ======================================================================

    $('#checkout-onepage').on('sectionChange', function(e, section) {
        var sections = ['login', 'billing', 'shipping', 'shipping_method', 'payment', 'review'];
        
        if(section < 0)
            opc_section_index--;
        else
            opc_section_index = sections.indexOf(section);
            
        for(var i = 0; i < sections.length; i++) {
            $('#step-box-container-' + sections[i]).removeClass('active');
            $('#step-label-container-' + sections[i]).removeClass('active');
        }
        
        for(i = 0; i <= opc_section_index; i++) {
            $('#step-box-container-' + sections[i]).addClass('active');
        }
        
        $('#step-label-container-' + sections[opc_section_index]).addClass('active');
        if(sections[opc_section_index] == 'review')
            $('#checkout-progress-wrapper').show();
        else
            $('#checkout-progress-wrapper').hide();
    });

    // ======================================================================
    //  * Initialisierung
    // ======================================================================

    $('#showroom #slides').slidesjs({
        width: 525,
        height: 349,
        play: {
          active: true,
          auto: true,
          interval: 4000,
          swap: true
        }
    }); 
      
    var hoverCount = 0;
    $('#home-page .wrapper > div > a > img').each(function() {
        var src = this.src;
        var hover_src = src.replace('.jpg', '_hover.jpg');
        hoverCount++;
        $(this).addClass('hover');
        $(this).addClass('image-' + hoverCount);
        $(this).parent().prepend('<img src="' + hover_src + '" alt="" class="image-' + hoverCount + '-hover">');

        $('#home-page > .wrapper > div > a > img.image-' + hoverCount).mouseover(function(e) {
            $(this).stop().fadeOut('fast');
        });
        $('#home-page > .wrapper > div > a > img.image-' + hoverCount + '-hover').mouseleave(function(e) {
            $('img.' + this.className.replace('-hover', '')).stop().fadeIn('fast'); 
        });
    });
    
    $('#category-filter-price').find('a').append('<div id="category-filter-price-tag"><span id="category-filter-price-label">' + $('#category-filter-price').slider('value') + ' €</span></div>');
    
    $('#product-info-panel').tabs({
        activate: function(event, ui){
            var container = $('#product-info-panel');
            var element = $('#' + event.toElement.id.replace('ui-id-', 'category-'));
        }
    });
    
    $('#customer-account-categories').tabs(); 
    /*$('.col-left.sidebar').scrollFollow({
        container: 'main-box'
    });
    $('#top_navigation').scrollFollow();*/
    /*$('.product-view img#image').elevateZoom({
        scrollZoom: true,
        zoomLevel: 1,
        cursor: "none",
        lensBorderColor: '#f00',                                
        lensBorderSize: 2,
        zoomWindowWidth: 545,
        zoomWindowHeight: 545,
        zoomWindowOffsetx: 1,
        zoomWindowOffsety: 0,
        borderColor: "#989898",
        borderSize: 1
    });*/
    
    /*$('.col-left.sidebar').mCustomScrollbar({
        scrollButtons:{
                enable:true
        }
    });*/
    
    //$('#live-category-filter-enabled').trigger('click');
    showProductFilter(0);
    toggleProductFilter(false, 0);
    
    //updateProductFilters();
    createBreakingPoints();
    getActiveBand();    
});

// ======================================================================
//  * Window Handler
// ======================================================================
//jQuery.addOutsideEvent('sectionChange');

jQuery(function() {    
    var documentHeight = jQuery(document).height();
    var topPadding = 80;
    var offsetProductInfoBox;
    var scrollBarWidth = getScrollBarWidth();
    var sideBarAdjusted = false;
    var addWrapperClass = false;
    var lastSidebarWidth = 193;

    function alignElements() {
        var h = jQuery(window).height();
        if(window.location.href.toLowerCase().indexOf('arbeiten') != -1) {
            h += jQuery("#live-category-filter-wrapper").height() + 40;
        }
        jQuery(".col-left.sidebar").css('height', h + 'px');

        var colMainWidth = jQuery(window).width() - 194;
        if (colMainWidth + 194 > 1024) {
            var homePageColWidth = (colMainWidth / 3);
            if (addWrapperClass) {
                addWrapperClass = false;
                jQuery('.last-wrapper').addClass('wrapper');
            }
        }
        else if ((colMainWidth + 194 < 1025) && (colMainWidth + 194 > 480)) {
            var homePageColWidth = (colMainWidth / 2);
            jQuery('.last-wrapper').removeClass('wrapper').css('width', '100%');
            jQuery('.last-wrapper img').css('width', '100%');
            //jQuery('.last-wrapper div').css('width', homePageColWidth.toString() + 'px');*/
            addWrapperClass = true;
        }
        else {
            homePageColWidth = colMainWidth;
        }
        
        var windowWidth = jQuery(window).width();
        console.log(windowWidth);
        if ((windowWidth > 555) && (windowWidth < 775) ){
	        var topNavigationLiWidth = colMainWidth / 2 - 31;
	        jQuery('.top-navigation li').css('width', topNavigationLiWidth.toString() + 'px');
            jQuery('.top-navigation li.login div.responsive-wrapper').css('width', topNavigationLiWidth.toString() + 'px');
            
            if(jQuery('.nav-container').css('display') == 'none')
                jQuery('.nav-container').show();
        } else if (windowWidth <= 555) {
	        colMainWidth = windowWidth;
	        var topNavigationLiWidth = colMainWidth / 2 - 42;
	        jQuery('.top-navigation li').css('width', topNavigationLiWidth.toString() + 'px');
            jQuery('.top-navigation li.login div.responsive-wrapper').css('width', 'auto');
            //jQuery('.nav-container').hide();
        } else if (windowWidth >= 775) {
            colMainWidth = windowWidth - 194;
            jQuery('.top-navigation li').css('width', 'auto');
            jQuery('.top-navigation li.login div.responsive-wrapper').css('width', 'auto');
            var li = jQuery('.top-navigation li.about_us');
            li.css('width', 'auto').css('width', (li.width() + 56) + 'px');
            var li = jQuery('.top-navigation li.showroom');
            li.css('width', 'auto').css('width', (li.width() + 56) + 'px');           
        }
                       
        var wh = jQuery(window).height(); 
        jQuery('.col-left.sidebar').css('height', wh.toString() + 'px');
        jQuery('.col-main').css('min-height', (wh - 1).toString() + 'px');
        if (windowWidth > 555) {
            jQuery('.col-main').css('margin-left', lastSidebarWidth + 'px');
            if (jQuery('.col-left.sidebar').hasScrollBar() && !sideBarAdjusted) {
                sideBarAdjusted = true;
                lastSidebarWidth += scrollBarWidth;
                jQuery('.col-left.sidebar').css('width', lastSidebarWidth + 'px');
                jQuery('.col-main').css('margin-left', lastSidebarWidth + 'px');
            } else if (!jQuery('.col-left.sidebar').hasScrollBar() && sideBarAdjusted) {
                lastSidebarWidth -= scrollBarWidth;
                jQuery('.col-left.sidebar').css('width', lastSidebarWidth + 'px');
                jQuery('.col-main').css('margin-left', lastSidebarWidth + 'px');
                sideBarAdjusted = false;
            }
        } else {
            jQuery('.col-main').css('margin-left', '0');
        }
        jQuery('#top_navigation').css('width', colMainWidth.toString() + 'px');    
        jQuery('#home-page .wrapper').css('width', homePageColWidth.toString() + 'px');
        
        if (colMainWidth >= 770) {
            var productContainerImageWidth = (Math.round(jQuery('#product-container').width()) - 550);
            if (productContainerImageWidth < 280)
                productContainerImageWidth = 280;
            var pt = (jQuery('#product-container div.col_right').height() / 2) - (productContainerImageWidth / 2);
            jQuery('#product-container div.col_left').css({'width': productContainerImageWidth.toString() + 'px'});
            jQuery('#product-container div.col_left .product-img-box').css({'width': productContainerImageWidth.toString() + 'px', 'height': productContainerImageWidth.toString() + 'px', 'padding-top':  pt + 'px' });
            //jQuery('#product-container div.col_left img#image').css('width', productContainerImageWidth.toString() + 'px');
        } else {
            jQuery('#product-container div.col_left').css({'width': '100%'});
            jQuery('#product-container div.col_left .product-img-box').css({'width': '100%', 'padding-top': '0'});
        }
        
        
        var imgWidth = jQuery('#product-container div.col_left .product-img-box').width();
        jQuery('.zoomContainer').css('width', imgWidth + 'px');
        jQuery('.zoomContainer').css('height', imgWidth + 'px');
        jQuery('.zoomWindowContainer').css('width', imgWidth + 'px');
        jQuery('.zoomWindowContainer').css('height', imgWidth + 'px');
        jQuery('.zoomWindow').css('width', imgWidth + 'px');
        jQuery('.zoomWindow').css('height', imgWidth + 'px');
        
        var checkoutSteps = jQuery('#checkoutSteps');
        if(!(undefined === checkoutSteps)) {
            if(!(jQuery('#checkout-steps-overview').css('display') == 'none')) {
                var w = (colMainWidth - jQuery('#checkout-steps-overview').width() - 80) + 'px';
            } else
                w = '99%';
            checkoutSteps.css('width', w);
        }
        
        var productsListWidth = Math.floor((colMainWidth - 0) / 340) * 340;
        if((windowWidth < 568) && (windowWidth > 359)){
	       productsListWidth = Math.floor((colMainWidth - 0) / 180) * 180;
        } else if (windowWidth < 372){
	       productsListWidth = Math.floor((colMainWidth - 0) / 150) * 150;
           var itemImgWidth = jQuery('#products-list > li.item > div.item-wrapper').width();
           if (productsListWidth < itemImgWidth * 2)
            productsListWidth = itemImgWidth;
        }
        
        jQuery('#products-list').css('width', productsListWidth.toString() + 'px');
        
        var productInfoPanelTabWidth = Math.round(jQuery('#product-info-panel').width() / 3) - 2;
        /*if (productInfoPanelTabWidth < 140)
            productInfoPanelTabWidth = 140;*/
        jQuery('#product-info-panel>ul>li').css('width', productInfoPanelTabWidth.toString() + 'px')
        
        jQuery('#home-page .wrapper > div, #home-page .last-wrapper > div').each(function() {
            var height = 0;
            var elm = jQuery(this);
            var img = elm.find('img');
            
            if(img == 'undefined')
                return;
            
            elm.css('height', img.css('height'));
        });

        var form = jQuery('#pickup-info');
        if(!(undefined === form)) {
            form.css({top:'50px', left:'50%',margin:'0 0 0 -'+(form.width() / 2)+'px'});
        }
        
        offsetProductInfoBox = jQuery("#product-container>div.col_right").offset();
    }

    jQuery(window).load(function () {
        alignElements();        

        jQuery('#bestsellers').productsSlider({
            imagePath: '/skin/frontend/wexim/default/images/',
            imageArrowPrev: 'red-arrow-prev.png',
            imageArrowNext: 'red-arrow-next.png',
            visibleImages: 3
        });
        jQuery('#recently-viewed').productsSlider({
            imagePath: '/skin/frontend/wexim/default/images/',
            imageArrowPrev: 'red-arrow-prev.png',
            imageArrowNext: 'red-arrow-next.png',
            visibleImages: 3
        });       
    });

    jQuery(window).resize(function () {
        alignElements();        
    });
    
    jQuery(window).scroll(function() {
        /**** Slide-Handler for Product Configuration Options Box in Product View ****/
        
        var windowWidth = jQuery(window).width();
        if(!offsetProductInfoBox || (windowWidth <= 963))
            return;
            
        //console.log(jQuery(window).scrollTop().toString() + ' :: ' + offsetProductInfoBox.top.toString());
        if (jQuery(window).scrollTop() + topPadding > offsetProductInfoBox.top) {            
            var productInfoBoxHeight = jQuery('#product-container>div.col_right').height();
            var imageHeight = jQuery('#product-container>div.col_left #image').height();
            var newPosition = (jQuery(window).scrollTop() - offsetProductInfoBox.top) + topPadding;
            var maxPosition = imageHeight - productInfoBoxHeight;
            if (maxPosition < 0)
                maxPosition = 0;
            if (newPosition >= maxPosition) {
                newPosition = maxPosition;
            }
            jQuery("#product-container>div.col_right").stop().animate({
                marginTop: newPosition
            });
        } else {
            jQuery("#product-container>div.col_right").stop().animate({
                marginTop: 0
            });
        }
        
        /********/        
    });    
});